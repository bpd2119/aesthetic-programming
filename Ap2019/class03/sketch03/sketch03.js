//sketch03, prepared by Winnie Soon
let loading_createimg;

function preload() {
 loading_createimg = createImg("images/loading.gif"); //img ref: http://i.imgur.com/omGnqz7.gif
}

function setup() {
 createCanvas(windowWidth, windowHeight);   //create a drawing canvas
 background(10);
 frameRate (10);  //try to change this parameter
}

function draw() {
  loading_createimg.position(width/2-loading_createimg.width/2,20); //loads GIF - related to the width of the gif
  fill(10,80);  //check this syntax with alpha value
  noStroke();
  rect(0, 0, width, height);
  drawThrobber(9);  //pass to another function, try changing this number
}

function drawThrobber(num) {
  push();
  translate(width/2, height/2); //move things to the center
  // 360/num >> degree of each ellipse' move ;frameCount%num >> get the remainder that indicates the movement of the ellipse
  let cir = 360/num*(frameCount%num);  //to know which one among 9 possible positions.
  rotate(radians(cir));
  noStroke();
  fill(255,255,0);
  ellipse(35,0,22,22);  //the moving dot(s), the x is the distance from the center
  pop();

  stroke(255,0,0);
  line(60,0,60,height);   //a static line
  line(width-60,0,width-60,height);   //a static line
}
