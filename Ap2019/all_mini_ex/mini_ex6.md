# Weekly mini ex6: due week 12, Monday night | Games with objects

**Objective:**
- To implement a class-based object oriented sketch via abstracting and designing objects' properties and behaviors.
- To reflect upon object orientation in the domain of digital culture

**Get some additional inspiration here:**
- [p5.js coding challenge #31: Flappy Bird by Daniel Shiffman](https://www.youtube.com/watch?v=cXgA1d_E-jY)
- [p5.js coding challenge #3: The snake game by Daniel Shiffman](https://www.youtube.com/watch?v=AaGK-fj-BAM)
- [EAT FOOD NOT BOMBS by Ben Grosser](https://editor.p5js.org/bengrosser/full/Ml3Nj2X6w?fbclid=IwAR0pegtorx1cyYYKsEh8jNXTHdFika6tGIGOjEUgr8vTXGLHv6ajYuY4EQI)
- You may find [p5.play library](http://p5play.molleindustria.org/) useful
  - [p5.play library - examples, Pong](http://p5play.molleindustria.org/examples/index.html?fileName=pong.js)
  - [p5.play library - examples, Asteroid](http://p5play.molleindustria.org/examples/index.html?fileName=asteroids.js)
  - [p5.play library - examples, Breakout](http://p5play.molleindustria.org/examples/index.html?fileName=breakout.js)
- [Characterror: 90 second time challenge demo](http://characterror.decontextualize.com/) by Allison Parrish

**Tasks (RUNME and README):**
1. Make sure you have read/watch the required readings/instructional videos and references on object oriented programming
2. Think of a simple game that you want to design and implement, what are the objects required? What are their properties and behaviors? At the most basic level, you need to **use class-based object oriented approach** to design your game components that can exhibit certain behaviors (that means you need to at least have a class, constructor and method). If you can master objects and classes, you may further work on a mini game (with basic rules) and/or with p5.play that works with data input and interactive devices (such as keyboard and mouse) that you have learned from previous classes.
3. Upload your 'runme' to your own Gitlab account under a folder called **mini_ex6**. (Make sure your program can be run on a web browser)
4. Create a readme file (README.md) and upload to the same mini_ex6 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot/animated gif/video about your program (search for how to upload your chosen media and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/ or https://raw.githack.com/.
  - Try to contextualize your sketch by answering these:
    - **Describe** how does your game/game object work?
    - **Describe** how you program the objects and their related attributes and methods in your game.
    - Based on Shiftman's videos, Fuller and Goffey's text and in-class lecture, what are the **characteristics** of object-oriented programming?
    - **Extend/connect** your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being **abstracted**? What are the **implications** of using object oriented programming?
5. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)" (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 5600 characters.

**mini exercise peer-feedback: guideline**
1. (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
2. Describe (basic) what is the program about, how objects are being constructed? What does the work express?
3. Do you like the design of the program, and why? and which aspect do you like the most? How would you interpret the work?
4. How about the conceptual linkage on 'objects' beyond technical description and implementation? What's your thoughts/response on this?
