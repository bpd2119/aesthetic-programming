# Weekly mini ex8: due week 14, Monday night | Generate an electronic literature

**Objective:**
- To design and implement an electronic literature that utilizes text as the main medium (but text can be also manifested in various forms)
- To learn to code and conceptualize a program collaboratively.
- To reflect upon the aesthetics of code and language.

**Get some additional inspiration here:**
- [e-lit collection1](http://collection.eliterature.org/1/)
- [e-lit collection2](http://collection.eliterature.org/2/)
- [e-lit collection3](http://collection.eliterature.org/3/)
- [Digital Poetry by David Jhave Johnston](http://glia.ca/)
- [Poems by Ian Hatcher](http://ianhatcher.net/#!/poems)
- [Rita library](http://rednoise.org/rita/) by Daniel Howe (you need to make sure you have time to experiment with the library)

**Tasks (RUNME and README):**
1. This is a group mini exercise for the RUNME part. Form a group of 2 (max 3) based on your study group.
2. Make sure you have read/watch the required readings/instructional videos and references for this week.
3. Work together in a group, and design and develop an electronic (audio)literature (it is not a must to use voice/speech)
4. Upload your group 'runme' to your own Gitlab account under a folder called **mini_ex8**. (Make sure your program can be run on a web browser)
5. Create a readme file (README.md) and upload to the same mini_ex8 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot/animated gif/video about your program (search for how to upload your chosen media and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/ or https://raw.githack.com/.
  - Who are you collaborate with?
  - What's the program about and how's the collaborative process?
  - Try to contextualize your sketch by answering these:
    - **analyze** your own e-lit work by using the text 'Vocable Code', **and** you can also choose to incorporate any text with the theme of 'language' in software studies class. Try to approach this question: what is the **aesthetic aspect** of your program in particular to the relationship between **code and language**?
6. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)" (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 5600 characters.

**mini exercise peer-feedback: guideline**
1. (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
2. Describe (basic) what is the program about and how does it works as a e-lit? What constitute the work as an e-lit for this case?
3. Do you like the design of the program, and why? and which aspect do you like the most?
4. How about the conceptual linkage on 'language' beyond technical description and implementation? What's your thoughts/response on this both conceptually and technically?
