# Weekly mini ex10: due week 17, Monday night | Working together with a Flow Chart (individual and group work)

**Objective:**

- To acquire the ability to decompose/break down a computer program into definable parts
- To organize and structure a computer program through a flow chart
- To understand flowchart as a means for communication and planning
- To understand the concept of algorithms from both computer science and cultural perspectives

**Tasks:**
1. This mini exercise is divided into two parts. First is individual and second is a group work (your whole study group)
2. Make sure you have read/watch the required readings/instructional videos and references for this week.
3. For the **individual** work:
  - Revisit your previous mini exercises and select the most technically complex one
  - Draw an individual flow chart to present the program (Pay attention to: which items you select to present through a flow chart)
  - In the readme file:
      - Attach the flowchart image
      - You need to have a hyperlink that links to your chosen mini_ex folder.
      - What may be the difficulty in drawing the flow chart?
4. For the **group** work:
  - Get together as a study group and brainstrom the forthcoming program for the final project
  - In the readme file:
      - Present two different ideas with two different flow charts (it is important to think about the balance between simplicity and complexity. How can we get a sense of what's your program about?)
      - What might be the possible technical challenges for the two ideas and how are you going to solve them?
      - Individual: How is this flow chart different from the one that you had in #3 (in terms of the role of a flow chart)?
      - Individual: If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms? (see if you could refer to the text and articulate your thoughts?)
5. Upload your individual and group flowcharts to your own gitlab account under a folder called **mini_ex10**. The readme should contain the the elements in #3 and #4 above.
6. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)" (Feedback is due before next Wed tutorial class as usual)

NB1: The readme file should be within 5600 characters.

**mini exercise peer-feedback: guideline**
1. How's the individual and group flowchart different from each other? Do you understand the flowchart and the algorithm of the program?
2. Comment on the their two group flowcharts from the perspective of the final project, which one do you think is more doable? Which one you think is more interesting and challenging? What might be the challenges? Do you have any advice for them?
