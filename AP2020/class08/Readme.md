## Class 08: Auto Generator

(adjusted according to the immediate introduction of online teaching)

**Messy notes:**

**Agenda:**

1. Everyone check-in
2. Midway evaluation
3. Mini-ex[5] short discussion 
4. Group 6 presentation: the text "randomness" 
5. 10 Print
6. Mini-Lecture
7. Mini-ex walk-through: A generative program | due SUN mid-night
8. Final Project 
9. (Extra - not likely can cover online) Langton's Ant 
10. (Extra - not likely can cover online) Exercise in class

---

### 1. Everyone check-in 

Notes on blackboard: 
- To join the zoom meeting: Go to Blackboard > Online class (left menu) > Zoom meeting > Select the topic "F20 - AP: Class08 - Auto Generator" > Click Start/Join. (You may need to register for the Zoom account and download the application at this point, follow the instruction and use the AU email account for the registration.)
- Join the online meeting on a device that has a wired connection to the internet to avoid delays. Wi-Fi still does a decent job so no worries if you access the meeting through a non-wired device like a mobile phone.
- Use earplugs or a headset for a better sound experience and avoid the echos.
- Mute yourself and hide your camera feed upon entry if it is not default in the meeting room. 
- Join the meeting well ahead of time (say 15 mins) to test your connection and sound (and video)
- Change your profile name to your real name, so that everyone know who is there

In-class Activities: 

**1. Everyone check-in**
- see and hear things clearly? 
- Check-in: Can say anything - not necessarily relate to the programming class: how do you feel right now?
- Potential changes
    - How long we need to use online teaching/learning
    - How will this affect the learning
    - How will this affect the oral exam 
    
**2. Walkthrough some of the features in Zoom**
- on/off video/sound (the mute function)
- Chat
- Share screen (useful for the presenter)
- Whiteboard (useful for the presenter)
- Annotate
- questions?

---

### 2. Midway evaluation 

https://padlet.com/siusoon/iaaabkoo9qey

- Group work 
- Shut up and code 
- Mini Lecture - why and adjustment
- Longer break 
- Class structure: exercise / discussion / coding (too big in class size)
- Glossary for syntax and references: https://pad.riseup.net/p/AP2020-keep 
- Re-state the course objectives: 
    1. Read and write computer code in a given programming language
    2. Use programming to explore digital culture
    3. Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

---

### 3. Mini-ex[5] short discussion

- seperate js files
- Object Oriented Programming vs Object Abstraction
- Link with the assigned reading or cross other themes
- start from something small and repeated action/sketch: e.g shiffman's challenge, recode pacman tofu -> to make sure you know the process in details
- Focus NOT on the game, but the objects and class with properties and behaviors and how you manipulate them e.g push and splice. 
- Thinking part:  think conceptually about how objects model the world and what this suggests in terms of an understanding of its hidden layers.

---

### 4. Group 6 presentation:

Week 12: the text randomness in 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, The MIT Press, 2012, pp. 119-146

1. Jonas Paaske Ditlevsen https://gitlab.com/jpditlev
2. Sophia Alexandra McCulloch https://gitlab.com/SophiaMcCulloch
3. Mads Lindgaard https://gitlab.com/MadsLindgaard
4. Mikkel Heinrich https://gitlab.com/HeineFaetter

Brief: 
- we can focus more on the text randomness.
- read the text closely
- prepare a short walkthrough and reflection on the text (below are some examples to for you to think differently about the text instead of just having a summary)
    - thinking around what's interesting to you and why (from reading).
    - You have already known how to use randomness in terms of syntax, so what's new to you that you found in the text?
    - how do you think about approaching randomness in this cultural way? (as the text illustrated)
    - how you imagine you could link (the text and the syntax randomness) to the idea of “thinking with and through code”
- also prepare 2 additional questions/confusions about the text so that we can all look into them together.

---

### 5. 10 Print (Discussion & Exercise)

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_4.png" width="600">

Run here: https://editor.p5js.org/siusoon/sketches/pQlol8qc

**Background:** 

<img src = "http://text-mode.org/wp-content/uploads/2019/08/10print-trammell-hudson.jpg" width="600">

- written in BASIC
- command: `10 PRINT CHR$(205.5+RND(1));: GOTO 10`
- executed on a Commodore 64 home computer 

Prepare to have breakout rooms

**A. Discuss the following simple rules of 10 PRINT and map them to the related lines of code:**
1. Throw a dice randomly and half of the time print a backward slash
2. The other half of the time will print a forward slash

**B. Discuss the use and the role of randomness in 10 PRINT and in the arts, literature, and games? What is randomness to a computer?** (more thinking via text and your experience)

**C. Coding Try to modify the existing rules, for example:**
- Can we have other outputs than just the backward and forward slash?
- Can we chnage the size and color of the slashes?

10 PRINT has been appropriated by many artists, designers and students. Take a look at some of the different possibilities for [10 PRINT](https://twitter.com/search?q=%2310print&src=typd) that are documented on Twitter. Your in-class task is to create a sketch with a clear set of rules and that operates as a modified version of 10 PRINT.

---

### 6. Mini Lecture:

**1. Abstract Machine -> Turing Machine**
- object abstraction -> abstract machine 
- self-operating machine (with the creation of rules)
- Turing's article "[On Computable Numbers, with An Application to the Entscheidungsproblem](http://www.cs.virginia.edu/~robins/Turing_Paper_1936.pdf)" (1936)
- Universal Computing Machine -> How a machine "can be used to computate any computable sequence
- follows a predetermined sequence of instruction that process input and produce output results

![turing](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_1.jpg)

**2. The operations of the Turing Machine va the endless tape**
- Six types of fundamental operations (no computer at that time)
    - read, write, move left, move right, change state, halt/stop
- Running tape as something like memory/storage in a modern computer 
- Deals with read/write/move

**3. Implication of the Turing Machine**
- Instructions are fundamental principles (both to Turing Machine and Modern Computer)
- Capability to compute tasks in numbers 
- Auto-generate various processes
- Seen in pervae digital culture -> processes of production, consumption and distribution of contemporary (informational) capitalism

**4. Instruction and Rules**
- how **instructions** are fundamental elements of adaptive systems
- how **rules** are performed and how they might produce unexpected results 

**5. Instruction based work - Conceptual art vs Generative Art**
- Fluxus and the Conceptual Art movements (1960s and 1970s)
- Challenge art's object-ness -> dematerialisation 

**6. Sol Le Witt**
- his concept is based on a set of instructions but may have different outcomes
- "The idea becomes a machine that makes the art"
- Combines idea and action with scores/scripts (relating to computing: instructions and execution)
- Wall Drawing #289 

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_2.jpg" width="600">

    - twenty-four lines from the center
    - Twelve lines from the midpoint of each of the sides
    - Twelve lines from each corner

**7. Processing as software sketchbook**
- Artist Casy Reas (founder of Processing)
- See: https://artport.whitney.org/commissions/softwarestructures2016/map.html

**8. Coded Algorithmic Drawing by Joan Truckenbrod**
- programmed in 1970s 
- Language: Fortran 
- Simulating moving substance in natural phenomena 

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_3.jpg" width=600>

- Other simulation:
    - flocking: https://p5js.org/examples/simulate-flocking.html
    - fractal: https://editor.p5js.org/marynotari/sketches/BJVsL5ylz

**9. Instruction-based drawing with creativity**
- Drawing -> based on mathematical logic 
- machinic creativity 
- Questions the centrality of human agency 

**10. Generativity**

- Difference between rule-based system (generative art) vs to draw a white ellipse at a particular location in a particular size (`ellipse(100, 120, 10, 10);`)
- Generative capacity: throws into question the extent of control and autonomy over the creative process

> Generative art refers to any art practice where artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art. (Philip Galanter, 2003)

**11. Thinking** 

Auto generator - generativity - autonomy - instructions - control - creativity - automation

---
### 7. Mini-ex walk-through: A generative program | due SUN mid-night

(Optional group work - max 2 people)
https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/Readme.md

---

### 8. Final Project

https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/FinalProject.md




Extra: 
---

### 9. Langton's Ant

Check out the RUNME for this class: http://siusoon.gitlab.io/aesthetic-programming/

Langton's Ant is a classical mathematical game that simulates the molecular logic of a living ant. The simulation of the cell's state is inspired by the classical Turing machine that can perform computational tasks with the manipulation of symbols on a strip of tape according to a set of rules. Based on simple rules, an ant is considered to be the sensor that processes the cell's data as input, then the cell will change it's color by having the ant moving in four possible directions. Gradually, the ant will turn the grid into a more complex system that exhibits emergent behavior over time.

![ant](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_5.gif)

Two general rules below:

1. If the ant is at a white cell, it turns right 90° and changes to a black cell then moves forward one unit.
2. If the ant is at a black cell, it turns left 90° and changes to a white cell then moves forward one unit.

At the beginning, the canvas displays only a grid system and all the individual cells are set in white color. The ant has four possible directions — UP, RIGHT, DOWN, LEFT — and can turn 90° either left or right subject to the color of the cell. The ant's head is pointing in an UPWARD position at the start, and is located in the middle of the white grid. It then follows rule 1 above to rotate the head direction from UP to RIGHT, then it also changes the white cell to the black and moves forward one unit (pointing to the right as per the new head direction). Then the second step is to follow rule 1 again because the new cell is still white. The ant's head direction will turn right 90° and point from RIGHT to DOWN, and then changes the white cell to black and the ant moves forward one unit. Then the third and forth steps are similar to the previous ones until the ant meets a cell which is black, which is started in step 5. At this point, the ant will follow rule 2 instead to change back the cell's color to white and then turns left 90° instead of the right. The complexity builds.

![ant2](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_6.gif)

The above shows how the ant starts building the emergent 'highway' pattern after the first few hundred moves with simple symmetricial patterns. It is then followed by an additional 9000 steps with the seemingly random steps at the center. The highway pattern repeats indefinitely until most of cells are reconfigured, leading to something that is similar to Figure 6.7 at which point the ant is still constantly moving and changing the color of the cells.

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_7.png" width=500>

Based on the above example, there are three areas can help in slowing down or zooming in on the program. 
1. `let grid_space = 5;`: You can change the value to 10 then you are able to see it at a larger size.   
2. `frameRate(20);`: Lower the frame rate value can help slower down the program
3.  `draw()`: This function contains a for-loop where n is the number of steps of the ant so if desired you can reduce the `n < 100` to `n < 1` in the line `for (let n = 0; n < 100; n++) {` thus instructing the program to only process n steps per frame.

Intead of going through the code line by line, this next part is more to show what each function does. 

- `function setup()`: This is more to setup the canvas size, initiate the ant's head direction, frame rate, color and to prepare drawing the background grid structure. 

- `function drawGrid()`: To divide the canvas into a grid structure with lines.

- `function draw()`: This is the main function to check against the two rules of Langton's Ant and change the color of cells. 

- `function nextMove()`: The four directions are structured in a number format so that the variable `dir++` can be used for incrementing or decrementing the ant's direction. Each different direction of *UP, RIGHT, DOWN, LEFT* corresponds to moving forward in either horizontal (xPos) or vertical (yPos) steps on the canvas. 

- `function checkEdges()`: This function is to check if the ant moves out of the four edges, then it will continue at the other end. 

**Nested for-loop**

```javascript
let grid =[]; //on/off state

function setup() {
  grid = drawGrid();
}

function drawGrid() {
  cols = width/grid_space;
  rows = height/grid_space;
  let arr = new Array(cols);
  for (let i=0; i < cols; i++) { //no of cols
    arr[i] = new Array(rows); //2D array
    for (let j=0; j < rows; j++){ //no of rows
      let x = i * grid_space; //actual x coordinate
      let y = j * grid_space; //actual y coordinate
      stroke(0);
      strokeWeight(1);
      noFill();
      rect(x, y, grid_space, grid_space);
      arr[i][j] = 0;  //assign each cell with the off color and link to individual cells
    }
  }
  return arr; //a function with a return value
}
```
---

### 10. Exercise in class

1. Give yourself sometime to read and tinker with the code, as well as to observe the different stages of *Langton's Ant*. 

2. Right now the *Langton's Ant* program is more about representing the world of an ant through abstraction, such as limited cell color, as well as only being able to move in four directions and turn 90°. Rethink the rules that have been implemented. Can you try changing the existing rules or adding new rules so that the ant behaves differently? (Recall what you have changed in the previous exercise on *10 PRINT*.) 

3. It is clear that in simulating and abstracting living systems — such as the complex behaviour of insects — there is a focus on emergent and generative process over end-result. In terms of creative process this allows us to think about how rules and instructions can produce complexity and other forms of control and agency. 

Let's discuss the following questions:
- Can you think of, and describe, other systems and processes that exhibit emergent behaviour? 
- How would you understand autonomy in this context? (For instance, generative artist Marius Watz would suggest that "autonomy is the ultimate goal", from his talk "Beautiful Rules: Generative Models of Creativity") To what extent do you consider the machine to be an active agent in generative systems? What are the implications for wider culture?


