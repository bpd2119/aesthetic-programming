## Class 10: Que(e)ry Data

(adjusted according to the immediate introduction of online teaching)
N.B: update after class with the simple version

**Messy notes:**

**Agenda:**

1. Mini-ex[8] short discussion 
2. Group 8 presentation: API practices...
3. Exercise: NAG
4. Image Processing
5. Exercise: Accessing Web APIs (step by step)
6. Que(e)rying Data + Cross-Origin Resource Sharing 
7. Exercise in class
8. Load Pixels
9. Mini-Lecture
10. To do list

---

### 1. Mini-ex[8] short discussion 

- e-lit 
- Much better text linkage and development 
- Develop a sense of using JSON and will continue...

---

### 2. Group 8 presentation 

Work with this week's assigned reading: API practices and paradigms - Prepare a short walkthrough and reflection on the text - below are some examples for you to think differently about the text instead of just giving a summary

    - thinking around what's interesting to you and why (from the reading).
    - You have already known about the Google service - Image search and JSON, so what's new to you that you found in the text?
    - how do you think about approaching search in this cultural and technical ways? (as the text illustrated)
    - Prepare 2 additional questions/confusions about the text so that we can all look into them together.

1. Katrine Holm Kjærgaard 
2. Amanda Hansen 
3. Mathies Damm  
4. Caroline 

---

### 3. NAG walkthrough 

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_1.png" width="600">

1. Go to net.art generator (https://nag.iap.de/)
2. Explore the generation of images and previous images examples. 
3. Pay close attention to the interface and map out the relationship between user input (e.g. a title) and the corresonding output (the image). What are the processes in between the input and output? How are the images being composited and generated (high level thoughts)?

N.B: limitation of API access.

--- 

### 4. Sample Code

The focus on API (image search API as an example) + JSON parsing + image processing 

Demo

source code: https://editor.p5js.org/siusoon/sketches/wmJd5awLJ

N.B: You cannot run the program because you NEED to create your **OWN API KEY and ID**

![before](beforeImg.png)

---

### 5. Exercise: Accessing Web APIs (step by step)

EVERYONE

TASK: To get the google API key and engine ID to run the sample code

You will go through:

- the Google image search API's workflow
- the API specification that indicates what data and parameters are available
- the returned JSON file format from the web API
- using the Google API by first registering the API key and search engine ID, as well as configuring the search setting

GO: 

**Step 1**: Create a p5 sketch, then copy and paste the source code to your code editor (assuming you have the html file and the p5 library).

**Step 2**: Replace the API key with your own details on the line: let apikey = "INPUT YOUR OWN KEY";.

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_3.png)
- Register a Google account if you don't have one
- Login to your account
- Go to [Google Custom Search](https://developers.google.com/custom-search/v1/overview) and find the section API key
- Click the blue button "Get A Key" and then create a new project by entering your project name (e.g. "nag-test") and press the enter key
- You should able to see the API key and you just need to copy and paste the key into your sketch.

**Step 3**: Replace the Search engine ID (cx) with your own, on the line: let engineID = "INPUT YOUR OWN";

- Go to [Custom Search Engine](https://cse.google.com/all)
- Click the "Add" button to add a search engine
- You can limit your search area but if you want to search Google entirely, just simply type "http://www.google.com"
- Enter a name of your search engine, e.g. "nag-test"
- By clicking the blue "create" button, you agree to the Terms of Service that is offered by Google (and you should know your rights of course)
- Go to the Control Panel and modify the settings of the search engine
- Copy and paste the Search engine ID and put it in your sketch

**Step 4**: Configuration in the control panel 

- Make sure the "Image search" is ON indicated by the blue color
- Make sure the "Search the entire web" is ON indicated by the blue color

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_3b.png)

Check your console.log: 

API: a long URL request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize + "&searchType=" + searchType + "&q=" + query; that includes all the credentials and items that you want to search and filter (it looks like this: https://www.googleapis.com/customsearch/v1?key=APIKEY&cx=SEARCHID&imgSize=medium&searchType=image&q=warhol+flowers).

**For those who are done:**

- change the search parameter and get different images: line 13


Note: In general most online data requires you to register for an API key, but the ways are different across platforms. You need to spend time to study how things work. 

---

### 6. Que(e)rying Data 
Understanding the data file structure - JSON format:

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_4.png)

Then items[0] points at the first data object => The dot syntax allows you to navigate to the object link under items[0]

Note: this hierarchy is specific to this API as other web APIs might structure their data and its organization differently.


### 7. Exercise in class (in group)

![api](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_5.png)

1. Can you recap what has been requested and received through the web API?
2. Change your own query strings: The current keywords are 'warhol flowers' but note that the program doesn't understand space between text and therefore it needs to be written as "warhol+flowers"
3. Add more parameters in terms of the search filter with [different parameters](https://developers.google.com/custom-search/v1/cse/list#parameters), such as adding image color type. (The URL parameters are seperated by a "&" sign like this: https://www.googleapis.com/customsearch/v1?key=APIKEY&cx=SEARCHID&imgSize=medium&searchType=image&q=warhol+flowers
4. Study the JSON file and modify the sketch to get other data such as the text by showing that onto the web console.

---

### 9. Mini Lecture

**1. Query the query** 

![api](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_5.png)

- Ask a question 
- Checking 
- Questioning the operation: Query the query

**2. Underlying processes** 

![search](https://live.staticflickr.com/1850/44482190612_bf0e23bff1_z.jpg)

(the image above shows the search result on different days according to the same keywords)

- Search engines (may be also voice assistants)
- The promise to answer all questions
- query-driven society -> powerful machines
- attention economy 
> "Through habits users become their machines: they stream, update, capture, upload, share, grind, link, verify, map, save, trash and troll"  - Wendy Chun, 2016
- The habits of searching => storable, tracable and analysable

**3. Data processing & Search Practice** 

- an extension of Data Capture 
- from physical capturing devices => data hosted on online platforms 
- Scaling => massive amounts of captured data (so-called Big Data)
- data: user-profiling, targeted marketing, personalized recommendations and various sorts of predictions and e-commerce 
- part of larger socio-technical assemblages and infrastructures 

**4. Quer(r)y Data - API** 

- real-time query of APIs
- protocol
- Querying data -> two ways communication process
- Information processing => actions of data selection, extraction, transmission, and presentation 

**5. Background of nag** 

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_1.png" width="600">

- [Female Extension](https://anthology.rhizome.org/female-extension)
- Creating fake profiles 
- The term net art generator in relation to Sol Lewitt's conceptual art ideas => hacking the art operating system 
- Critical artworks
- Latest version: politics of APIs

**6. Algorithmic Governmentality**

- determined by corporate interests 
- algorithmic governmentality (Antoniette's Rouvroy) => how our thinking is shaped by various techniques
- What's the truth? 
- subjects are produced 

**7. New normalization** 

- nag: hacking pthe process of personalization at work (without a subject)
- restrictions: blackbox processes, limiting requests, different charging schemes, liability
- Norms and the process of generalization

**8. Que(e)ry Data** 

- how data is collected, stored, analyzed, recommended, ranked, selected and curated 
- social and political implications 
- To query the power structures of materials 

**9. Thinking** 

Que(e)ry data - API - data processing - power structure 

---

### 10. To do list in coming 2 weeks: 

1. **Mandatory group work (whole group)**

- MiniX[9]: https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/Readme.md
- Due this Sun, and same for the feedback on Mon 

2. **Final Project** (https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/FinalProject.md)

- Discuss among your group and come up with some ideas: what do you want to do and why? 

3. **To clear up the previous miniEx issues**

4. **Prepare for next class** in two weeks (after Easter)

- everyone : readings 
- next group 9 presentation: Generate a list of 1000 unique and random integer between two ranges. Then implement a sorting algorithm and display them in a chronological order (You can't use the sort function in p5 or javascript). How wold you approach this problem? How would you visualize it via a flowchart? How is sorting been used in digital culture? What is sorting in both technical and conceptual sense? Think through the text on algorithm: Bucher, Taina. If...THEN: Algorithmic Power and Politics, Oxford University Press, 2018, pp. 19-40 (The chapter called "The Multiplicity of Algorithms")