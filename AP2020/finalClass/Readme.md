## last class on 12 May - 12.45-14.15

## All presentations (questions?)

| Time        | Group                             
| ------------|:------------------------------------------  
| 09.00-09.15 | Group 10            
| 09.20-09.35 | Group 9                                 
| 09.40-09.55 | Group 8                              
| 10.00-10.15 | Group 7                              
| BREAK
| 10.25-10.40 | Group 6     
| 10.45-11.00 | Group 5     
| 11.05-11.20 | Group 4                             
| 11.25-11.40 | Group 3 
| BREAK
| 11.50-12.05 | Group 2
| 12.10-12.25 | Group 1
| (LUNCH) BREAK
| 12.45-14.15 | ALL


## The course (20 ECTS)

“Aesthetic Programming” is a practice-oriented course requires no prior programming experience, **exploring the relationship between code and cultural and/or aesthetic issues related to the research field of software studies**. The purpose of the course is to enable the students to design and programme a piece of software, to understand programming **as an aesthetic and critical action which extends beyond the practical function of the code**, and **to analyse, discuss and assess software code** based on this understanding. The course has links to Software Studies on the same semester, and the purpose of the collaboration between these two courses is to provide specific experiences with **programming as a reflected and critical practice**.
In the course, programming practices are considered as a way to describe and conceptualise the surrounding world in formal expressions, and thus also as a way to understand structures and systems in contemporary digital culture. We will use P5.js primarily, which serves as a foundation for further courses on Digital Design.

## The Course - competence / learning outcomes

Knowledge:
- Demonstrate insight into and reflect on programming as a practice that allows you to explore, think about and understand contemporary digital culture
- Demonstrate insight into programming and programming principles by writing, reading and processing code

Competences:
- Read and write computer code in a given programming language
- Use programming to explore digital culture
- Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class07/ap.png)

## Oral exam - RUNDOWN

prerequisite/rules: 
- webcam available 
- Zoom as the main and skype business as the backup
- Audio and/or video recordings of oral online exams are not permitted
- all exams will be conducted between the hours of 08:00 and 16:00. 
- check out details at the student portal: https://studerende.au.dk/en/studies/subject-portals/arts/corona-information-from-the-faculty-of-arts/online-exams-at-the-faculty-of-arts/

0. You will be at the waiting room, once you are accepted in the exam room, then **show your student id** before the exam starts

1. Start with a 5 mins presentation (Prepare a **5 mins presentation** based on the **pre-assigned question**: “Draw a concrete technical example (a small block/line of code) from your final project and discuss any one of the aesthetic aspects of code that informs your critical reflection on digital design/culture.”)

2. When you are done with the presentation on the pre-assigned question, the examiners will follow by talking about one of the topics below. A really good tip would be to prepare something specific to say about each topic (based on the readings, weekly mini-exercises, themes and course materials). You are welcome to show slides, code snippets, your mini exercises and other experiments, or other forms that you think could help articulate your thoughts within the realm of Aesthetic Programming. We will use this part of the exam to go into depth with a specific aspect of the course. Don’t prepare more than 5 mins for each topic; short but precise.

- Geometry and shapes
- Object-oriented programming
- Objects
- Variables, arrays, 
- JSON
- APIs
- Loop
- Time-related syntax
- Conditional statements/structures
- Algorithms
- DOM elements, specifically forms related objects: button, input box, slider, checkbox, etc
- Randomness
- Rules
- Language
- Any one of the JavaScript libraries that you have used
- Any one of the data capture and feedback syntax (e.g keyboard, mouse, DOM objects, audio input, face tracker, etc) 

3. Questions will arise by examiners based on your presentation and your previous submitted final project. We might only discuss on one of the topics above, but we might discuss more. This all depends on the conversation. Eventually, we will perhaps also discuss things from the course that are not listed above but are still relevant to the course. All the discussion should be thought of as from within the realm of Aesthetic Programming. 

4. Within 5 mins discussion within examiners only (you will be directed to Zoom’s “Waiting room”. Do not leave the Zoom meeting while you are waiting for your grade. You will be invited back into the meeting when the time comes to give you your grade. )

5. Within 5 mins feedback on grade and exam performance

**CONCERNS**

1) Notes:
- If there is a systems breakdown before or during an exam, the student must contact you by phone. The phone number can be found in the Digital Exams system under the exam in question.
- Zoom + skype for business 
- If it proves impossible to establish a connection within five minutes, you must terminate the exam and contact the student by email or phone to arrange a new time for the exam. This new time will be on the same day, after the planned exams have been completed.
- If the exam is terminated owing to a systems breakdown, this will not be regarded as a failed attempt to pass it. This is the case whether the breakdown occurs before or during the exam. And it does not matter whether the breakdown is due to the student’s system or the system used by the examiner/co-examiner.  The student is not required to produce evidence of any systems breakdowns occurring either before or during the exam.

2) hotline for exams:

The hotline has two entrances and supports both teachers and students.
* Admin issue- The Arts Studies Exam Hotline: The hotline is open daily 8-16 (May 11 through the end of June). The exam hotline is contacted on 8716 1060.
* Arts IT Support is open daily 8-16, if technical problems are needed. IT Support is contacted on 8715 0911.
 

## Final evaluation 
- link on the blackboard
- a report will be prepared, your signature is needed
- miniX + instructor class + lecture
- online/physical

## Aesthetic Programming 

- [Postcards from Isolation](https://isolation.is/)

- Code and Share website: https://codeandshare.net/ 

- Code and share facebook group: https://www.facebook.com/codeAndShareAarhus/

- Aesthetic Programming: A Handbook of Software Studies: 

<img src="https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/AP2020/finalClass/ap_book.png" width = 300>

How to use it in your whole program study in Digital Design: 

- As a methodology: embrace both formal and disursive meanings (Critical Technical Practice)
- read/write/run/think/design/contextualize with and about code/program as methods
- Relationship with design and computational culture

https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source

> The argument the book follows is that computational culture is not just a trendy subject to study and to improve problem solving and analytical skills, or a way to understand more about what is happening with computational processes, but a means to engage with programming as a way to question existing technological interfaces and further create changes in the technical system. Thus we consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means to understand some of the complex procedures that underwrite our lived realities, in order to act upon those realities.

> we have been working with fundamental concepts of programming as the starting point for further aesthetic reflection — such as geometry and object abstraction; variables, data types, functions and namings as well as data capturing, processing and automation — thereby the technical functionings are setting the ground works for further understanding of how cultural phenomena is constructed and operationalized. Aesthetic Programming in this sense is considered as a practice to build things but also with the need to produce "reflexive work of critique". This comes close to Philip Agre's notion of "critical technical practice" with the drawing together of two different perspectives: formal technical logic and discursive cultural meaning. In other words, this necessitates practical understanding and knowledge of programming to underpin critical understanding of techno-cultural systems, underpinned by levels of expertise in both fields — as in the case of Wendy Hui Kyong Chun. We hope to encourage more and more people to defy the separation of fields in this way.

## what would you recommend for the next cohort? 

https://pad.riseup.net/p/AP2020-keep