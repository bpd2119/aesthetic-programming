### Class 06: Pause Week

**Messy notes:**

**Agenda:**

- Mini-ex[4] short discussion (Sophia Alexandra McCulloch)
- Group 3 presentation: image/video/audio glitches...
- Group 4 presentation: Reflection on why we need to code...
- Basic Concepts revisit
- [Reflecting what's aesthetic programming, and programming as a practice and method](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/class06/AP_reading.md)
- [Further discussion on Aesthetic Programming](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/class06/AP_reading2.md)
- Mini-ex walk-through: [Revisit the past](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex) | due SUN mid-nigh
- Next week group presentation - (Week 11: 3D objects in p5js + think through the text: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries)
