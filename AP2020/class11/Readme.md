## Class 11: Algorithmic Procedures

(adjusted according to the immediate introduction of online teaching)

**Messy notes:**

**Agenda:**

1. Mini-ex[9] short discussion 
2. Group 9 presentation: Sorting...
3. Mini lecture + next miniX
4. Discussion in class
5. Flow Charts
6. Exercise + Discussion in class  
7. FLowcharts as artistic medium
8. Final Project Discussion 

---

### 1. Mini-ex[9] short discussion 

- API vs JSON
- Complexity of data processing

---

### 2. Group 9 presentation: Sorting...

For everyone as a group (8 mins):

**THINK ABOUT SORTING!!!!**

> Generate a list of 1000 unique and random integer between two number ranges. Then implement a sorting algorithm and display them in a chronological order (You can't use the sort function in p5 or javascript). Think about how you would approach this. (Perhaps draw it out as a flowchart)


Group's brief: 

Generate a list of 1000 unique and random integer between two number ranges. Then implement a sorting algorithm and display them in a chronological order (You can't use the sort function in p5 or javascript). How would you approach this problem? How would you visualize it via a flowchart? How is sorting been used in digital culture? What is sorting in both technical and conceptual sense? Think through the text on algorithm: Bucher, Taina. If...THEN: Algorithmic Power and Politics, Oxford University Press, 2018, pp. 19-40 (The chapter called "The Multiplicity of Algorithms")

1. Agnete Lystbæk Gjesse 
2. Freyja Viskum Madsen 
3. Stine 
4. Olivia 

note: https://editor.p5js.org/siusoon/sketches/7g1F594D5

---

### 3. Mini Mini lecture + next miniX

**1. Cookbook and codebook**
- practice of coding and cookinng 
- how ingredients are seected, action applied, and how transformations take place

**2. Algorithms** 
- More than simple steps and procedural operations
- Operations of Algorithms
- Understand operative dimensions
- “fundamentally productive of new ways of ordering the world" - Bucher
- Algorithm vs lines of code 
- Skeleton
- Systematic breakdown of procedural operations

**3. Flowcharts**
- explanatory tool 
- representational diagrams 
- communication
- used in many disciplines ([1](https://webhelp.episerver.com/15-1/en/Content/_Images/Commerce/ShopWorkflow.png), [2](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/9-AlgorithmicProcedures/ch9_5.jpg))
- challenges:
    - To translate programming syntax and functions into understandable plain language.
    - To decide the level of detail to show the important operations that allow other people to understand the logic of your program.

=> this week [miniX](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/Readme.md) 

---

### 4. Discussion in class

* Based on the assigned reading from Taina Bucher, can you list some of the properties of algorithms? How is it both technical and social?
* We discussed rule-based systems in the week - Auto Generator, how is that different from the way we are discussing procedure here? (rules vs procedures)

---

### 5. Flowcharts 

Conventionally, each step in a flowchart is represented by a symbol and connecting lines that guide the flow of logic progressively towards a certain output. Basic components of drawing a typical flowchart:

- **Oval**: Indicates the start or end point of a program/system. (But this requires further refection on whether all programs have an end.)
- **Rectangle**: Represents the processual steps.
- **Diamond**: Indicates the decision points with yes and no branches.
- **Arrow**: Acts as a connector to show the relationship and sequence, but sometimes an arrow might be returned to a previous process, especially when showing repetition and loops.

Example: 

[Vocable Code - RUNME](https://siusoon.github.io/VocableCode/vocablecode_program/) 
![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/9-AlgorithmicProcedures/ch9_1.png)


---

### Exercise in class

Let's start with something that appears to be relatively simple. Draw a flowchart based on the program code below:

```javascript
function setup() {
  var sometext = ['h','e','l','l','o'];
  for (var i=0;i < sometext.length; i++) {
    console.log(sometext[i]);
  }
}

/*output
h
e
l
l
o
*/

```

**Task 1: For each group, draw a flowchart (take a photo) and then put it up on [padlet](https://padlet.com/siusoon/iaaabkoo9qey) with the **title** "Class 11 - flowcharts from Group[x]".**

- challenges:
    - To translate programming syntax and functions into understandable plain language.
    - To decide the level of detail to show the important operations that allow other people to understand the logic of your program.

NB: https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/source/9-AlgorithmicProcedures/flow.png

**Task 2: Discussion**

We have seen three flowcharts today:

1. Sorting 
2. Vocable Code 
3. Task 1 above

Quotes from Nathan Ensmenger's article "The Multiple Meanings of a Flowchart", discuss within your group and articulate what they mean:

"To view the computer flowchart as having only one purpose is narrow and misleading, [because] every flowchart had multiple meanings and served several purposes simultaneously";

"Flowcharts allow us to 'see' software in ways that are otherwise impossible."

--- 

### 7. FLowcharts as artistic medium

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/9-AlgorithmicProcedures/ch9_2.gif)

 GWEI - Google Will Eat Itself / THE ATTACK (2005) by Cirio in collaboration with Alessandro Ludovico and UBERMORGEN
 
link: http://www.gwei.org/index.php 
 
![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/9-AlgorithmicProcedures/ch9_3.gif)

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/9-AlgorithmicProcedures/ch9_4.jpg)

The Project Formerly Known as Kindle Forkbomb (2012) by UBERMORGEN

---

### 8. Final Project Discussion 

Group discussion and check-in
