## Class 05: Data Capture

**Messy notes:**

**Agenda:**

1. Mini-ex[3] short discussion 
2. Group 2 presentation: DOM elements...
3. Mini-Lecture 
4. Sample Code + Exercise in class
5. DOM elements
6. Mouse and Keyboard capture
7. Audio capture and Face tracker
8. Exercise in class 
9. The concept of capture
10. Mini-ex walk-through: Capture all | due SUN mid-night

---

### 1. Mini-ex[3] discussion  

Magnus Bak Nielsen
Amanda Hansen

---

### 2. Group 2 presentation: DOM elements...

DOM elements and other form control elements (https://p5js.org/reference/#group-DOM). Using the Button's text from Software Studies as a reference/departure point, then pick one other form control/DOM element and describe/analyze the specificity of that (such as radio button, slider, checkbox, etc). Go deep to think about the aesthetic issue)

1. Anne Nielsen
2. Pernille Johansen
3. Simon 
4. Torvald Pockel

---

### 3. Mini-Lecture 
- Data Capture vs Interactivity 
- What kinds of data is being captured, how it is being processed, and the wider consequences of this 
- Binary button 
- Capture
- Transmediale - [capture all](https://transmediale.de/content/call-for-works-2015)
- individuation and transindividuation 
---

### 4. Sample Code + Exercise in class
![](https://gitlab.com/siusoon/aesthetic-programming/raw/master/Ap2019/class04/sketch04.gif)

Go to the RUNME of class05: http://siusoon.gitlab.io/aesthetic-programming/

**Exercise 1: Speculation:**
Based on what you see/experience on the screen, describe:

- What are the elements and interactions? List the items and actions.

Next: Try mapping those with the source code. 

---
### 5. DOM elements

Steps to create buttons: 

1. `let button;`: declare by assigning a name
2. `button = createButton('like');`: create a bbutton and consider the button text 
3. `button.style("xxx":"xxx");` CSS styling

**Exercise:**
By looking at the Like button closely in the RUNME, can you come up with a list of potential customizations that have been introduced in the sample code?

Next: Try to look at the source code 

4. `button.mousePressed();`: listening events 
5. `button.size();`: sizing  -  in terms of width and height 
6. `button.position();`: set the button's position
---

### 6. Mouse and Keyboard capture 

```javascript
button.mousePressed(clearance);  
//click the button to clear the screen

function clearance() {
  clear();
}
```
The function `mousePressed()` is attached to the button you want to trigger actions. There are other mouse related mouseEvents, such as `mouseClicked()`, `mouseReleased()`, `doubleClicked()`, `mouseMoved()`, and so on. (See the related function in the reference page, which is under Events > Mouse> https://p5js.org/reference/)

```javascript
function keyPressed() {
  if (keyCode === 32) { //spacebar - check here: http://keycode.info/
    button.style("transform", "rotate(180deg)");
  } else {   //for other keycode
    button.style("transform", "rotate(0deg)");
  }
}
```

The use of the `keyPressed()` function is to listen any keyboard pressing events. If you want to specify any `keyCode` (that is the actual key on the keyboard), the sample code shows how a conditional statement can be implemented within the `keyPressed()` function. Within the if-else conditional statement, if a keyboard press of a spacebar is detected, then the button will rotate through 180 degrees and any other keys will just resume back to the original state of 0 degrees.

`keyCode` takes in numbers or special keys like BACKSPACE, DELETE, ENTER, RETURN, TAB, ESCAPE, SHIFT, CONTROL, OPTION, ALT, UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW. In the above example, the `keyCode` of a spacebar is 32. (check: https://keycode.info/)

Capital and lower case letters do not make any difference as they are using the same `keyCode`, such that 'A' and 'a' use the same number 65. 

Similar to `mouseEvents`, there are also many other `keyboardEvents`, such as `keyReleased()`, `keyTyped()`, `keyIsDown()`. 

---
### 7. Audio capture and Face tracker

```javascript
let mic;

function setup() {
  // Audio capture
  mic = new p5.AudioIn();
  mic.start();
}

function draw() {
  //getting the audio data
  let vol = mic.getLevel(); //get the overall volume (between 0 and 1.0)
  button.size(floor(map(vol, 0, 1, 40, 500))); //as the button is too big, check map function: https://p5js.org/reference/#/p5/map
}
```

check: https://p5js.org/reference/#/libraries/p5.sound

Similar to a button, you first declare the object, e.g. `let mic;`, and then set up the input source (commonly from a computer microphone) and start to listen to the audio input (see the two lines within `setup()`). When the entire sample code is executed, a popup screen from a browser will ask for permission to access the audio source. This audio capture only works if access is granted.

`p5.AudioIn();`: reads the amplitude (volume level) of the input source with the return value between 0 to 1.0 by using the method `getLevel()`.

`map()`: map a number across a range. Since the return of the volume is set between a range of 0 to 1.0, the corresponding number will not make a significant difference in terms of the size of the button. As such, the range of the audio input will then map to the size range of the button dynamically. 

![](https://www.auduno.com/clmtrackr/examples/media/facemodel_numbering_new.png)

The library: https://github.com/auduno/clmtrackr

For face capture, the sample code has used the clmtrackr which is a JavaScript library developed by a data scientist Audun M. Øygard in 2014 for fitting a facial model to faces in images or video.Based on facial algorithms designed by Jason Saragih and Simon Lucey,the library analyses a face and divides it into 70 points in real-time based on a pretrained machine vision model of facial images for classification. 

see: https://www.auduno.com/2014/01/05/fitting-faces/ and Jason M. Saragih, Simon Lucey and Jeffrey F. Cohn, "Face Alignment Through Subspace Constrained Mean-shifts", 2009 IEEE 12th International Conference on Computer Vision, Kyoto (2009): 1034-1041. doi: 10.1109/ICCV.2009.5459377.

But you can fork/download the [code repository](http://siusoon.gitlab.io/aesthetic-programming/) for experimentation now: 

```javascript
let ctracker;

function setup() {
//web cam capture
let capture = createCapture();
capture.size(640,480);
capture.position(0,0);

//setup tracker
ctracker = new clm.tracker();
ctracker.init(pModel);
ctracker.start(capture.elt);
}

function draw() {
let positions = ctracker.getCurrentPosition();
if (positions.length) { //check the availability of web cam tracking
    button.position(positions[60][0]-20, positions[60][1]); //as the button is too big, place it in the middle of my mouth, and 60 is the mouth area (check lib spec)
    for (let i=0; i<positions.length; i++) {  //loop through all major face tracking points
       noStroke();
       fill(map(positions[i][0], 0, width, 100, 255), 0,0,10);  //color with alpha value
       ellipse(positions[i][0], positions[i][1], 5, 5);
    }
}
}
```

What the program does in terms of face capture and facial recognition: 
1. `createCapture()`: This is a HTML5 <video> element that captures the feed from a web camera. In relation to this function tasks might be to define the size (which is subjected to the resolution of the web camera) and position on screen, e.g. `capture.size(640,480);` and `capture.position(0,0);`
2. The three lines related to ctracker: `ctracker = new clm.tracker()`, `ctracker.init(pModel);` and `ctracker.start(capture.elt);`: Similar to audio and camera use, first you need to initialize the tracker library, select the classified model and start tracking from the video source. 
3. `ctracker.getPosition()`: While we get the tracker points into an array position, there is a for-loop that has been implemented to loop through all the 70 tracker points and return the position in terms of x and y coordinates as a two-dimension array in the form of `position[][]`. The first dimension ([]) of the position array is to indicate the tracker points from 0-70. The second dimension ([][]) is to get the x and y coordinates of the tracker points. Since the Like button will follow the mouth of the face and the tracker point of a mouth is 60, the program then will return the position in terms of an array: `positions[60][0]-20` and `positions[60][1]`. The second array's dimensions of [0] and [1] refers to the x and y coordinates specifically.
4. The last part is to draw the ellipses to cover the face. A for-loop is implemented to loop through all the ctracker points and then get the x and y coordinates for the drawing of the ellipses. 

---
### 8. Exercise in class 

To familiar yourself with different modes of capture, try the following:
1. Explore the different modes of capture by tinkering with various parameters such as `keyCode`, as well as other keyboard and mouse events. 
2. Study the tracker points and try to change the position of the Like button that you previously customized in the earlier exercise. 
3. Try to test the boundaries of facial recognition: to what extend can(not) a face be recognized as a face?
4. Do you know how the face is being modelled? How has facial recognition technology been applied in society at large, and what are some of the issues that arise from this? 

It would be worth checking back to Variable Geometry for a reminder of how facial recognition identifies a person's face from its geometry — such as the distance between a person's eyes or size of mouth — to establish a facial signature that is comparable to a standardised database. Not least of the problems is that the database is itself skewed by the ways in which the data has been prepared, in its selection, collection, categorization, classification and cleaning. To what extent does your face meet the standard? 

---

### 9. The concept of capture

**Web Analytics and Heatmap**
![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/4-DataCapture/ch4_4.png)
*Screenshot of Google Analytics*

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/4-DataCapture/ch4_7.png)
*An example of a heatmap for analysing a web page*

**Form elements**

<img src="https://journals.sagepub.com/na101/home/literatum/publisher/sage/journals/content/nmsa/2017/nmsa_19_6/1461444815621527/20170607/images/large/10.1177_1461444815621527-fig4.jpeg" width="400">

*Timeline of gender-related changes to Facebook’s sign-up page by Rena Bivens*


![](genderFB.png)

*The custom gender field of Facebook as of Feb. 2020*

**Metrics of likes**

![](https://bengrosser.com/wp-content/resources/fbd-home-master-530x291.gif)

*Benjamin Grosser's Facebook Demetricator Demetricating Likes, Shares, Comments, and Timestamps Original (top), Demetricated (bottom).*

https://bengrosser.com/projects/facebook-demetricator/

**Voice and audio data**

![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/4-DataCapture/ch4_6.png)

**Health tracker**

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/4-DataCapture/ch4_5.png" width="300">
 
---

### 10. Mini-ex walk-through: Capture All | due SUN mid-night

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex

### 10.1 Next class presentation - max 20 mins

Group 3: 

image/video/audio glitches  in p5.js, thinking through micro(temporarily) with the text: Soon, Winnie. Throbber: Executing Micro-temporal Streams, 2019

Group 4: 

Reflection on why we need to code + Aesthetic Programming with your experience of coding- refer to first week and this week readings and your processes

*with your own sample code/experimentation - address both the technical understanding and the conceptual thinking with the text*


*reflection:*
- path for the trackr lib 
- looping the tracker points 
- 2d arrays <-> x and y coordinate 