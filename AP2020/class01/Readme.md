## Class 01: Getting Started 

**Messy notes:**

**Agenda:**
1. Introducing each other 
2. Discussion: Why we need to learn programming?
3. Overview: Syllabus, class and learning, expectation
4. What is JavaScript and why p5.js?
5. Setting up the working environment (p5.js lib + Atom code editor)
6. Run the first p5.js program
7. In class exercise
8. Web console + reading reference guide
9. Mini-ex [1] walk-through: My First Program | due SUN mid-night
10. Git

---

### 1. Introducing each other 
Teaching team: 
- [Winnie Soon](http://www.siusoon.net)
- [Ann Karring](https://gitlab.com/RaggedyAnn)
- [Noah Sanchez Echevarria Aamund](https://gitlab.com/Aamund)

About you:
- Who are you? 
- Do you have any programming/scripting experience? If yes, what are they? 
- What are your "machine feelings" towards code? 

---

### 2. Discussion: Why we need to learn programming?

**short lecture**
- technical vs cultural (STEM vs STEAM)
- literacy (Vee, 2017)
- programming > wider social relations (Vee, 2017)
- Exploratory programming for arts and humanities  (Montfort, 2016)

**Discussion in pairs:**  *write your thoughts here: https://padlet.com/siusoon/iaaabkoo9qey*
- Why do you think you need to know programming? 
- Why is it important to know programming in the Digital Design programme?

---

### 3. Overview: Syllabus, class and learning, expectation

- Forming group, learning outcomes, Wed/Fri session, weekly themes, miniExes + feedback, Peer presentation, Expectation
https://gitlab.com/siusoon/aesthetic-programming/tree/master/AP2020
- PCD @ Aarhus on 22 Feb (SAT) (http://www.pcdaarhus.net)
---

### 4. What is JavaScript and why p5.js?

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_1.png">

- open source 
- web based scripting language: run, read and execute on a web browser 
- Javascript vs Java 
<img src="https://regmedia.co.uk/2013/01/30/java_logo.jpg">

- Code execution 
<img src="https://2r4s9p1yi1fa2jd7j43zph8r-wpengine.netdna-ssl.com/files/2017/02/02-02-interp02.png" width="400">

- p5.js (2014) -> put focus on diversity and inclusivity 

---

### 5. Setting up the working environment 

p5.js + ATOM (code editor) 

**p5.js**

1. First go to the download page of p5.js (https://p5js.org/download/) and get the p5.js complete library (in the compressed format of 'p5.zip') by clicking it and saving the file which includes all the necessary libraries to run the code.
2. Double click the zip and unzip the file to extract all the files associated with the compressed one, then you should see that another new folder is created called 'p5'.
3. The next part is crucial for the on-going development process, because you have to somehow decide where your working folder will be located. If you have no idea, you may consider using the 'Desktop' folder. (Foldering is a concept used for organizing files in your device, which is similar to organizing papers, folders, books on a bookshelf. With the increasing streamlined UX design, many people find it alienating to navigate or locate the path and directory of files, like pictures, in a device as we are getting more used to putting everything on the first few pages of a phone or simply on the desktop.)
4. If you put the unzipped folder 'p5' in a customized directory, then you should see the list of files in the folder as below. You should see the two p5.js libraries, one is the complete one (p5.js) and the other is the 'mini' version of it (p5.min.js).
<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_2.png">

5. Click on the folder 'empty-example', and then you will see a list of files you will need to begin:
<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_3.png">

*  **index.html** - the default Hypertext Markup Language (HTML) which will be first to be picked up by a web browser. HTML is a fundamental technology used to define the structure of a webpage and it can be customized to include text, links, images, multimedia, forms, and other elements.
*  **sketch.js** - the key working file for writing JavaScript. The use of the word 'sketch' is similar to drawing a sketch in visual arts, which is less formal to use a sketch to work out or capture ideas and experiment with composition.
*  **p5.js** - the p5.js core library.
*  **p5.sound.js** - the p5.js sound library for web audio functionality, including features like playback, listening to audio input, audio analysis and synthesis.  

<img src="https://user-content.gitlab-static.net/225bfa13d9a6cb7cfa37ab445066e2ae92269122/68747470733a2f2f6d646e2e6d6f7a696c6c6164656d6f732e6f72672f66696c65732f31333530342f657865637574696f6e2e706e67">

**ATOM (code editor)**

ATOM is used as the key code editor for this course. It supports cross-platform editing which can be run on Mac OS, Windows and Linux.
1. Download the software ATOM from the homepage: https://atom.io/
2. Drag the 'p5' folder that you have just unzipped onto ATOM. You should able to see the left-hand pane with your project. Then you try to navigate to the 'index.html' file under the 'empty-example' folder, double click that file and the source code should display on the right-hand pane. See below:
<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_4.png">

- The 'index.html' is the default page, amongst all other pages and files, which the web browser will display.
- You can customize the page title and other styling issues
- Since `p5.js` is a library, here the lines 8-10 indicate how to incorporate JavaScript files and libraries by using the tags `<script>` and `</script>`.
- the concept of 'path' and 'directory'

Next you will need to install a package called 'atom-live-server' and this is useful for setting up a local web server so you can update your code and see the results immediately on a browser without the need to refresh it. You can first check under 'Packages' on your menu bar and see if the package is there. If not, then go to Edit > Preferences > +Install, then type 'atom-live-server'. Hit the blue install button and you should able to find it again under the Packages menu.

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_5.png">

*If you want to customize the theme like the background color of the panes, simply go to Preferences > Themes.*

---

### 6. Run the first p5.js program

Task: To be able to run the first program on your own computer with ATOM: 

- Sample code repository - sketch01: http://siusoon.gitlab.io/aesthetic-programming/  -> copy the js code (sketch01.js) and paste at your working sketch
- functions: `setup()` & `draw()`
- run the code: ATOM > menu bar >  Packages > atom-live-server (or you can use the shortcut Crtl + Alt/Option + L). Then you will see a popup window, click on the 'empty-example' folder 

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_6.png">

---
### 7. In class exercise

Reconfigure the "path" and "folder":

1. Stop the atom-live-server by going to Packages > 'atom-live-server' > Stop (or to use the shortcut Ctrl + Alt/Option + Q)
2. Try to rename the folder 'empty-example' to 'myFirstSketch' (in order to help the computer to process better, don't use space in between)
3. Try to create a folder called 'libraries' under 'myFirstSketch'
4. Drag the two p5 libraries into the newly created folder: 'libraries'
5. Change the relative path of the two js libraries in index.html
6. Change the title in the HTML file (line 6)
7. Can you run the program so that you can see almost the same screen as before on a web browser?

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_7.png">

This exerise is to get familiarize you with the path and local directory so as to know that running a sketch on a web browser requires loading the right path of the JavaScript libraries. You are also free to create your own folder name and rename the file like sketch.js as you please. You may also try to change the parameters of the numbers to get a sense of how things work.

Can you see how the folder structure works on the sample code for this course? https://gitlab.com/siusoon/aesthetic-programming/tree/master/public

What happen if the libraries folder is outside your sketch folder? 

---
### 8. Web console + reading reference guide

- All the syntax check: `createCanvas()`, `background()`, `random()`, `ellipse()`,`print()` .
- reference guide (walkthrough together): https://p5js.org/reference/  > `ellipse()`
<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/raw/master/source/1-GettingStarted/ch1_11.png" width=650>

- Web console: To see the result of line 4 `print("hello world);` text, you need to open the web console area: navigating the menu bar. In the Firefox browser, for instance, it is located under Tools > Web Developer > Web Console (check the short cut).
- Hello World Program - natural language

---
### 9. Mini-ex [1] walk-through: My First Program | due SUN mid-night

Walkthrough [1]: My First Program + feedback arrangement 

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex

---
### 10. Git

- What's Git?
- Gitlab as open source web-based repository platform and a social platform

Steps for the account and repository creation: 

1. Register an account by clicking 'Register' at the navigation bar (https://gitlab.com/) 
2. To create new project: Go to Projects > New Project
3. Give a projet name and a project description, and click Public if this project can be accessed by others without any authentication.
4. At this point you can also initialize a README within the repository by ticking the checkbox.
5. A folder in your repository then should be created

To upload the file or create a directory, just simply click on the '+'' sign under the repository project name that you have just created. 

---

Reflection:
- better with 5 hours for the first class to cover why p5.js conceptually and lauren's video
- need to address readme and runme concretely: two things, and how they work together.
- if have time, can talk about git, gitlab, uploading files, web hosting of gitlab (.io)