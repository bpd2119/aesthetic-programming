## Class 07: Object Abstraction

**Messy notes:**

**Agenda:**

1. Mini-ex[4] short discussion 
2. Group 5 presentation: 3D objects...
3. Mini-Lecture 
4. Pseudo Objects
5. Tofu Go! by Francis Lam
6. Source Code + Exercise
7. class-object creation 
8. Exercise in class
9. Mini-ex walk-through: Games with Objects | due SUN mid-night
10. Mid-way evaluation
---

### 1. Mini-ex[4] discussion 

Line Stampe-Degn Møller

---

### 2. Group 5 presentation: 3D objects...
Week 11: 3D objects in p5js + think through the text: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries)

---

### 3. Mini Lecture
- object manipulation
- Geometric objects
- The notion of abstraction 
- Computational abstraction at different layers 

![abstraction](https://miro.medium.com/max/572/1*qJ7PA_c-5F4GVW96oNl41g.jpeg) 

Source: https://medium.com/@twitu/a-dive-down-the-levels-of-abstraction-227c96c7933c

![abstraction2](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class06/abstraction.png)

Source: https://www.coursehero.com/file/12999936/Chapter-1/

- object-oriented modeling of the world is a social-technical practice (Fuller & Goffey)
- perceiving the world

---

### 4. Pseudo objects

Object Abstraction in computing is about representation. Certain attributes and relations are abstracted from the real world, but at the same time details and contexts are left out. Let's imagine a person as an object (rather than a subject) and consider what properties and behaviors that person might have. We use the name **class** to give an overview of the object's properties and behaviors.

For example:

**Properties**: A person with the **name** Winnie, has black **hair color**, **wears** no glasses with **height** as 164 cm. Their **favorite color** is black and their **favorite food** is Tofu.  

**Behavior**: A person can run from location A (Home) to location B (University). 

From the above, we can construct a pseudo class that can use to create another object with the following properties and behaviors:

|Person                  |
| ---------------------- |
| Name, HairColor, withGlasses, Height, FavoriteColor, FavoriteFood, FromLocation, ToLocation |
| run()                  |

In the same token, we can *reuse* the same properties and behavior to create another *object instance* with the corresponding data values:

| Object instance 1             | Object instance 2         |
|-------------------------------|---------------------------|
| Name = Winnie                 | Name = Geoff              |
| HairColor = Black             | HairColor = Brown         |
| withGlasses = No              | withGlasses = Yes         |
| Height = 164 cm               | Height = 183 cm           |
| FavoriteColor = Black         | favoriteColor = Green     |
| FavoriteFood = Tofu           | FavoriteFood = Avocado    |
| FromLocation = Home           | FromLocation = University |
| ToLocation = University       | ToLocation = Home         |
| run()                         | run()                     |


---

### 5. Tofu Go! by Francis Lam

![tofu](https://user-content.gitlab-static.net/c4e46670437799a250ad4813501806d8a6973fa5/687474703a2f2f7061796c6f61643332362e636172676f636f6c6c6563746976652e636f6d2f312f31372f3536333533342f383832333439312f6970686f6e65305f3637302e706e67)

[embeded video](https://www.youtube.com/watch?v=V9NirY55HfU)

Tofu Go! (2018), a game developed and designed by Francis Lam (HK/CN).

About Francis: Francis Lam is a designer, new media artist and programmer currently based in Shanghai. He was brought up and educated in Hong Kong. After his studies in Computer Science and Graphic Design, he moved to Boston and received his Master’s degree in Media Arts and Sciences from the MIT Media Lab. Francis is interested in the social aspects of interactive media and new interfaces for computer-mediated communications. His new media arts and installations have been exhibited and awarded worldwide. In 2008, he moved to Shanghai and joined Wieden+Kennedy as the Interactive Creative Director and Head of Interactive. Francis has created many digital and interactive work for clients such as Nike, Estée Lauder, Tiffany, Levi’s, Coca Cola and Unilever that drew the attention of millions of consumers in China. He is now the Chief Innovation and Technology Officer at Isobar China and leading Nowlab, the in-house R&D unit.

Interview: [Francis Lam](https://www.design-china.org/post/35833433475/francis-lam)
 
When tofu becomes a computational object, as in Tofu Go!, abstraction is required to capture the complexity of processes and relations, and to represent what are thought to be essential and desired properties and behaviours. In the game, tofu is designed as a simple white three-dimensional cube form with an emoticon, and the ability to move and jump. Of course in the real world tofu cannot behave in that way, but one can imagine how objects can perform differently when you program your own software, and if you love tofu as Lam does: "Tofu Go! is a game dedicated to my love for tofu and hotpot" as he puts it. You need to save the tofu from the chopsticks.

---

### 6. Source Code + Exercise

https://siusoon.gitlab.io/aesthetic-programming/ > sketch06
(idea from Francis Lam and Pernille: https://gitlab.com/pernwn/ap2020/-/tree/master/public%2FMX4)

**Exercise**

*SPECULATION*

Based on what you see/experience on the screen, describe:
- What are the elements? 
- What are moving and not moving? (and how do they move?)
- What are the instructions/rules to play the game?
- Can you describe the algorithm (logical procedure) of the game? 
- Task: Come up with a list of features.

Further questions to think about:
- How to insert tofu continuously in the screen?
- Under what conditions the game will be ended? 


*MAPPING with the source code*

Look at the source code: The repository includes two files sketch06.js and Tofu.js

1. Map some of the findings/features from the Speculation that you have done with the source code. Which block of code relates to your findings?
Can you identify the part/block of code that responds to the elements that you have speculated on earlier?
2. Identify the syntax and function that you might not know and check out on p5.js reference site: https://p5js.org/reference/


**Note:**
- How to incorporate seperate js file to better manage your overall sketch:

```javascript
//in your html file
  <script language="javascript" type="text/javascript" src="sketch06.js"></script>
  <script language="javascript" type="text/javascript" src="Tofu.js"></script>
```

Why we need seperate js files? 

---

### 7. class-object creation 

It requires some planning before you start coding if you want to implement a class-object in your own program. In a nutshell, an object consists of attributes/properties and actions/behaviors, and all these hold and manage data in which the data can be used and operation can be performed. 

To first construct objects in OOP, it is important to have a blueprint. A class specifies the structure of its objects' attributes and the possible behaviors/actions of its objects. Thus, class can be understood as a template and blueprint of things.  

Similar to the template that we had for a person-object, we have the following:

|Tofu                                              |
| ------------------------------------------------ |
| speed, xpos, ypos, size, toFu_rotate, emoji_size |
| move(), show()                                   |

- **(Step 1) Naming**: Give a name for your class.
- **(Step 2) Properties**: What are the attributes/properties of tofu?
- **(Step 3) Behaviors**: What are the actions/behaviors of tofu? 
- **(Step 4) Object creation**: After the basic setup of the class structure, next is to create a tofu object that can display on screen. 
- **(Step 5) Display**: How you want to display and present the tofu object over time?
- **(Step 6) Trigger point**: Think with the holistic logic.

It is by no means saying the steps should be in the exact sequence as stated. Of course one might think about a program or a game in a holistic way at the beginning and come up with the different object instances afterwards. As such, the steps is just a suggested guideline especially for beginners who are encountering class-object creation in their first time. 

https://editor.p5js.org/siusoon/sketches/HAYWF3gv

**(Step 1) Naming: Give a name of your class**

```javascript
class Tofu {

}
```

**(Step 2) Properties**: What are the (varying) attributes/properties of tofu?**

```javascript
class Tofu { //create a class: template/blueprint of objects with properties and behaviors
    constructor()
    { //initalize the objects
    this.speed = floor(random(2,5));
    this.pos = new createVector(width+5, random(12,height/1.7));  //check this feature: https://p5js.org/reference/#/p5/createVector
    this.size = floor(random(15,35));
    this.toFu_rotate = random(0,PI/20); //rotate in clockwise for +ve no
    this.emoji_size = this.size/1.5;
    }
}
//something more here
```

**(Step 3) Behaviors: What are the behaviors of the tofu?**

```javascript
class Tofu {
  constructor() { //initalize the objects
    // something here
  }
  move() { //moving behaviors
    this.pos.x-=this.speed; //i.e, this.pos.x = this.pos.x - this.speed;
  }
  show() {
      //show Tofu as a cube by using vertex 
      //show the emoji on the Tofu surface/pane
  }
}
```

**(Step 4) Object creation: After the basic setup of the class structure, the next step is to create a tofu object that can display on a screen.**

```javascript 
let min_tofu = 5;  //min tofu on the screen
let tofu = [];

function setup() {
 //something here
 for (let i=0; i<=min_tofu; i++) {
   tofu[i] = new Tofu(); //create/construct a new object instance
 }
}

```
NB: reusability of objects

**(Step 5) Display: How you want to display and present them over time?**

```javascript
let min_tofu = 5;  //min tofu on the screen
let tofu = [];

function setup() {
 //something here
}

function draw() {
 //something here
 showTofu();
}

function showTofu() {
 //something here
  for (let i = 0; i <tofu.length; i++) {
   tofu[i].move();
   tofu[i].show();
 }
}
```

**(Step 6) Trigger point: Think with the holistic logic**

```javascript
function draw() {
  checkTofuNum(); //available tofu
  checkEating(); //scoring
}

function checkTofuNum() {   
  if (tofu.length < min_tofu) {
    tofu.push(new Tofu());
  }
}

function checkEating() {
  //calculate the distance between each tofu
  for (let i = 0; i <tofu.length; i++) {
    let d = int(dist(pacmanSize.w/2, pacPosY+pacmanSize.h/2,tofu[i].pos.x, tofu[i].pos.y));
    if (d < pacmanSize.w/2.5) { //close enough as if eating the tofu
      score++;
      tofu.splice(i,1);
    }else if (tofu[i].pos.x < 3) { //pacman missed the tofu
      lose++;
      tofu.splice(i,1);
    }
  }
}
```

---

### 8. Exercise in class

1. Tinkering
- modify the different values to understand the function/syntax
- add more features/behaviors to the game such as 
    * add sound for each successful/fail score?
    * Any others you can think of...

2. Discussion in groups:
- Identify a game that you are familiar with, and describe the characters/objects by using the concept and vocabulary of class and object. Can you identify the classes and objects within the chosen example?
- Given that the creation of objects requires the concept of abstraction, and following some of the introductory ideas for this class, can you use the sample code or your game as an example to think through some of the political implications of class/object abstraction? Does the fact that this is a game allow for further reflection on the way everyday activities (such as enjoying tofu) become object-oriented?

---

### 9. Mini-ex walk-through: Games with Objects | due SUN mid-night

https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/Readme.md

---

### 10. Mid-way evaluation

- The concept of low floor and high ceiling
- reading, writing+coding and thinking about code

![ap](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class07/ap.png)

[Discuss in groups](https://padlet.com/siusoon/iaaabkoo9qey) - write with the title "Class07-midway, Group [x]"
- What have you learnt in Aesthetic Programming?
- What is the most remarkable example/theme/ex/thing?
- How do you think about the course in relation to:
  coding environments, peer-tutoring, weekly mini ex, notes/slides/sample code, assigned/suggested readings, Tue/Wed/Fri sessions, etc?
- What are the things you can do to make the course better?
- What could be changed/experimented in the course to make it better?
- How can we better use our peers?
