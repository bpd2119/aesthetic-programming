## Class 09: Vocable Code

(adjusted according to the immediate introduction of online teaching)

**Messy notes:**

**Agenda:**

0. Announcement
1. Mini-ex[7] short discussion 
2. Group 7 presentation: JSON and Text Parsing
3. Vocable Code
4. Exercise: Decoding Vocable Code
5. Excercise: Textuality 
6. JSON
7. Exercise: adding your own voice
8. Mini-Lecture
9. Mini-ex walk-through: e-lit | due SUN mid-night
10. Final Project 
---

### 0. Announcement 

1. Zoom vs Blackboard collaborate
2. Online teaching/learning - how long? 
3. Change of instructor class to Fri, think and work before the Fri session.

---

### 1. Mini-ex[7] short discussion 

- quite a lot class/object structure 
- many of you pay more attention on readme, keep it up! It is **IMPORTANT** => more quoting is needed and articulating how you understand the concepts 
- rules (process vs results) -> clear and precise rules, how would you arrow that? How's it different from algorithm? 
- How's it feel like working in pairs? (collaborative (live) coding platform : Glitch/OpenProcessing.org/Teletype https://github.com/processing/p5.js-web-editor/issues/1337)
- next: mandatory group work: 2 people different from the previous groupmate. 

---

### 2. Group 7 presentation: JSON and Text Parsing

**JSON** and **text Parsing** Vs thinking of data processing - also work with the text: Gerlitz, Carolin, and Helmond, Anne. “The like Economy: Social Buttons and the Data-Intensive Web.” New Media & Society 15, no. 8 (December 1, 2013): 1348–65))

1. Mikkel Holger Nørgaard Lundager 
2. Julie Steffensen
3. Magnus Bak Nielsen
4. Jane

---

### 3. Context - Vocable Code

We will be working on text >> electronic literature and the concept of data structure (JSON).

go to sketch08 for class09: http://siusoon.gitlab.io/aesthetic-programming/  (the source code is: vocableCode.js and voices.json)

![code](https://user-content.gitlab-static.net/9ee29983d8cad665c424192cd309fe85dda77173/68747470733a2f2f6c6976652e737461746963666c69636b722e636f6d2f36353533352f34363937313531393936355f346365323039346537355f7a2e6a7067)

The text *Vocable Code* and *aesthetics of generative vode*:
- speech-like qualities, what does it mean by voices? (computer voice, human voice, political voice, without a voice...)
- embodiment: bodily practices, the body of the code, the body of the reader, the body of the voice's performativity, "computer have bodies" > materialities and meaning are deeply interwoven.
- expressive qualities (in the form of text and voices), poem and reader 
- aesthetics - a form of critism 
- codeworks: like poetry - plays with structures of language itself

The artwork *Vocable Code*:

Vocable Code is both a work of “software art” (software as artwork, not software to make an artwork) and a “codework” (where the source code and critical writing operate together) to embody “queer code”. Through collecting voices and statements from others that help to complete the sentence that begins “Queer is...”, the work is computationally and poetically composed: texts and voices are repeated and disrupted by mathematical chaos, to create a dynamic artwork (software art or work of electronic literature) and to explore the performativity of code, subjectivity and language. Behind the executed web interface, the code itself is a mixture of a computer programming language and human language, and aims to expose the material and linguistic tensions of writing and reading within the context of (non)binary poetry and queer computer code.

**the use of constraints or rules**: 

- When writing the source code, do not use binary 0 or 1, either a single X or Y, a single operator of '>' or '<'.
- When writing the source code, be mindful of the naming of the variables, arrays and functions.
- For each specific voice, the sentence starts with the phrase: "Queer is".
- For each specific voice, each sentence contains the minimum of 1 word but no more than 5.

---

### 4. Decoding Vocable Code 

**Task 1:**  (10 mins)

The program operates mainly **around text**, and some of the features as below: 

- There is always text on the black color screen/canvas.
- The text moves upwards and then mostly downwards, but also sometimes slowly oscillates between the two.
- The text fades over time.
- The text varies in size.
- Some of the content of the text is overlapping but there are at least 10 different or unique texts in the pool at any time.
- For each new batch of the text shown on screen, one can hear a voice that speaks one of the texts.
It seems there is a maximum limit of the text appearing on screen for each new batch.
- ...

The program of Vocable Code has used the approach of **Object-Oriented Programming** to construct the class and the text objects. Can you describe the details of the class and objects (such as text properties and behaviors) without looking at the source code? Can you locate when and how (new) text objects are being created/removed?

**Task 2:** (10 mins)
Based on what you see and hear, what are the other functions/features that have been implemented in the program especially in relation to text and voice, and can you describe them? 

Now look at the source code and discuss what you have described about class and objects, as well as other features in the program. 

**Task 3:**  (10 mins)
By reading the source code, you might discover that this is not the most 'efficient' way of writing code. What does it mean and can you spot the redundancy?

---

### 5. Exercise: Textuality 

Below are all the text related syntax in the sample sketch, do you know them all? How are they being used in the program?

* `loadFont()`
* `textFont()`
* `textSize()`
* `textAlign()`
* `text()`

---

### 6. JSON

Beyond the core source code, *Vocable Code* utilizes JSON file to store the data from all voice donors, such as their written statements. By using JSON, all the data can be updated on this JSON file without changing anything at the level of the javascript source code.

https://github.com/siusoon/VocableCode/blob/master/vocablecode_program/voices.json

Javascript Object Notation (JSON) is an open-standard and independent file format, which is widely used for data storage and as a communication format on the internet and software applications. This format can be read and processed by many programming languages such as Javascript. A piece of software implements computational logic to manipulate data, such as retrieving and displaying data on a screen in any color, size, and at any tempo. This kind of separation of data and computational logic is commonly seen in software development. Google, for example, offers their web or image search results in JSON format via their Application Programming Interfaces (APIs). (We will look into more about APIs in next class)  

JSON looks similar to Javascript in terms of the use of arrays and objects but they are formatted differently. Some of the rules are:
- Data is stored in name/value pairs, e.g `"copyLeft": "Creative Common Licence BY 4.0"` and the pair is separated by a colon.
- All property name/value pairs have to be surrounded by double quotes.
- Each data item is separated by commas.
- Square brackets `[]` hold arrays.
- Curly braces `{}` hold objects, e.g. as there are many object instances that share the same structure.
- Comments are not allowed.
- No other computational logics like conditional structure or for-loop are possible.

To process the JSON file, you need to use the syntax `loadJSON` in p5.js. See how this is put together in a sketch:

1. loadJSON (where)
```javascript
let whatisQueer;

function preload() {
  whatisQueer = loadJSON('voices.json');
}
```

2. process the JSON file
```javascript
function makeVisible() {
//queers is the array in the JSON file
  queers = whatisQueer.queers;
//the objects under 'queers'he JSON file -> to select which voice to play)
  SpeakingCode(queers[WhoIsQueer].iam, makingStatements);
}
```

See the relation between the program and the JSON file: 

![json](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/7-VocableCode/ch7_4.png)


3. Locating and loading the sound file
```javascript
function SpeakingCode(iam, makingStatements) {
	let getVoice = "voices/" + iam + makingStatements + ".wav";
	speak = loadSound(getVoice, speakingNow);
}
```

The code is structured in a way those voices are NOT preloaded in the program (like images or JSON file), but it executes on-the-fly. The so-called execution has dual functions: 1) functional 2) Speaking / Running the file as a political act (conceptually)

4. Play the sound file
```javascript
function speakingNow() {
	speak.play();
}
```

---

### 7. Exercise: Adding your own voice (40 mins)

1. Work as a group.
2. Download the *Vocable Code* [sketch](https://github.com/siusoon/VocableCode), and run it on your own computer.
3. Briefly discuss the various computational structures and syntax to understand how things work, and specifically on the relationship between the voice file naming and JSON file structure.
4. Follow the instruction and record your own voice with your computer or mobile phone. (The program takes wav file format only.)
    - Find a blank paper and prepare to write a sentence.
    - Complete the sentence with the starting words: “Queer is ...”
    - Each sentence contains no more than 5 words (the first words - “queer is” - are not included).
    - It is ok to have just one word in a sentence.
    - Maximum two sentences/voices per person.
    - Download/locate a voice recording app on your smartphone (e.g “Voice Recorder” on Android or "Voice Memos app” on iOS).
    - Try to find a quiet environment and record your voice, and see if the app works (controlling the start and end of the recording button).
    - Prepare to record your voice with your written sentence(s).
    - You may decide the temporality and rhythm of speaking the text.
    - You may either speak the full word or full sentence with different pitch/tempo/rhythm.
    - You may speak on a certain part (phonetics) of the word or sentence. In other words, the word / sentence doesn’t need to be fully prounced.
    - The first words “queer is” can be omitted.
    - Record your voice, and convert your voice file into wav file format. (The free software audicity could be considered to do the file conversion.)
5. Add your voice/s and update the program with your own voice/s (update the JSON file and put your voice files under the voices folder). Refresh the program and see if you can hear your own voice in connection with other voices.
6. Advanced: Try to change the presentation of the text, e.g. its color and the animated behavior of the text.
7. Discuss the different critical and aesthetic aspects of queer code with others.

---

### 8. Mini-lecture

**1. The phrase Vocable Code**

- More than functional aspects
- Mirrors human language 
- Code: script and performance
- Performace: Saying and Doing  (Speech-act theory)
- How we can do things with words and code
    
**2. Performativity**
    
- Poetry (performativity of code + the aesthetics of generative art)
- See [Ian Hatcher](https://ianhatcher.net/)'s work: https://anomalouspress.org/11/2.hatcher.plexus.php
- Language as found objects (ref to Dada period e.g [Dada Poem Generator](http://www1.lasalle.edu/~blum/c340wks/DadaPoem.htm)
- Code is like poetry (why?)  [check out the assigned readings!]
    
**3. Connection between speaking and coding**
    
- Explore material connections and creative tensions between the two
- How? (embodiment + materialities)
- Voice and political issues 

**4. Code and language**

- Soure code: send instructions to machines and communicates to humans
- Writing source code: Use of signs and symbols 

![image1](image1.png)

![image2](image2.png)

- Operates across both programming and so-called natural languages (double code)
- Variable names -> voice of the programmer 

**5. Source code?** 

![](https://glossaryweb.com/wp-content/uploads/2018/03/high-level-language.jpg)

- Does NOT show how a machine operates
- DOES NOT show how it interacts with memory 
- DOES NOT show how it translates symbolic actions into real addresses 

=> discrepancy in what you see is not literally how it operates
    
**6. Discrepancy 1: Translation of source code** 
    
- sourcey: high level programming languages 
- blackboxing even more the operations of the machine 
- source code vs machine operations 

**7. Discrepancy 2: Giving voice to both front and back end** 

![interface](https://www.guru99.com/images/1/091318_0717_WhatisBacke1.png)

- interface principle: WYSIWYG 
- interface and source code running
- queering the binary distinction: front and back end
    
**8. Bodily practice**

- voice <-> political practices
- beyond simple representation or interpretation
- executing the function: `SpeakingCode(iam, makingStatements);`
- nonhuman actants 

**9. Queer Code**

Vocable code - voices - aesthetics/politics 

- Challenge the normative use of programming 
- Challenge the normative thinking of front/back ends, and efforts are always undermined for front end development 
- Chalelnge the binary logic and discrete thinking in computing => potential impact if we human becomes more computational 
- Challenge the concept of code as something neutral and it never is

**10. Thinking**

Vocable Code - language - performativity - voice - expressivity - politics - queer code 

---

### 9. Mini-ex walk-through: e-lit | due SUN mid-night

Mandatory group work (2 people in your group)
MiniX [8]: https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/Readme.md 

---

### 10. Final project

https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/FinalProject.md
