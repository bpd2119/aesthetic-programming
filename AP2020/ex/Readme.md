Here is the protocol for the weekly RUNME (a program that can be executed) and README (contextualize your thoughts on the program) mini exercises:

## Weekly mini-Exercise (Both RUNME and README)
- **Due on every SUN mid night** (those who are late submission might miss the chance to receive feedback from your peers and fail to enter the ordinary exam)
- Upload 1x README and 1x RUNME on your gitlab account
- Suggest to have the folder miniEx in your gitlab repository and create a folder miniEx to store all your README and RUNME.
- See the README's markdown guide: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/
- The README.md (within 5600 characters): 
    - 1 x screenshot 
    - 1 x URL that links to your work 
    - 1 x URL that links to your repository (at the code level)
    - Answer/approach the questions stated in the weekly brief (Try your best to reflect on/link to the assigned readings)
    - Reference URLs (if any - this is more to see where you get your inspiration and ideas)
- The RUNME (the software program):
    - js libraries
    - a HTML file
    - a js sketch file 
- Everyone need to prepare to be asked showing and articulating your project in the class every week

## Weekly feedback 
- **Due date: before next class**
- Your group will be splitted into 2. (In general, 2x group feedback is needed for each mini group) For example:
   - class 2: 2 people in group 1 feedback 2 of the students in group 2. 
   - class 3: 2 people in group 1 feedback 2 of the students in group 3. 
   - class 4: 2 people in group 1 feedback 2 of the students in group 4. 
   - ...
- How: Go to "issues" on his/her gitlab corresponding repository. Write the issue with the title "**Feedback on mini_ex(?) by (2x YOUR FULL NAME)**"

## How to do feedback
(around 2000 characters)

For the MiniX[1]:
- **Describe** what is the program about and what syntax were used
- **Reflect upon** what are the differences between reading other people code and writing your own code. What can you learn from reading other works?

For the miniX[2,3]:
- (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
- **Describe** what is the program about, what syntax was used, what does the work express?
- Do you like the design of the program, and why? and which aspect do you like the most? How would you **interprete** the work?
- How about the **conceptual linkage/reflection** about the work beyond technical description and implementation? What's your thoughts on this?

For the miniX[4+]:

There are no specific guidelines and I want you to develop your own set of feedback that you think is useful and relevant to others. Think about what you want to say to your peers? Rethink about what might be aesthetic programminng and how does the work address this. (The only requirement is that the feedback is around 2000 characters)

## Mini_Exercise[1]: My First Program

See the section Mini_Exercise[1]: My First Program:

https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source/1-GettingStarted

## Mini_Exercise[2]: Geometric Emoji 

See the section Mini_Exercise[2]:

https://gitlab.com/siusoon/Aesthetic_Programming_Book/blob/master/source/2-VariableGeometry/Readme.md

## Mini_Exercise[3]: Designing a Throbber 

See the section Mini_Exercise[3]:

https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source/3-InfiniteLoops

## Mini_Exercise[4]: Capture All 

See the section Mini_Exercise[4]:

https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/source/4-DataCapture/Readme.md

## Mini_Exercise[5]: Revisit the past

**Objective:**

- To catch up and explore further with the previous learnt functions and syntaxes
- To revisit the past and design iteratively based on the feedback and reflection on the subject matter.
- To reflect upon the methodological approach of "Aesthetic Programming"

**Tasks (RUNME and README):**

Revisit one of the earlier mini exercises that you have done, and try to rework on your design, both technically and conceptually.

- what is the concept of your work?
- what is the departure point?
- what do you want to express?
- what have you changed and why?
- Reflect upon what Aesthetic Programming might be (Based on the readings that you have done before (especially this week on Aesthetic Programming as well as the class01 on Why Program? and Coding Literacy), What does it mean by programming as a practice, or even as a method for design? Can you draw and link some of the concepts in the text and expand with your critical take? What is the relation between programming and digital culture?)

## Mini_Exercise[6]: Games with Objects 

See the section Mini_Exercise:

https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source%2F5-ObjectAbstraction

## Mini_Exercise[7]: A generative program (**optional** group work - 2 people within your study group)

See the section Mini_Exercise:
https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/source/5-AutoGenerator/Readme.md

For those who are working in the group:
- State the one you are collaborated with (if applicable).
- Each person's readme with a link to the person in-charge in your group

## Mini_Exercise[8]: E-lit (**mandatory** group work - 2 people within your study group)

See the section Mini_Exercise: 
https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/source/7-VocableCode/Readme.md

Each person's readme with a link to the person in-charge in your group

## Mini_Exercise[9]: Working with APIs in a group 

See the section Mini_Exercise:
https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source/8-Que(e)ryData

Each person's readme with a link to the person in-charge in your group

## Mini_Exervise[10]: Flowcharts (individual and group)

See the section Mini_Exercise: 
https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/source/9-AlgorithmicProcedures/Readme.md

Each person with a submission on Gitlab as many of the questions are for individual reflection but of course you can incorporate the group flow chart and the same discussion that you have in your own entry.

## Mini_Exercise[11]: Draft final project (group) 

In order to prepare for the final project submission, this mini exercise is considered as a means towards this final goal.

**Tasks:**

1. Prepare a **max 4 pages** of synopsis that describes your final project idea - both technically and conceptually. What might be the problem of concern that you want to address in your final project?

2. You need to submit a draft flow chart and a reference list (on top of the written 4 pages).
    - Submit your draft in one single PDF format (Make sure all your group's member names are on the doc)
    - For draft submission: Create the blog entry name: "Group X - Draft Final Project" by going to Blackboard\Course Blog\Draft Final Project Submission\Create Blog Entry\

3. As a group: Provide detailed oral peer-feedback to the other group according to your supervision schedule next week.
    - What is the focus of the project?
    - What does the group want to address?
    - Is it specific enough to unfold the project/concept clearly?
    - Is the draft cleared to you (in terms of identifying/exploring the problems, addressing the specific theme of the course and how to use the readings)?
    - Can you get a sense of how their program would work through their flow chart and description?
    - Are there enough critical thoughts towards the reading(s)? What is the voice of the writers regarding the theoretical materials that they bring forward to the synopsis?
    - What are the new or original perspectives that this piece (both writing and program) brings to you?
    - Which part do you like the most and what might be the weakness of the synopsis?
    - Do you have any suggestion for improvement?

## Final Project (group work)

[here](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2020/ex/FinalProject.md) 