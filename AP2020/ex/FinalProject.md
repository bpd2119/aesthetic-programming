**Final Project of Aesthetic Programming** 

# Description

Aesthetic Programming introduces computer coding as an aesthetic, expressive, creative and critical endeavour beyond its functional application. It explores coding as a practice of reading, writing and building, as well as thinking with and in the world, and understanding the complex computational procedures that underwrite our experiences and realities in digital culture. To address these intersections we have been working with fundamental concepts of programming as the starting point for further aesthetic reflection — such as geometry and object abstraction; variable, data type, functions and namings as well as data capturing, processing and automation — thereby the technical functionings are setting the ground works for further understanding of how cultural phenomena is constructed and operationalized.

By drawing upon different theoretical and conceptual texts from Aesthetic Programming (also include Software Studies), your task (**as a group**) is required to conceptualize, design, implement and articulate a computational artifact. Your work should demonstrate the ability to integrate practical programming skills and theoretical/aesthetic understandings to articulate and develop critical computational artifacts, examining the aesthetic, cultural, social and political aspects of software.

# To begin...

To begin with, there are few tips may help you to come up with a project idea:

1. You may take a look again on the themes that we have used and structured for inspiration, including literacy, variable geometry, infinite loops, data capture, object abstraction, auto generator, que(e)ry data, algorithmic procedures, machine learning and the relations to topics like writing and coding, faces, emojis and representation politics, (micro)temporalities, capture all, interactivity, object orientation, rule-based systems, language, expressivity, electronic literature, algorithms, politics of data processing.

2. Take a look again on all the previous mini exercises and all the questions that were set. Is there any one that you want to explore further?

3. Is there any assigned/suggested text that you are especially connected with and you want to explore in a deeper way?

4. Is there any particular technical area that you want to explore and employ in this project such as games, data visualization, 2D/3D objects, and among others.

**RUNME:**
A piece of software written in p5.js (or a combination of HTML/CSS/JS/P5/node.js).

Remember to include all external libraries and data such as images, font, text file, sound etc. Furthermore, if you have borrowed other sample code or ideas, please cite your sources in the code comments.

**README:**
A single word document within 6-8 pages (max characters: 2400 per page include space) which has to be properly written **academically with scholarly citations** (exclude images, references and notes).

The document should include **a title**, **a screen shot**, **a flow chart**, **references**, **a link** to your final project's RUNME, the links to related projects (if there is any), as well as the links of all your previous mini exercises (as an appendix).

The README should address at least the following questions with the help of your source code, programming processes and your selected readings:

- What is your software about (short description: what is it, how does it work and what do you want to explore/unfold)?

- How does your work address at least one of the themes and further explore the intersections of technical and cultural aspects of code in order to reflect deeply on the pervasiveness of computational culture and its social and cultural effects?

- Open question: How do you see the project as a critical work in itself with some of the understanding of the inner workings of software and its material conditions?

# Submission via Blackboard\Course Blog: 

Make sure all your group's member names are on the readme file in the PDF format
- For draft submission: Create the blog entry name: "**Group X - Draft Final Project**" by going to Blackboard\Course Blog\Draft Final Project Submission\Create Blog Entry\
- For final submission: Create the blog entry name: "**Group X- Final Project**" by going to Blackboard\Course Blog\Final Project Submission\Create Blog Entry\
    * Zip your RUNME and README into one file and upload to the blog (Make sure all your related libraries and data files are included and are in correct path. Please specify if you need us to run it on chrome/firefox.)

# Final Project Presentation

- Conduct a presentation within 10 mins with a software demo, articulating your work in both conceptual and technical levels. The presentation will follow by a short Q & A session (within 5 mins).
- Be selective of your presentation items; present those you think are key to the class
- Make sure you have tested your program in a classroom setting, such as projector, Internet connection, sound, slide, resolution etc.

# Timeline:

| Date         | Check point                                  
| ------------ |:-------------------------------------------  
| Week 12   , 17 Mar (TUE) | Final project brief introduction             
| Week 15   , 7 Apr (TUE) | Easter holiday, no class                                 
| Week 16   , 14 Apr (TUE) | Discussion on Final project in class + flow chart miniEx[10] for the final project
| Week 17   , 21 Apr (TUE) | Introduce Mini Exercise[11] > draft of final project                              
| Week 17   , 26 Apr (SUN) | **Upload the draft project + revised flowchart**                           
| Week 18   , 28-29 Apr (Tue) | Supervision and peer feedback
| Week 19   , 5 May (Tue) | No class (Prepare for your final project) 
| Week 20   , 12 May  (Tue) | **FINAL GROUP PRESENTATION + SUBMISSION**     
| Week 24   , 9-11 Jun    | ORAL EXAM
