# last class on 12 May - 13.30-15.00

## All presentations (questions?)

## The course (20 ECTS)

The purpose of the course is to enable the students to design and programme a piece of software, to understand programming as an aesthetic and critical action which extends beyond the practical function of the code, and to analyse, discuss and assess software code based on this understanding.
The course provides an introduction to programming and explores the relationship between code and cultural and/or aesthetic issues related to the research field of software studies. In the course, programming practices are considered as a way to describe and conceptualise the surrounding world in formal expressions, and thus also as a way to understand structures and systems in contemporary digital culture.

The course builds on the idea of practice-based knowledge development as an important IT-academic skill from the courses Interaction and Interface Design and Co-design. The insight into how knowledge of programme code and digital aesthetic forms of expression enhances critical reflection on digital designs will be extended in the courses Platform Culture, Interaction Technologies and Design Project. The course has links to Software Studies on the same semester, and the purpose of the collaboration between these two courses is to provide specific experiences with programming as a reflected and critical practice.

## The Course - competence / learning outcomes

Knowledge:
- Demonstrate insight into and reflect on programming as a practice that allows you to explore, think about and understand contemporary digital culture
- Demonstrate insight into programming and programming principles by writing, reading and processing code

Skills:
- Write, read and process code, including communicating basic programming concepts
- Analyse and assess their own and others’ attempts at expressing aesthetic issues in programme code

Competences:
- Read and write computer code in a given programming language
- Use programming to explore digital culture
- Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

## Oral exam - RUNDOWN (8-11 Jun)

prerequisite/rules: 
- Online > Zoom
- webcam available 
- 10 mins testing time
- Audio and/or video recordings of oral online exams are not permitted
- check out details at the student portal: https://studerende.au.dk/en/studies/subject-portals/arts/corona-information-from-the-faculty-of-arts/online-exams-at-the-faculty-of-arts/

### Steps 

0. You will be at the waiting room, once you are accepted in the exam room, then **show your student id** before the exam starts

1. Start with a 5 mins presentation (Prepare a **5 mins presentation** based on the **pre-assigned question**: “Draw a concrete technical example (a small block/line of code) from your final project and discuss any one of the aesthetic aspects of code that informs your critical reflection on digital design/culture.”)

2. When you are done with the presentation on the pre-assigned question, the examiners will follow by talking about one of the topics below. A really good tip would be to prepare something specific to say about each topic (based on the readings, weekly mini-exercises, themes and course materials). You are welcome to show slides, code snippets, your mini exercises and other experiments, or other forms that you think could help articulate your thoughts within the realm of Aesthetic Programming. We will use this part of the exam to go into depth with a specific aspect of the course. Don’t prepare more than 5 mins for each topic; short but precise.

- Geometry
- Object-oriented programming
- Objects
- Abstraction
- Variables, arrays, 
- JSON
- APIs
- Loops
- Conditional statements/structures
- Algorithms
- Flowcharts
- DOM elements, specifically forms related objects: button, input box, slider, checkbox, etc
- Randomness
- Data
- Generativity
- Rules/Rule-based system
- Language
- Code literacy
- Any one of the JavaScript libraries that you have used
- Any one of the data capture and feedback syntax (e.g keyboard, mouse, DOM objects, audio input, face tracker, etc) 

3. Questions will arise by examiners based on your presentation and your previous submitted final project. We might only discuss on one of the topics above, but we might discuss more. This all depends on the conversation. Eventually, we will perhaps also discuss things from the course that are not listed above but are still relevant to the course. All the discussion should be thought of as from within the realm of **Aesthetic Programming**. 

4. Within 5 mins discussion within examiners only (you will be directed to Zoom’s “Waiting room”. Do not leave the Zoom meeting while you are waiting for your grade. You will be invited back into the meeting when the time comes to give you your grade. )

5. Within 5 mins feedback on grade and exam performance

**CONCERNS**

1) Notes:
- If there is a systems breakdown before or during an exam, the student must contact you by phone. The phone number can be found in the Digital Exams system under the exam in question.
- If it proves impossible to establish a connection within five minutes, you must terminate the exam and contact the student by email or phone to arrange a new time for the exam. This new time will be on the same day, after the planned exams have been completed.
- If the exam is terminated owing to a systems breakdown, this will not be regarded as a failed attempt to pass it. This is the case whether the breakdown occurs before or during the exam. And it does not matter whether the breakdown is due to the student’s system or the system used by the examiner/co-examiner.  The student is not required to produce evidence of any systems breakdowns occurring either before or during the exam.

2) hotline for exams:

The hotline has two entrances and supports both teachers and students.
* Admin issue- The exam hotline is contacted on 8716 1060.
* Arts IT Support is open daily 8-16, if technical problems are needed. IT Support is contacted on 8715 0911.
 
## Final evaluation 

0. Safe space/online participation: 
- zoom -> not casual enough "when everybody is looking at you" / intimidating
- difficult to find motivation on zoom 
- "difficult to make small remarks without being pulled out in front of the class"
- use of discord and zoom to share code snippets
- "feeling being heard with the focus on mental well-being , stay after class" 
- "being inclusive and ways of teaching contributes to the feeling of a safe space and to feeling comfortable when speaking up in class"

1. conceptual and technical 
- "seemed to be minimal focus on the conceptual compared to the learning of code" 
- the class favored spending time with code 
- "going through the code and getting it explained is good" 
- like the "hands-on" aspect -> "the hands-on aspect of the course was a good way of understanding the readings for the classes"
- miniXs were "a good way to ensure we know both the technical and coneptual aspect of AP" 
- like the "mix between practical work, lectures, presentations", "discussion, live-coding, and working together as a group"
- "the aspect of thinking about these programs, and what we are putting out into the world, and how they will have an affect on certain themes" 

2. Book - overview/predictable lecture 
- "the structure of the textbook was nice for my learning experience and served as a nice overview over how the class would look like in relation to ex" 
- "the rigid structure after the book made the lectures predictable and boring." 
- "it was also great to have a course book made specifically to our course." 
- "great structure and connection to the other SS as well." 

Learning/+ve notes:
- miniX and hands-on
- feedbackgivers vs feedbacktakers
- focus on mental well-being and feel welcome + inclusive
- relevant and interesting course activities/readings
- ok to have the space to feel lost while exploring the subject matters
- keep shutup and code 
  
suggestions/issues:
- zoom + the need of physical classroom/interaction
- more about the world and politics (LGBTQ+) -> capitalism e.g data capture, authorship and machine agency in generativity, race in relation to search engines and emojis - representational politics 
- more focus in the beginning on the notion of "aesthetics"/make that AP+CM earlier
- more encouragement to go beyond slight edited code from lecture 

## Aesthetic Programming 

- Code and Share website: https://codeandshare.net/ 

- Code and share facebook group: https://www.facebook.com/codeAndShareAarhus/

- coming workshop: [by Sarah Schorr, Carlos Oliveira, and I, this Friday, at Galleri Image, so it’s physical. It engages with algorithmic culture, media/net art, and critical data studies. https://www.facebook.com/events/751194658880061/?ref=newsfeed

---

How to use it in your whole program study in Digital Design: 

- As a methodology: embrace both formal and disursive meanings (Critical Technical Practice)
- read/write/run/think/design/contextualize with and about code/program as methods
- Relationship with design and computational culture

> The argument the book follows is that computational culture is not just a trendy topic to study to improve problem-solving and analytical skills, or a way to understand more about what is happening with computational processes, but is a means to engage with programming to question existing technological paradigms and further create changes in the technical system. We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities.

> we have been working with fundamental programming concepts, such as geometry and object abstraction; loops and temporality; data and datafication; variables, functions, and their naming, as well as data capturing, processing and automation, as the starting point for further aesthetic reflection whereby the technical functions set the groundwork for further understanding of how cultural phenomena are constructed and operationalized.

> Aesthetic programming in this sense is considered as a practice to build things, and make worlds, but also produce immanent critique drawing upon computer science, art, and cultural theory. From another direction, this comes close to Philip Agre’s notion of “critical technical practice,” with its bringing together of formal technical logic and discursive cultural meaning. In other words, this approach necessitates a practical understanding and knowledge of programming to underpin critical understanding of techno-cultural systems, grounded on levels of expertise in both fields (as in the case of Wendy Hui Kyong Chun or Yuk Hui, for example, who have transdisciplinary expertise). 

## what would you recommend for the next cohort? 

https://pad.vvvvvvaria.org/AP2021
