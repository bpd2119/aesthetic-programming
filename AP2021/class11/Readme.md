[[_TOC_]]

# Class 11: 26-27 Apr | [Machine unlearning](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-11-week-17-26-27-apr-machine-unlearning)

- with Wed/Thur instructor session
- with shutup and code on Friday (1215-1500)

**Messy notes:**

## 1. check-in

- Week 18 no class on next Mon and Tue (but with wed/thur tutorial + fri shutup and code)
- feedback (due tomorrow): https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex#minix10-flowcharts-individual-whole-group

## 2. ELIZA

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/10-MachineUnlearning/ch10_5.png" width=600>

Norbert Landsteiner in 2005:

Class together: 

1. Visit the ELIZA Test (2005) by clicking the button "Next Step", https://www.masswerk.at/elizabot/eliza_test.html so you can see the original example given by Weizenbaum in his published article.19

2. Then visit the work ELIZA Terminal (2005) via the link https://www.masswerk.at/elizabot/eliza.html, and try to have your own conversation.20

Discussion:

3. Share your experience of the original conversation (by Weizenbaum) and your conversation with the chatbot:

    - How would you describe your experience of ELIZA (e.g. the use of language, style of conversation, and quality of social interaction)?
    - How would you assess the ability of technology such as this to capture and structure feelings, and experiences? What are the limitations?

## 3. Teachable machines (understanding input and output)

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/10-MachineUnlearning/ch10_2.png" width=600>

https://teachablemachine.withgoogle.com/v1/

- A quick demo (ML: data - model - output)

## 3.1 Exercise in class 

1. Train the machine using three different sets of gestures/facial expressions, then observe the predictive results shown as various outputs.
2. Test the boundaries of recognition or classification problems, such as having a different test dataset, or under different conditions such as variable lighting and distance. What can, and cannot, be recognized?
3. What happens when you only use a few images? How does this amount of training input change the machine's predictions?

## 4. Mahine Learning to Pattern Recognition

- (un)Learning?
    - Supervised Learning
        - labelling 
        - training dataset (pairs) 
        - classification 
        - https://thephotographersgallery.org.uk/whats-on/digital-project/exhibiting-imagenet (Exhibiting ImageNet (2019) by Nicolas Malevé)
            - ImageNet by Fei-Fei Li (2009)
            - Computer Vision
            - Invisible labour (Amazon Mechanical Turk)

    - Unsupervised learning 
        - <img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/10-MachineUnlearning/ch10_4.gif" width="400">
        - Clustering 
        - find similiaries 
    - Reinforcement learning 

## 5. ml5.js library 

RunMe:   https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch10_MachineUnlearning/

Repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch10_MachineUnlearning

- index.html > include the ml5.js library 
- DOM elements

Tinkering time 

## 6. Exercise in class 

1. Work with the Auto Chapter Generator program and try to generate texts based on different length and temperature values.
2. The generative text example also links to the Chapter 5, "Auto-generator," in terms of agency, unpredictability, and generativity, but how does this chapter change our understanding of these terms given what we know about machine learning? What is learning in this context? What do machines teach us? And in the production of prediction, what does machine learning want?

## 7. Coming to the end....To be or not to be hacked

Ref: Yuval Noah Harari, Audrey Tang, and Puja Ohlhaver, “To Be or Not to Be Hacked? The Future of Democracy, Work, and Identity,” RADICALxChange (2020),https://www.youtube.com/watch?v=tRVEY95cI0o.

- technology knows us better (hacking humans)?
- attention economy (emotions' button)
- transparency, accountability and control  -> civic technologies
- code is law (legal code)
- gender politics 
- are algorithmics gaining more power?

Discussion:

padlet: https://padlet.com/wsoon/nrwfr1mvp85ph8hv

1. Share your thoughts regarding the video/conversation and below questions

There are many things we can talk about when it comes to machine learning (or AI). Which specific aspect that interests you the most? and why? (try to substantiate your thoughts with the readings and the concrete experience that you have. - Not to just say something you feel, but substantiate and articulate it)

2. Try to formulate some questions to open up the discussion of ML/AL.

## 8. MiniX[11] - Draft of the Final Project (whole group)

due 5 May (Wed) mid-night

- https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex#minix11-draft-of-the-final-project-whole-group

## 9. Errors/Computer Vision by Gabriel Pereira 

- About Gabriel: https://www.gabrielpereira.net/
    - His main research interest are critical studies of data and algorithms. His PhD work focuses in understanding how computer vision algorithms mediate our relationship with the world, and what these mediations make possible (or impossible).
- Watch it before Tue class:
https://vimeo.com/321866181 (password: recoding)
