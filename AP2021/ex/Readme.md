Here is the protocol for the weekly RunMe (a program that can be executed) and ReadMe (contextualize your conceptual thoughts on the program) mini exercises:

[[_TOC_]]

## The Protocol of Weekly miniX (Both RunMe and ReadMe)
- **Due on every SUN mid night - unless stated otherwise** (those with late submission might miss the chance to receive feedback from your peers and fail to enter the ordinary oral exam)
- Upload 1x ReadMe and 1x RunMe on your gitlab account
- See the README's markdown guide: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/
- The Readme.md (within 5600 characters): 
    - 1 x screenshot (static or animated gif images, or even video if you like)
    - 1 x URL that links to your work (your work that can run on a web browser) 
    - 1 x URL that links to your repository (at the code level)
    - Answer/approach the questions stated in the weekly brief (Try your best to reflect on/link to the assigned readings)
    - Reference URLs (if any - this is more to see where you get your inspiration and ideas)
- The RunMe (the software program):
    - js libraries
    - a HTML file
    - a js sketch file (your working file/sketch)
    - make sure your program works online
- **Everyone need to prepare to be asked showing and articulating your project and your peer-feedback in the class every week**

### MiniX[11]: Draft of the final project (whole group)

due 5 May (Wed) mid-night

- check Aesthetic Programming textbook p. 246-248
- Give a title to your project and make a revised flowchart
- Include a reference list
- The draft would be 3-4 pages (exclude images, references and notes) in the form of a **PDF**
- Upload to blackboard, and NOT GitLab. Follow the submission procedures [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex#submission-via-blackboardcourse-blog)

**ORAL Feedback | due 10-11 May** 

### MiniX[10]: Flowcharts (Individual + whole group)

- check Aesthetic Programming textbook p. 222 | due Sun mid-night

**Feedback | due next Tue** 
(within 5000 characters)

- Do you know how the program works by looking at the flowcharts? 
- Regarding the group flowcharts, which idea do you like more and with a higher potential to make sense within the cirriculum, and why? 
- What are the potential obstacles? Do you think it is feasible to do it within the timeframe?
- Looking at the readme, do you share the same difficulties on drafting a flowchart?  
- Can you speculate and reflect upon how the practice of diagramming (flowcharts as an "image of thought" - see p. 220-222 of Aesthetic Programming), facilitates critical thinking on algorithms and/or the operations of programming? 

### MiniX[9]: Working with APIs (miniGroup of 2 or 3)

- check Aesthetic Programming textbook p. 207 | due SUN mid-night

**Feedback | due next Tue** 

(within 5000 characters)

- **Describe** 
    - What is the program about and how does it operate?
    - What API has been used, and can you describe in details about the logic of request and response in this example?
    - Are there any new syntax that have been used but your are not familiar with? What are they?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?
    - Does it link to the readings (AP/SS) explicitly?
- **Reflection**
    - Do you like the design of the program, and why? 
    - What's the specificiy of this API? How is it differ and similar to yours?
    - What kinds of question you will ask specifically in relation to que(e)ry data (how data is collected, stored, analyzed, recommended, ranked, selected, and curated) in order to understand the broader social and political implications?

### MiniX[8]: E-lit (miniGroup of 2 or 3)

- check Aesthetic Programming textbook p. 184 | due SUN mid-night

**Feedback | due next Tue** 

(within 5000 characters)

- **Describe** 
    - What is the program about and how does it operate?
    - Are there any new syntax that have been used but your are not familiar with? What are they?
    - What constitutes an E-Lit here? 
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?
    - Does it link to the readings (AP/SS) explicitly?
- **Reflection**
    - Do you like the design of the program, and why? 
    - The way how the group structures and uses JSON is the same as how you do?
    - How would you analyze the work with the perspective of code and language that opens up and extends aesthetic reflection?

### MiniX[7]: Games with objects (individual)

- check Aesthetic Programming textbook p. 162-163 | due Mon mid-night

**Feedback | due next Tue mid-night** 

(within 5000 characters)

- **Describe** 
    - what is the program about?
    - What are the properties and behaviors of objects?
    - what does the work express?
    - How does the game/sketch operate?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?
    - Does it link to the assigned readings explicitly?
- **Reflection**
    - Do you like the design of the program, and why? 
    - How do objects interact with the world?
    - How do worldviews and ideologies built into objects' properties and behaviors?

### MiniX[6]: Revisit the past (individual) 
- due SUN mid-night

**Objective:**

- To catch up and explore further with the previous learnt functions and syntaxes
- To revisit the past and design iteratively based on the feedback and reflection on the subject matter.
- To reflect upon the methodological approach of "Aesthetic Programming"

Tasks (RUNME and README):
Revisit/Rework one of the earlier mini exercises that you have done, and try to focus on the methodology of Aesthetic Programming and consider technology as a site of reflection. Rework your ReadMe and RunMe: 

- Which MiniX do you plan to rework?
- What have you changed and why?
- How would you demonstrate aesthetic programming in your work?
- What does it mean by programming as a practice, or even as a method for design? 
- What is the relation between programming and digital culture?
- Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics? 

**Feedback | due next Tue before class:** 

(within 5000 characters)

- **Reflection**
    - Share & discuss the notion of aesthetic programming within your feedback team 
    - How do you see aesthetic programming in the work that you are going to feedback? How's their approach similar to or different from your own? 
    - Which aspect of aesthetic programming/critical making is missing in the ReadMe and you think is relevant to include? 

### MiniX[5]: A generative program (individual)
- check Aesthetic Programming textbook p. 139)| due SUN

**Feedback | due next Tue before class:** 

(within 5000 characters)

- **Describe** 
    - what is the program about?
    - What are the rules and how the rules are implemented?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation? (e.g generativity, rule-based system, (un)predictability, control, etc)
    - Does it link some of the concepts to the cirriculum (AP/SS) explicitly?
- **Reflection**
    - Do you like the design of the program, and why? 
    - which aspect do you like the most? How's the thinking different from your own miniX?
    - Relate and articulate the work to some of the concepts or quotes in the assigned readings?

### MiniX[4]: CAPTURE ALL (individual)
- check Aesthetic Programming textbook p. 117 | due SUN mid-night

**Feedback | due next Tue before class:** 

(within 5000 characters)

- **Describe** 
    - what is the program about?
    - what syntax were used?
    - what does the work express?
    - What has been captured and what data has been captured?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?
    - Does it link to the assigned readings explicitly?
- **Reflection**
    - Do you like the design of the program, and why? 
    - which aspect do you like the most? How would you interprete the work and how's the thinking different from your own miniX?
    - Relate and articulate the work to some of the concepts or quotes in the assigned readings?

### MiniX[3]: Design a throbber (individual)
- check Aesthetic Programming textbook p. 93 | due SUN mid-night

**Feedback | due next Tue before class:** 

(within 5000 characters)

- **Describe** 
    - what is the program about?
    - what syntax were used?
    - what does the work express?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?
    - Does it link to the assigned readings explicitly?
- **Reflection**
    - Do you like the design of the program, and why? 
    - which aspect do you like the most? How would you interprete the work and how's the thinking different from your own miniX?
    - Can you relate and articulate the work to some of the concepts or quotes in the assigned readings?

### MiniX[2]: Geometric emoji (individual)

- check Aesthetic Programming textbook p. 68 | due SUN mid-night

**Feedback | due next Tue before class:** 

(within 5000 characters)

(Just think, no need to write) Think about what kind of feedback you think would be useful for others. What kind of feedback you want to receive by yourself? 

- **Describe** 
    - what is the program about?
    - what syntax were used?
    - what does the work express?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?
- **Reflection**
    - Do you like the design of the program, and why? 
    - which aspect do you like the most? How would you interprete the work and how's the thinking different from your own miniX?
    - Are there any other thoughts you want to share?

### MiniX[1]: RunMe and ReadMe (individual)

- check Aesthetic Programming textbook p. 46 | due SUN mid-night

**Feedback | due next Tue before class:** 

(within 5000 characters)

- **Describe**:
    - what is the program about? 
    - what syntax were used?
    - what have you learnt from this sketch? 
- **Reflect**:
    - what are the differences between reading other people code and writing your own code. What can you learn from reading other works?
    - what's the similarities and differences between writing and coding? 
    - What is programming and why you need to read and write code (a question relates to coding literacy - check Annette Vee's reading)?

## How to do feedback

It is a great opportunity to learn and study together by reading and discussing other people's code. Please reserve time to do the feedback every week.

- **Due date: before Tue class**

**For individual miniX:**
- Your buddy group will be splitted into 2. (In general, 2x group feedback is needed for each group) For example:
   - class 2: 2 people in group 1 will feedback 1 student in group 2 (your own group no + 1).
   - class 3: 2 people in group 1 will feedback 1 student in group 3 (your own group no + 2).
   - class 4: 2 people in group 1 will feedback 1 student in group 4 (your own group no + 3).
   - ...  

**For group miniX:**
- Your buddy group will then feedback to the whole group of group Y

- **How:** 
1. Go to "Issues" on his/her/their gitlab corresponding repository (locate on the left panel vertical bar). 
2. Click the green 'New issue' button 
3. Write the issue with the title "**Feedback on miniX(?) by (YOUR FULL NAMES)**"

## Final project (group) 

- check Aesthetic Programming textbook p. 246-248 | due SUN mid-night
- **IMPORTANT!!** include everyone's URL that link to the weekly miniX's folders (make sure you have complied all the requirements of weekly mini exercises e.g screenshots, RunMe, ReadMe, etc.) This is a prerequisite for ordinary oral exam for this course.

### Submission via Blackboard\Course Blog: 

Make sure all your group's member names are on the readme file in the PDF format
- For draft submission: Create the blog entry name (one submission per group): "**Group X - Draft Final Project**" by going to Blackboard\Course Blog\Draft Final Project Submission\Create Blog Entry\
- For final submission: Create the blog entry name (one submission per group): "**Group X- Final Project**" by going to Blackboard\Course Blog\Final Project Submission\Create Blog Entry\
    * Zip your RUNME and README into one file and upload to the blog (Make sure all your related libraries and data files are included and are in correct path. Please specify if you need us to run it on chrome/firefox.)

### Final Presentation (18 May)

- Conduct a presentation within 10 mins with a software demo, articulating your work in both conceptual and technical levels. The presentation will follow by a short Q & A session (within 5 mins).
- Be selective of your presentation items; present those you think are key to the class
- Make sure you have tested your program in your computer.

### Timeline:

| Date         | Check point                                  
| ------------ |:-------------------------------------------           
| Week 16   , 19-20 Apr (MON/TUE) | Discussion on Final project in class + flow chart miniX for the final project
| Week 17   , 27 Apr (TUE) | Introduce MiniX > draft of final project (Due: 5 May)                             
| Week 18   , 5 May (WED) | **Upload the draft project + revised flowchart**                           
| Week 19   , 10-11 May (Tue) | Supervision and peer feedback
| Week 20   , 18 May  (Tue) | **FINAL GROUP PRESENTATION + SUBMISSION (11.59.59)**     
| Week 24   , 8-11 Jun    | ORAL EXAM

