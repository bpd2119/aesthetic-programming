[[_TOC_]]

# Class 08: 4 Apr | [Vocable Code](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-08-week-14-6-apr-vocable-code)

- with Wed/Thur instructor session
- with shutup and code on Friday

**Messy notes:**

## 1. check-in

## 2. Language and code 

- code is both script and performance -> ready to do something
- analogy to speech (do things with words + code)
- code with symbols (language structure) and as poetry: read aloud (poetry reading)
- Voice: instability - human subject  

## 3. Vocable Code 

- RunMe: https://dobbeltdagger.net/VocableCode_Educational/
- source code + critical writing operate together 
    - expose the material and linguistic tensions between writing and reading 
- hierarchy between the source and its results 

### 4.1 Decoding Vocable Code 

1) Take a look of the RunMe for 1-2 mins, describe the characteristics of text, how the text is displayed and performed? 

2) If I tell you the code is using an object-oriented approach, can you describe the properties and methods of the class? 

### 4.2 JSON 

### 4.2.1 Peer tutoring

Group 10 presentation: What's JSON and what's the structure of JSON? Can you give an example of how does it work in p5.js? Following the approach of Critical Making/Aesthetic Programming, how would you denaturalizing standard assumptions, cultural values, and norms of JSON in order to reflect on the position and role of this technology (which is the JSON standard that governs how data can be recognized/organized) within society? 

### 4.2.2 JSON in vocable code 

- Full repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch7_VocableCode
- voices.json: https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch7_VocableCode/voices.json

JSON: open standard file format used for data storage and communication/in software applications. 
- the seperation of data and computational logic 

Specification of JSON: 

```
{
  "description": "This file contains the meta data of queer text",
  "condition": "yourStatement cannot be null",
  "copyLeft": "Creative Common Licence BY 4.0",
  "lastUpdate": "Apr, 2019",
  "queers":
  [
  {
    "iam": "WinnieSoon",
    "yourStatement": "not fixed not null",
    "myStatement": "not null not closed"
  },{
    "iam": "GeoffCox",
    "yourStatement": "queer and that means queer",
    "myStatement": "null"
  },{
    "iam": "GoogleAlgorithm",
    "yourStatement": "not a manifesto",
    "myStatement": "here"
  }
}
```

- Data is stored in name/value pairs, e.g. "copyLeft": "Creative Common Licence BY 4.0" and the pair are separated by a colon.
- All property name/value pairs have to be surrounded by double quotes.
- Each data item is separated by commas.
- Square brackets "[]" hold arrays.
- Curly braces "{}" hold objects as there are many object instances that share the same structure.
- Comments are not allowed.
- No other computational logics like conditional structures or for-loop can be used.

In p5.js, we use the function `loadJSON()`

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/7-VocableCode/ch7_4.png)


Step 1: loadJSON (to load the specific file and path)
let whatisQueer;

```javascript
function preload() {
  whatisQueer = loadJSON('voices.json');
}
```

Step 2: Process the JSON file (selected lines)

```javascript
function makeVisible() {
  //get the json txt
	queers = whatisQueer.queers;
  //which statement to speak - ref the json file
  SpeakingCode(queers[WhoIsQueer].iam, makingStatements);
}
```

Step 3. Locating and loading the sound file
https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch7_VocableCode

```javascript
//which voice to speak and load the voice
function SpeakingCode(iam, makingStatements) {
	let getVoice = "voices/" + iam + makingStatements + ".wav";
	speak = loadSound(getVoice, speakingNow);
}
```

Step 4. Play the sound file
```javascript
function speakingNow() {
	speak.play();
}
```

## 4.3 Textuality 

```javascript
let withPride; //font

function preload() {
  withPride = loadFont('Gilbert_TypeWithPride.otf');
}
…
function notNew(getQueer){
  this.size = random(20.34387, 35.34387);
  this.time = random(2.34387, 4.34387);
  this.yyyyy = random(height/3.0, height+10.3437);
  this.xxxxx = width/2.0;
  this.gradient = 240.0;
}
…
this.acts = function() {
  textFont(withPride);
  textSize(this.size);
  textAlign(CENTER);
  this.gradient-=0.5;
  noStroke();
  fill(this.gradient);
  text(getQueer, this.xxxxx, this.yyyyy);
}

```

- `loadFont()`
- `textFont()`
- `textAlign()`
- `noStroke()`
- `text()`

## 5. Aesthetic Programming: performing critique (in this specific artwork - Vocable Code)

- Think about the materials: what's code and what's execution? 
    - language: natural language and execution of code 
- what's happening behind? "there is a discrepancy as what you see is not literally how it operates"
    - the concept of 'sorcery' (Chun)
- questioning the assumptions: normativity of code and coding 
    - What's being prioritized in software design and apps? (front/back end) 
    - what's an interface?
    - queerng the demarcation 
- bodily practice 
    - voice (both humans and nonhumans) & voice as political statements/expressions
    - binary status of 1/0 and discrete structure
        - master and slave, parent and child, human and machine...

## 6. MiniX[8] walk-through: E-lit | due SUN mid-night

MiniX[8] walk-through: E-lit (check Aesthetic Programming textbook p. 184) | due SUN mid-night

2-3 people in a group (depends on your buddy group size)

Collaborative coding: teletype for atom: https://teletype.atom.io/ (but you need a Github account: https://github.com/)

## 7. MiniX[7] + feedback short presentation

>> need to be done in Wed/Thur class (no time in the class)

## 8. Peer Tutoring next 

Buddy Group 11:  What's API in aesthetic programming? Following the approach of Critical Making/Aesthetic Programming, how would you turn this object of study (API) from matter of fact into matter of concern? (the assigned readings should be able to help to think through the question)
