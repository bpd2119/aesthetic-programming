[[_TOC_]]

# Class 09: 12-13 Apr | [Que(e)ry data](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-09-week-15-12-13-apr-queery-data)

- with Wed/Thur instructor session
- with shutup and code on Friday (1215-1500)

**Messy notes:**

## 1. check-in

- other update 
- Que(e)ry data > links to Data capture > datafication, and also to auto-generator (authorship, copyright, generativity), and technically build upon the JSON format that we know from Vocable Code.

## 2. Project showcase: net art generator by Cornelia Sollfrank

<img src="https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/8-Que(e)ryData/ch8_1.png" width="450">

1. Go to net.art generator (https://nag.iap.de/)
2. Explore the generation of images and previous images examples.
3. Pay close attention to the interface and map out the relationship between user input (e.g. a title) and the corresonding output (the image). What are the processes in between the input and output? How are the images being composited and generated (just high level speculation)?

N.B: limitation of API access.

<img src="https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/AP2021/class09/FMC_infographic_nag_overview.png" width="450">

<img src="https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/AP2021/class09/FMC_infographic_nag-Search.png" width="450">

img src: Sollfrank, C & Soon, W. (2021) *Fix my Code*. Berlin: EECLECTIC

## 3. Peer tutoring

Buddy Group 11 presentation:  What's API in aesthetic programming? Following the approach of Critical Making/Aesthetic Programming, how would you turn this object of study (API) from matter of fact into matter of concern? (the assigned readings should be able to help to think through the question)

## 4. Sample Code 

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/8-Que(e)ryData/ch8_2.gif)

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch8_Que(e)ryData/

Two parts: 
1. getting the data (JSON)
2. how to display the image data and pixel 

## 5. Accessing web APIs (step by step)

**Task**: To get your API key and Engine ID to run the following program
Source Code: https://editor.p5js.org/siusoon/sketches/Ci1z5dDyU

N.B: You cannot run the program because you NEED to create your OWN API KEY and ID

**How** to understand for flow of querying data with the example of Google Image Search?
1. the Google image search API's workflow
2. the API specification that indicates what data and parameters are available
3. the returned JSON file format from the web API
4. using the Google API by first registering the API key and search engine ID, as well as configuring the search setting

### 5.1 Step 1: Replace the API key

Objective: Replace the API key with your own details`let apikey = "INPUT YOUR OWN KEY";`.

Search interface

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/8-Que(e)ryData/ch8_3.png)

- Register a Google account if you don't have one (a Google account is needed in order to use the web API - this is also the project's assumption too)
- Login to your account
- Go to [Google Custom Search](https://developers.google.com/custom-search/v1/overview) and find the section API key
- Click the blue button "Get A Key" and then create a new project by entering your project name (e.g. "nag-test") and press enter
- You should be able to see the API key and you just need to copy and paste the key into your sketch

### 5.2 Step 2:  Replace the Search engine ID (cx) with your own

Objective: Replace the Search engine ID (cx) with your own `let engineID = "INPUT YOUR OWN";`.

- Go to [Custom Search Engine](https://cse.google.com/all)
- Click the "Add" button to add a search engine
- You can limit your search area but if you want to search all of Google, simply type "https://www.google.com"
- Enter the name of your search engine, e.g. "nag-test"
- By clicking the blue "Create" button, you agree to the terms of Service offered by Google (and you should know your rights of course)
- Go to the control panel and modify the search engine's settings
- Copy and paste the search engine ID and put it in your sketch

### 5.3 Step 3: Configuration in the control panel

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/8-Que(e)ryData/ch8_3b.png)

- Make sure "Image search" is ON — blue indicates it is 
Make sure the "Search the entire web" is ON — blue indicates it is 
- You should now finish modifying the settings, and now run the sample code with your own API Key and engine ID.

## 6. Reading the JSON + Que(e)rying data 

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/8-Que(e)ryData/ch8_4.png" width="450">

Replace with your keys and paste on the web browser: https://www.googleapis.com/customsearch/v1?key=APIKEY&cx=SEARCHID&imgSize=medium&searchType=image&q=warhol+flowers

API: client & server communicaton:

![](https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/AP2021/class09/FMC_diagram_nag_server_client.png)

img src: Sollfrank, C & Soon, W. (2021) *Fix my Code*. Berlin: EECLECTIC

## 7. MiniX[9] walk-through: Working with APIs (group) | due SUN mid-night

MiniX[9] walk-through: Working with APIs (group)  (check Aesthetic Programming textbook p. 207) | due SUN mid-night

- 2-3 people in a group (depends on your buddy group size, and the group combination should be slightly different from what you had last week)

- Collaborative coding: teletype for atom: https://teletype.atom.io/ (but you need a Github account: https://github.com/)

- Peer feedback - MiniX[9] | due Tue before class

## 8. Tinkering (homework)

To study the original sample code:

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch8_Que(e)ryData/

Repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch8_Que(e)ryData

Other search API's parameters: https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list#parameters

---
---

## 9. 🧐 Break out room: Asking question

1. So you have gone through the process of API and have read the assigned readings. How much do you know the data? and the mechanism of data processing? Is it important to know how data presents to us? 

2. What other questions we could ask for API? (with the perspective of matter of fact to matter of concern)

## 10. 🧐 Break out room 

With the focus on image processing and color visualization:

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/8-Que(e)ryData/ch8_6.png" width="400">
<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/8-Que(e)ryData/ch8_2.gif" width="400">

- Discuss the source code based on #8
- how would you understand `loadPixels()` syntax/function in the sketch? 
- More conceptual thinking: How machines process and store an image as data? How is it similar or different from how humans see?

## 11. Bugs 

![](https://i0.wp.com/mostinterestingfacts.wordpress.com/files/2009/04/bug.jpg)

Programmer Grace Murray Hopper's computer log book in 1945 

#### Debugging: 

1. Syntax errors 
2. RunTime errors 
3. Logical errors 

#### Conceptually what's bug? 

there is a discrepancy between the actual running of code and your mental picture of what's happening in your code. (what you think and what it does)

#### Notes:

1. Debugging as a creative art -> don't panic 
2. Explain your logic to others and break into smallest possible steps
3. Be patience and observe the problem 
    - What you think: what is it supposed to do? 
    - What it actually does: how is it run?
4. Make use of `console.log` (web console)
5. re-adjust the scale and complexity of the program + estimation of debugging time

See additional info here: https://p5js.org/learn/debugging.html

## 12. Peer Tutoring next 

Buddy Group 12: Generate a list of 1000 unique and random integer between two ranges. Then implement a sorting algorithm and display them in a chronological order (You can't use the sort function in p5 or javascript). How wold you approach this problem? How would you visualize it via a flowchart? How is sorting been used in digital culture? What is sorting in aesthetic programming? Think through the assigned reading for this and/or last week.

## 13. Peer MiniX presentation 

[Julie Fey and Andrea Mønster](https://gitlab.com/Julie_Fey/aesthetic-programming/-/tree/master/MiniX8)
