[[_TOC_]]

# Class 04: 1 & 2 Mar | [Data capture](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/Readme.md)

**Messy notes:**

## 1. Check-in 

- issue with Zoom > password protected 
- MiniX[3]
- Pause week ex: loops/arrays/time/function/push-pop/transform: rotate-scale-translate
- wed/thur instructor class: will tighten the linkage between thinking and doing
- how to approach complex code and how to use the miniX to practice
- attention: https://docs.google.com/spreadsheets/d/1s-AiLq7pIcYlwXguUNhUE-3OaC62iuFFhStskNP5SL4/edit#gid=0 (screenshot, RunMe, ReadMe) -> pls update our instructors 

## 2. Peer tutoring by buddy group 

- Announcement: group 4
- Group 3 peer-tutoring: DOM elements & society/culture

## 3. Various mode of capture 
- data processing/capturing vs interactivity 

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/4-DataCapture/ch4_10.gif)

- sample code + Exercise in class (Decode)
    - RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch4_DataCapture/
    - Repository structure: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch4_DataCapture 
    - speculation together 
        - identify the elements and interactions -> list them

### 3.1 Binary button, CSS, Mouse capture
[🔴 video recording] 

- Focus on the Button
- 🧐 Discuss the affordances of a button (ref p. 99 of the AP book)
- 🧐 What might be the stylistic customizations of the button?
- The steps to style a button and create an interactive function  

```javascript
let button;
button = createButton('like');
button.style("xxx","xxx"); //e.g button.style("color", "#fff");
button.size();
button.position();
button.mousePressed(function_name1);
button.mouseOut(function_name2);
```

```javascript
//mouse capture
button.mouseOut(revertStyle);
button.mousePressed(change);

function change() {
  button.style("background", "#2d3f74");
  userStartAudio();
}
function revertStyle(){
  button.style("background", "#4c69ba");
}
```

### 3.2 Keyboard capture
[🔴 video recording] 

```javascript
function keyPressed() {
  //spacebar - check here: http://keycode.info/
  if (keyCode === 32) {
    button.style("transform", "rotate(180deg)");
  } else {   //for other keycode
    button.style("transform", "rotate(0deg)");
  }
}
```

The concept of listening events.

### 3.3 Audio capture 
[🔴 video recording] 

This requires web audio p5.sound library 
    - add the sound library in the library folder 
    - update the index.html 

```javascript
let mic;

function setup() {
  button.mousePressed(change);
  // Audio capture
  mic = new p5.AudioIn();
  mic.start();
}

function draw() {
  //getting the audio data i.e the overall volume (between 0 and 1.0)
  let vol = mic.getLevel();
  /*map the mic vol to the size of button,
  check map function: https://p5js.org/reference/#/p5/map */
  button.size(floor(map(vol, 0, 1, 40, 450)));
}

function change() {
  userStartAudio();
}
```

### 3.4 Video/Face capture 
[🔴 video recording] 

- clmtrackr JS library (2014) by data scientist Adum M. Øygard
    - facial model -> collection of data -> machine learning training 
    - face analysis -> classification -> the issues on data collection and biases 
- [download a library](https://github.com/auduno/clmtrackr) 
    - how do we approach a new library? 
    - how to locate a js library for use? 
- the interaction between video capture and ctracker analysis

```javascript
let ctracker;
let capture;

function setup() {
  createCanvas(640, 480);
  //web cam capture
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();
  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
}

function draw() {
  //draw the captured video on a screen with the image filter
  image(capture, 0,0, 640, 480);
  filter(INVERT);

  let positions = ctracker.getCurrentPosition();

  //check the availability of web cam tracking
  if (positions.length) {
     //point 60 is the mouth area
    button.position(positions[60][0]-20, positions[60][1]);

    /*loop through all major points of a face
    (see: https://www.auduno.com/clmtrackr/docs/reference.html)*/
    for (let i = 0; i < positions.length; i++) {
       noStroke();
       //color with alpha value
       fill(map(positions[i][0], 0, width, 100, 255), 0, 0, 120);
       //draw ellipse at each position point
       ellipse(positions[i][0], positions[i][1], 5, 5);
    }
  }
}
```

in-class walkthrough material
```javascript
let button;
let mic;
let capture;
let ctracker;

function setup() {
  createCanvas(640,480);
  background(100);
  //button
  button = createButton('dislike');
  button.style("color", "#fff");
  button.style("background", "#4c69ba");
//  button.size(200,100);
  button.position(width/2, height/2);
  button.mousePressed(change);
  button.mouseOut(revertStyle);

  // audio
  mic = new p5.AudioIn();
  mic.start();

  //video
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

}

function draw() {
  let vol = mic.getLevel();
  //console.log(vol);
  button.size(floor(map(vol, 0, 1, 40, 500)));

  image(capture, 0,0, 640, 480);
  filter(INVERT);

  let positions  = ctracker.getCurrentPosition();

  if (positions.length) {
    button.position(positions[60][0]-20, positions[60][1]);

    for(let i=0; i< positions.length; i++) {
      noStroke();
      fill(255,0,0);
      ellipse(positions[i][0], positions[i][1], 5,5);
    }

  }

}

function change() {
  button.style("background", "#2d3f74");
  userStartAudio();
}

function revertStyle() {
  button.style("background", "#4c69ba");
}

//keycode..info
function keyPressed() {
  if (keyCode === 32 ) {
    button.style("transform", "rotate(180deg)");
  } else {
    button.style("transform", "rotate(0deg)");
  }
}
```
---
---

## 3.5 Form elements

![](https://www.queness.com/resources/images/formplugin/1.gif)

## 3.6 The 2d arrays

variables -> arrays -> 2d arrays

positions[x][y]
- 1: indicate the tracker points from 0 to 70  i.e positions[i]
- 2: retrieves the x and y coordinates of the tracker points i.e positions[i][0] or positions[i][1]

## 4.  🧐 Exercise in class (modes of capture)

To familiar yourself with the various modes of capture, try the following:

1. Explore the various capture modes by tinkering with various parameters such as keyCode, as well as other keyboard, and mouse events.
2. Study the tracker points and try to change the position of the like button.
3. Try to test the boundaries of facial recognition (using lighting, facial expressions, and the facial composition). To what extend can a face be recognized as such, and to what extent is this impossible?
4. Do you know how the face is being modeled? How has facial recognition technology been applied in society at large, and what are some of the issues that arise from this?

## 5. MiniX[4] walk-through: Capture ALL | due SUN mid-night

Brief: 

MiniX[4] walk-through: CAPTURE ALL  (check Aesthetic Programming textbook p. 117) | due SUN mid-night

remind for the upload with new libraries such as sound and clmtrackr

## 6. MiniX[3] + feedback short presentation
- Svend Bay Møller
- Nina Isis Kinch Bolton

## 7. The concept of capture 

🧐 discussion based on the AP book p. 111- 115
Random breakout room (20 mins)

Select 2 from below 

- web analytics and heat map  (what are these?)
- form elements  (what's the affordances of form elements?)
- metrics of likes (what's the meaning of metrification and the values of metrics?)
- voice and audio data  (What's personalized experience? what's query data? )
- health tracker / "quantified self" (what's self-tracking and quantified self?)

## 8.  Next time peer-tutoring

<img src="https://media.giphy.com/media/ylBc46uTBHGEg/giphy.gif">

- Guidline
- buddy group 5 & 6: https://pad.vvvvvvaria.org/AP2021

