[[_TOC_]]

# Class 10: 19-20 Apr | [Algorithmic procedures](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/Readme.md#class-10-week-16-19-20-apr-algorithmic-procedures)

- with Wed/Thur instructor session
- with shutup and code on Friday (1215-1500)

**Messy notes:**

## 1. check-in

- next week guest lecture (last lecture class)
- why algorithmic procedures

## 2. Discussion on algorithmic procedures 

![](https://media.giphy.com/media/dY0A2zOKQjzEigiR58/giphy.gif)

- instructions - procedures - algorithms => recipe-like algorithmic procedures
- one way of thinking: an algorithm demonstrates the systematic breakdown of procedural operations to describe how an operation moves from one step to the next.

Flowcharts:
- "Flow of the chart -> chart of the flow"
- steps and sequences
- representation

## 2.1 3x Discussion 

padlet: https://padlet.com/wsoon/nrwfr1mvp85ph8hv

🧐 Discussion - 1 : (10 mins)

- Can you give an everyday example (detailing the computational logic) of an algorithm that you have used or experienced?
- Can you sketch an algorithmic procedure? For example, how your social media feeds are being organized?

🧐 Discussion - 2: (10 mins)

- Based on the assigned reading from Taina Bucher, can you list some of the properties of algorithms? How are they both technical and social?

🧐 Discussion - 3: (10 mins)

- We discussed rule-based systems in Chapter 6, "Auto-generator," how does that differ from how we are now discussing procedurality in this chapter?

## 3. Flowcharts 

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/9-AlgorithmicProcedures/ch9_1.png)

RunMe: https://dobbeltdagger.net/VocableCode_Educational/

- Oval: Indicates the start or end point of a program/system. (But this requires further refection on whether all programs have an end.)

- Rectangle: Represents the steps in the process.

- Diamond: Indicates the decision points with "yes" and "no" branches.

- Arrow: Acts as a connector to show relationships and sequences, but sometimes an arrow may point back to a previous process, especially when repetition and loops are concerned.

Challenges: 

1. Translating programming syntax and functions into understandable, plain language.
2. Deciding on the level of detail on important operations to allow other people to understand the logic of your program.

## 4. Exercise 1 (per group)

```javascript
function setup() {
  let multi = ['🐵','🐭','🐮','🐱'];
  for (let species = 0; species < multi.length; species++) {
    console.log(multi[species]);
  }
}
/*output
🐵
🐭
🐮
🐱
*/
```

The task is to draw a flowchart based on this program (20 mins)


## 5. Exercise 2 (per group)

Sorting is a common algorithm in digital culture, and recommendation lists on Spotify, Amazon, and Netflix, will be familiar to you. Think about the "algorithmic procedures" required to program something to solve the sorting task set below.

Watch this video together: * Marcus du Sautoy, “The Secret Rules of Modern Living: Algorithms,” BBC Four (2015),https://www.bbc.co.uk/programmes/p030s6b3/clips.

Task: 20 mins 
Generate a list of x (for example, x = 1,000) unique, random integers between two number ranges. Then implement a sorting algorithm and display them in ascending order. You are not allowed to use the existing sort() function in p5.js or JavaScript. How would you approach this problem? Draw the algorithm as a flowchart with the focus on procedures/steps, but not the actual syntax.

## 6. Peer-tutoring

Buddy Group 10: Generate a list of 1000 unique and random integer between two ranges. Then implement a sorting algorithm and display them in a chronological order (You can't use the sort function in p5 or javascript). How would you approach this problem? How would you visualize it via a flowchart? How is sorting been used in digital culture? What is sorting in aesthetic programming? Think through the assigned reading for this and/or last week.

## 7. Final project walk-through 

https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/ex/Readme.md#final-project-group

## 8. MiniX[10] walk-through: Flowcharts (individual and group) | due SUN mid-night

MiniX[10] walk-through: Flowcharts (both individual and whole group)  (check Aesthetic Programming textbook p. 222)| due SUN mid-night

- Peer feedback - MiniX[10] | due Tue before class

## 9. Peer MiniX presentation 

Sofie Louise Christiansen & Markus Alexander Mejer Bjerremand: https://gitlab.com/markusbjerremand1/aesthetic-programming/-/tree/master/MiniX9final

Mathias Højgård & Asger Mørk Ladekjær:
https://gitlab.com/kroghsen123/aestetisk-programmering/-/tree/master/Minix9

## 10. Discussion

- Bridging the themes que(e)ry data - algorithmic procedures - machine unlearning
- Touch upon Google Search, Sorting, Bias, Gender, Race, Algorithms, Literacy, Design etc

[Beyond the Search: The Biases Inside Google’s Algorithms](https://www.youtube.com/watch?v=9K9ZR_lGRpY)

Abstract: "Beyond the Search" tells the story of two leading information researchers who made shocking discoveries about hidden biases in the search technology we rely on every day. It begins when Dr. Safiya Umoja Noble set out to find activities to entertain her young nieces and entered the term “Black girls’’ into her search bar: pages of pornography appeared as the top results. Subsequent searches of “Latina girls” and “Asian girls” led to similarly sexualizing and racist results. Concerned about the effect of such dangerous stereotypes, Noble embarked on research that would lead to her groundbreaking book, ‘Algorithms of Oppression.’  

Along the way, she discovered the work of another prominent Black researcher, computer scientist Dr. Latanya Sweeney, who had made her own disturbing discovery: When she searched her own name, she got online ads for access to an arrest record. As Sweeney had never been arrested, she began investigating discrimination in online ad delivery. Her findings astounded her: Searching a name more commonly given to Black children was 25% more likely to deliver an ad suggestive of an arrest record. Both researchers share common concerns about how everyday online searches can reinforce damaging stereotypes, and explore how technology can be made more equitable.

Questions:

What's your reflection regarding the film? How does the course Aesthetic Programming might give you insights in looking at those issues?

https://padlet.com/wsoon/nrwfr1mvp85ph8hv

## 11 reminder 

- next week reading on Tue 
- this week physical class (wed/thur) 
- all miniX hand-in 


