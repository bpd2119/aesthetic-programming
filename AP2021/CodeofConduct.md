1. We are a community of, and in solidarity with, people from every gender identity and expression, sexual orientation, race, ethnicity, language, neuro-type, size, ability, class, religion, culture, subculture, political opinion, age, skill level, occupation, and background.

2. We keep our community respectful and open. 

3. Especially with the online teaching and the pandemic situation, we are more careful, patience, considerate and loving, and we acknowledge the importance of maintaining a good mental health. We can all help by doing the following during the online class time to maintain a lively and engaging atmosphere:
    - Try to turn your cameras on during the whole of the class; 
    - If you are attending your class in a space that can be considered disruptive, ensure you have either a blurred background or an image background;
    - Make sure your microphone is muted when you are not speaking, and unmuted when you speak;
    - Make use of the chat functions to post questions and responses regarding the course/class/peers' presentation;

Further reference from [p5.js - Code of Conduct](https://github.com/processing/p5.js/blob/main/CODE_OF_CONDUCT.md#p5js-code-of-conduct)
