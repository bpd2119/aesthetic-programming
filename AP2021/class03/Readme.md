[[_TOC_]]

# Class 03: 15 & 16 Feb | [Infinite loops](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-03-week-7-15-16-feb-infinite-loops)

**Messy notes:**

## 1. MiniX[2] discussion 

<img src="https://media.giphy.com/media/njbfgnK7RUaK4/giphy.gif" width="400">

- [Feedback/Assignment](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex#minix2-geometric-emoji-individual)
- Some issues with the miniX (links and readme)
  - Pls stay behind: group 4 ( Claire Lund + Aylin and Simon)
- Any questions about the miniX & feedback workflow? 
- How's instructor session & Fri shut up and code?
- For tomorrow short presentation 
  - MiniX2: Erna Holst Rokne 
  - MiniX1: Markus Alexander Mejer Bjerremand

## 2. loops

<img src="http://softwarestudies.projects.cavi.au.dk/images/9/99/Animatedthrobber.gif" width=600>

(Aesthetic Programming, p. 73)
- relate to control and automation tasks
- repetitive procedures e.g sound material in music or even animated gif 
- allows the repeated execution of a gragment of code 
  - continues until a given conditions is met 

BREAKOUT ROOM

- [micro whiteboard](https://miro.com/app/board/o9J_lYKfgmA=/)

> 🧐 Assume you have read the text book chapter 3: Have you experienced loops in everyday life? what is loop in programming? How's loop relate to time and temporality?

## 3. Throbber

- Repetition, task driven, & computer operation: Ada Lovelace - [diagram](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_1.jpg) / loop / cycle 
  - a set of instructions to calculate Bernoulli’s Numbers
  - written for Charles Babbage’s unbuilt Analytical Engine (in 1843)
  - See the video here by Zoe Philpott: https://www.youtube.com/watch?v=1QQ3gWmd20s
    - Zoe Philpott is an award-winning experiential storyteller, writer, designer and tech entrepreneur. 
    - Specialising in making complex ideas accessible and relevant to wider audiences. 
    - She creates brand experiences for businesses in collaboration with technologists and designers. 
    - She is the creator of Ada.Ada.Ada and speaks on story, creative innovation, and gender equality in STEM.

- Throbber as a prominent visual icon - contemporary culture (streams and real-timeness)
  <img src="https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class03/browser1.png" width=650>
  <img src="https://user-content.gitlab-static.net/e77ec1d81fa507275d6c6fa5fbdc085e86374ffb/68747470733a2f2f6d6f6e647269616e2e6d61736861626c652e636f6d2f75706c6f6164732532353246636172642532353246696d6167652532353246353334373331253235324663303434663930342d373361302d346465342d613034382d3630316536376537393230342e706e672532353246393530783533345f5f66696c7465727325323533417175616c6974792532353238393025323532392e706e673f7369676e61747572653d4d6e693859354666536c373976475651496f433367333939316f673d26736f757263653d6874747073253341253246253246626c75657072696e742d6170692d70726f64756374696f6e2e73332e616d617a6f6e6177732e636f6d" width=650>
    
    - Buffering icons of death see [here](https://mashable.com/2017/07/12/buffering-icons-ranked/?europe=true#4FSuDdS21Oqr) => many other different names e.g Hourglass cursor, Spinning Wait Cursor, Spinning Beach Ball, Spinning Wheel of Death, etc.
- Opearational and micro(temporal) processes beyond the threshold of human perception
    - streaming
    - example as mentioned in the text: digital signal processing, data packets and network protocols, buffer and buffering
- (Real)Time and Now 
  - (Lammerant 2018):
    - "how the actual expeirence of time is shaped is strongly influenced by all sort of design decisions and implementations, both for humans and machines"
    - standardized time (clock > unification of time lengths)
    - linked to natural rhythms: years, life cycles
    - develop alternative time practices and experiences

## 4. Peer-tutoring

![](https://media.giphy.com/media/l4FGjq205dsq8mcw0/source.gif)

Why [peer-tutoring](https://pad.vvvvvvaria.org/AP2021)? 

- Group 1 peer tutoring: Transform & society/culture, and cover push and pop
- Group 2 peer tutoring: loop & society/culture

## 5. Exercise in class (Decode)

RunMe (Not looking at the souce code): https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/sketch3_1/

NB! the technique of code reading and decoding code 

> 🧐 Can you describe the various elements and how they operate computationally in your
own words (precisely)?

🧐 BREAKOUT ROOM ex

**Speculation:**
- Based on what you see/experience on the screen, describe:
    - What are the elements? Come up with a list of features.
    - What is moving and what isn’t?
    - How many ellipses are there in the middle?
    - Try to resize the window and see what happens.
- Further questions:
    - How do you set the background color?
    - How does the ellipse rotate?
    - How can you make the ellipse fade out and rotate to the next position?
    - How can you position the static yellow lines, as well as the moving ellipses
in a single sketch?

TRY out together:

**Experimentation**
- Tinker with the source code
- Try to change some of the parameters, e.g. background(), framerate(), the
variables inside drawElements()
- There are some new functions you can check in the p5.js reference (e.g.
push(), pop(), translate(), rotate())

> 🧐 BREAKOUT ROOM ex

**Mapping**
- Map some of the findings/features from the speculation you have done to the
source code. Which block of code relates to your findings?
- Can you identify the part/block of code that responds to the elements that
you have speculated on?


## 6. Sample Code

```javascript
//throbber
function setup() {
 //create a drawing canvas
 createCanvas(windowWidth, windowHeight);
 frameRate (8);  //try to change this parameter
}

function draw() {
  background(70, 80);  //check this syntax with alpha value
  drawElements();
}

function drawElements() {
  let num = 9;
  push();
  //move things to the center
  translate(width/2, height/2);
  /* 360/num >> degree of each ellipse's movement;
  frameCount%num >> get the remainder that to know which one
  among 8 possible positions.*/
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255, 255, 0);
  //the x parameter is the ellipse's distance from the center
  ellipse(35, 0, 22, 22);
  pop();
  stroke(255, 255, 0, 18);
  //static lines
  line(60, 0, 60, height);
  line(width-60, 0, width-60, height);
  line(0, 60, width, 60);
  line(0, height-60, width, height-60);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
```

- attention to:
  - seperate/custom function (e.g windowResized and drawElements)
  - local variable, and what that means? 
  - translate / rotate 
  - listening event 
  - push/pop 
  - frameCount 

Any question? 

## 7. Transform

a spatial reconfiguration: translate (moving position), rotate, etc

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_3.png)

 the transformation is done at canvas background level, not at the individual shape/object level.

 `translate(width/2, height/2);`
 `rotate(radians(cir));`

> 🧐 what is `frameCount()`? 

`push()` & `pop()`
- save and restore style/settings

## 8. Function

🧐 what are the functions here? 

```javascript
let x = sum(4, 3, 2);   
print(x);
//passing values four as a, three as b, two as c to the function sum
function sum(a, b, c) {
  return a + b + c; //return statement
}
```

EXERCISE IN CLASS (do it together + tinkering):

You can also try to type/copy the above code into your own sketch, where it will return the number 9. These values are called "arguments" that are passed to the function (i.e. `sum()`). In the example, the parameters as variables a, b and c equals to the actual values 4, 3 and 2 as arguments, but the value of the variables can be changed. The function "sum" can be reused if you pass on other arguments/values to it, as for instance another line of code `let y = sum(5,6,7);` and the return value of y would be 18. 

pause for 3 mins:
> 🧐 You can try to come up with your own functions and arguments.

## 9. Exercise in class (tinkering)

Re sample code: 

1. Change the arguments/values, as well as the position/sequence of the sample code to understand the functions and syntax such as the variable `num`, the transformational functions `translate()` and `rotate()`, as well as saving and restoring current style and transformations such as `push()` and `pop()`.

2. This exercise is about structuring code. How would you restructure the sample code so that it is easier for others to understand, but still maintains the same visual outcome? There are no right or wrong answers, but some pointers below might facilitate discussion:
    - You may rename the function and/or add new functions
    - Instead of having drawElements(), you might have something like drawThrobber() and drawLines()?

## 10. MiniX[3] walk-through: Design a throbber | due SUN mid-night

Brief: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex#minix3-design-a-throbber-individual

Walkthrough together the section: [MiniX3: Designing a throbber](https://gitlab.com/aesthetic-programming/book/-/blob/master/source/3-InfiniteLoops/Readme.md)

**Important:**

- movement - time - temporality 
- group feedback MiniX[3] | due next Tue before class

---
---

## 11. MiniX[2] + feedback short presentation

- walkthrough your assignment, code and feedback that you have given.
- Erna Holst Rokne & Markus Alexander Mejer Bjerremand

## 12. Arrays

- a list of data (relate to variable and data types)

```javascript
//example
let words = [] //array -> start with 0
words[0] = "what";
words[1] = "are";
words[2] = "arrays";
console.log(words[2]); //output: arrays
console.log(words.length); //output: 3
```

Similar principle as how we use variables:

1. Declare 
2. Initialize/Assign 
3. Re(use)

NB! 
1. array index starts with [0]
2. `array.length` -> how many 


Others:
`array.push(value)` and `array.splice()`

## 13. Asterisk Painting 

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/Asterisk_Painting.gif" width=650>

- RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/
- Source code [here]( https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch3_InfiniteLoops/sketch.js)

🧐  Random BREAKOUT ROOM (25 mins)

Use the decoding method & try to speculate, experiment, and map your thoughts to the source code.

**Speculation:** 
- Describe what you see/experience on screen.
    - What are the elements on screen?
    - How many asterisks are there on screen and how are they arranged?
    - What is moving and how does it move?
    - What makes each asterisk spin/rotate and when does it stop to create a new one?
    - Can you locate the time-related syntax in this sketch?

**Experimentation:**
- Change some of the code's arguments
    - Try to change some of the values, e.g. the values of the global variables
    - Which new syntax and functions didn't you know? (Check them out in the p5.js reference.)
- Mapping: 
    - Map the elements from the speculation to the source code



## 14. Conditional statements 

```javascript
if(sentences >= maxSentences){  //reach the max for each asterisk
   //move to the next one and continues;
}
```

`let maxSentences = 77;`

## 15. Loops 

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_8.png)

draws 20 ellipses and each with a distance of 20 pixels.

👾 live coding: 

```javascript
let x = 20;

function setup() {
  createCanvas(420, 100);
  background(200);
  for (let i = 0; i < 20; i++) {
    ellipse(x, 45, 15, 15);
    x += 20;
  }
}
```

loop:
 - A variable declaration and initialization: Usually starts with 0
 - A specificed condition: The criteria to meet the condition
 - Action: What you want to happen when the condition is met
 - Loop for next: For the next iteration (usually incremental/decremental).

Look at *Asterisk Painting* again: 

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/Asterisk_Painting.gif" width=600>

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_5.png" width=600>

```javascript
/*calculate the x-position of each asterisk as
an array (xPos[]) that starts with an array index[0]*/
for(let i = 0; i < xPos.length; i++) {
  xPos[i] = xPos[i] * (xDim / (xPos.length+1));
}
/*calculate the y-position of each asterisk as
an array (ypos[]) that starts with an array index[0]*/
for(let i = 0; i < yPos.length; i++) {
  yPos[i] = yPos[i] * (yDim / (yPos.length+1));
}
```

## 16. While Loops

- another type of loop for executing iterations. The statement is executed until the condition is true and stops as soon as it is false.

🧐  BREAKOUT ROOM (10 mins)

`while(millis() < wait){}`

> Explain what is the above line? and what's the role of the above line in the entire sketch?

## 17. Pause week and Next time peer-tutoring

Pause week: NO ONLINE CLASS on WEEK 8 / 22-23 Feb
Tutorial will still happen on WEEK 8 / 23-24 Feb

Exercise: https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/Readme.md#pause-week-week-8-to-catch-up

Next peer-tutoring on week 9:

<img src="https://media.giphy.com/media/ylBc46uTBHGEg/giphy.gif">

- Guidline
- buddy group 3 & 4: https://pad.vvvvvvaria.org/AP2021


### teacher's reflection (just some notes for myself)
- Would be good to add back Ernst's text as core ref to strengthen the conceptual input 
- It needs more than half an hour for the second day decoding ex, it would have worked better in a physical room rather than online.
  - the speculation part can do it together with the whole class so that everyone has a common understanding before doing the mapping. It is too much right now that the group needs to handle.  
- The selected live coding session seems work i.e loops and throbber#1, but more to emphasis the students to follow along together
- pay attention to the let i = 0 - about the counting of iteration and you need to start from somewhere. students with difficulty on why i or why let i = 0
- for online mode, there is not enough time to do in class exercise in the first day.
- to remind asterisk painting is an advanced sketch, the purpose is not to understand each single line, but to see the potential and possibility of how things are being combined. Emphasis the core is to understand basic concepts e.g arrays, conditional statements, loops

