[[_TOC_]]

# Class 02: 8 & 9 Feb | [Variable geometry](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021#class-02-week-6-8-9-feb-variable-geometry)

**Messy notes:**

- sharing: https://editor.p5js.org/mace/sketches/sGLMoweXF
- no PCD for this semester
- some issues with your Gitlab (excel sheet)
- buddy group - discord - paring - (physical) meet up? - Internet Connection 

## 1. MiniX[1] discussion + why feedback

<img src="https://media.giphy.com/media/njbfgnK7RUaK4/giphy.gif" width="400">

- Sharing: First time experience & the notion of fun
- The concept of ReadMe and RunMe again
    - writing code & writing about code
    - break the divide of: thinking and doing / theory and practice
    - not only technical skills, but critical reflection and critical making/design
- notes
    - importance of reading code to learn 
    - excitement -> creativity -> expressivity -> empowering 
    - spatial orientation 
    - "low floors, high ceilings"
    - difference between atom live-server (local) vs gitlab web URL + pipeline (server)
    - questions? 
- How you might work on your ReadMe? (the linkage with the reading/conceptual phenomena is IMPORTANT!)
- Why we need feedback and what's feedback?

## 2. Again why p5.js - more than just a tool

<img src="https://cdathenry.files.wordpress.com/2016/11/oslcyof.png" width="300">

- open source culture -> open access -> the idea of communities
- accessibility, inclusivity and diversity 
- Lauren McCarthy's video (10 mins)
- check previlage 
- what matters you as a designer?
- Do you have any ideas about opening up spaces in this virutal classroom?

## 3. Geometry 

<img src="https://media.giphy.com/media/26BkLIH6ArvoKr1YI/source.gif" width="300">

- Designing objects: lines, points, shapes
- composition forms and spatial relations -> challenging geometric conventions
- Example: Emoticons -> emojis 
    - abstraction -> representation politics -> perpetuating normative ideologies
        - blatant gender stereotyping
        - racial discrimination
        - see [New ‘gender equality’ emoji to show women at work](https://www.telegraph.co.uk/technology/2016/07/15/new-gender-equality-emoji-to-show-women-at-work/) 
    - standardization -> universalism
- Example: [AImoji: AI-generated Emoji](https://process.studio/works/aimoji-ai-generated-emoji/) by Process (Design) Studio
    - The use of ML techniques and training on existing emoji data 
    - mess up reductive representation logic and reject universalism 
    - how can we think of alternatives?

## 4. Multi and source code 

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/2-VariableGeometry/ch2_1.gif" height="500">

Multi by David Reinfurt
- web design & book cover - key visual: http://www.data-browser.net/
- [apps](http://www.o-r-g.com/apps/multi)
    - with 1,728 possible arrangements, or facial compositions, built from minimal punctuation glyphs. 

Click the RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch2_VariableGeometry/

> (🧐) Decoding: Without looking at the source code/textbook, describe what's happening and the visual, design and interaction elements in **details**

NB: the first thing to learn is to be PRECISE! 

WRITE IT ON the class whiteboard: https://miro.com/app/board/o9J_lYKfgmA=/


## 5. Coordinates  

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/1-GettingStarted/ch1_11.png" width="550">

`createCanvas(windowWidth,windowHeight)` refers to creating a canvas with its width and height in line with your window size.


🤹🏻⏺️ **Exercise (code together):** 

```javascript
function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);
}

function draw() {
  background(random(230,240));
}
```

Explain in your terms: 

- 🧐 what is a `frameRate`?
- 🧐 what is `random`? (check: https://p5js.org/reference/#/p5/random)

About web console: (Under Tools > Web Developer > Web Console on Firefox).
    - Type `print(width);` and then press enter.
    - Type `console.log(width, height);` and then press enter.

(print vs console.log)
Type on the chat with your width and height. 

## 6. Variables  
⏺️
<img src="https://media.giphy.com/media/ORzfkUjDBuxYdqXLc0/source.gif" width="400">

- storing data (kitchen container)
- local vs global

```javascript
let moving_size = 50;
let static_size = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);
}

function draw() {
  background(random(230,240));
  ellipse(255, 350, static_size, static_size);
  ellipse(mouseX, mouseY, moving_size, moving_size);
  if (mouseIsPressed) {
    static_size = floor(random(5, 20));
  }
}
```

**USAGE:**

1. **Declare:**
    - Think of a name for the container you want to store the value in 
    - Declare with the syntax “let” in front. 
    - There are certain rules to follow in naming variables:
        - Names usually begin with a lowercase string and not with a number or symbols.
        - Names can contain a mix of uppercase and lower case strings, and numbers.
        - Names cannot contain symbols.

2. **Initialize/Assign:**
    - What is it that you want to store? A number? By assigning a value, you
will need to use the equal sign. 
    - There are four data types:
        - **number** for numbers of any kind: integer or floating-point.
        - **string** for strings. A string may have one or more characters and it has to be used with double or single quotation marks. For example: let moving_size = "sixty";
        - **boolean** for true/false. For example: let moving_size = true;
        - **color** for color values. This accepts Red, Green, Blue (RGB) or Hue, Saturation and Brightness (HSB) values. For example: let moving_size = color(255,255,0); see more: https://p5js.org/reference/#/p5/color)
        
3. **(Re)Use:**
    - How and when do you want to retrieve the stored data? 
    - If the variable changes over time, you may wish to reuse it many times.

🤹🏻 **DISCUSSION & EX:**

> Breakout room 5 mins

> Take a look at how mouseX and mouseY are being used, and if you want to know the coordinate of these two when you press your mouse, how to do it? (hint: use the print or console.log function and to consider mouseX and mouseY are changing variables)

🤹🏻 **DISCUSSION:**

> Breakout room 5 mins

> Why variables are important to know and good to use? (you may refer to the text book > Why use variables?)

## 7. Other functions

`noStroke()`, `strokeWight()`, `stroke()`, `fill()`, `nofill()`, `rect()`, `vertex()`, `floor()`, `mouseIsPressed()`

## 8. Conditional structures
⏺️
<img src="https://media.giphy.com/media/l4EoMN9qjAOaaAcNO/giphy.gif" width="400">

```javascript
//example in human language
if (I am hungry) {
    eat some food;
} else if (thirsty) {
    drink some water;
} else{
    take a nap;
}
```
- check boolean expression 

```javascript
if (mouseIsPressed) {
    static_size = floor(random(5, 20));
}
```
**RELATIONAL operators**

```javascript
if (I am hungry) && (I am in a good mood) {
    print("go out");
}
```

```javascript
/*
   Relational Operators:
   >   Greater than
   <   Less than
   >=  greater than or equal to
   <=  less than or equal to
   ==  equality
   === equality (include strict data type checking)
   !=  not equal to
   !== inequality with strict type checking
   */

   /*
   Logical Operators: boolean logic:
   &&  logical AND
   ||  logical OR
   !   logical NOT
   */

   /*
   Example:
   if () {
       //something here
   } else if() {
       //something here
   } else{
       //something here
   }
   */
```

🤹🏻 **TEST**

What will be the result in the console log? 

POLL 

```javascript
let x = 28;
if ((x > 10) && (x <= 20)) {
    console.log("one");
} else if (x == 28) {
    console.log("two");
} else if (x === 28) {
    console.log("three");
} else if (x > 5 || x <= 30) {
    console.log("four");
} else  {
    console.log("five");
}
```

## 9. Basic arithmetic operators 

<img src="https://media.giphy.com/media/seVVu09CPz2upPeU1s/giphy.gif" width="300">

- add(+): For addition and concatenation, which is applicable to both numbers and text/characters.
- subtract(-)
- multiply(*)
- divide(/)
- Special operators: increment (++), decrement (--)

Try together:

```javascript
console.log(2*3);
```

```javascript
console.log("hello" + "world");
```
---
---

## 10. Short presentation by student(s) on MiniX[1] 
- Jakob Wang
- Sine Jensen

## 11. geometry - shape - face - emojis 

<img src="https://media.giphy.com/media/3og0IDD1wg8dujEW6Q/giphy.gif" width="300">

- facialization
- racism
- variations
- facial recognition techologies
- how to think about geometry differently e.g escape normative configurations, and draw attention to relations 
    - rethink normative geometry 

Femke Snelting text on "Other Geometries":

> "A circle is a simple geometric shape. […] Circles are mathematically defined as the set of all points in a plane that are at the same distance from a shared center; its boundary or circumference is formed by tracing the curve of a point that keeps moving at a constant radius from the middle. […] Circles are omnipresent in practices and imaginaries of collectivity. [… and yet] Their flatness provides little in the way of vocabulary for more complex relational notions that attempt to include space, matter and time, let alone interspecies mingling and other uneasy alliances. The obligation to always stay at the same distance from the center promises a situation of equality but does so by conflating it with similarity. Circles divide spaces into an interior and an exterior, a binary separation that is never easy to overcome. We urgently need other axes to move along."

## 12. Mini-ex [2] walk-through: Geometric emoji | due SUN mid-night

(check Aesthetic Programming textbook p. 68)

Walkthrough together the section: [MiniX: Geometric emoji](https://gitlab.com/aesthetic-programming/book/-/tree/master/source/2-VariableGeometry)

**Important:**

- geometric drawing - design elements - spatial relations (coordinates)- visual compositions
- expressivity
- group feedback MiniX[2] | due next Tue before class

## 13. 🤹🏻 Discussion in class 

> BREAKOUT room

1. Examine existing geometric emojis (https://gitlab.com/aesthetic-programming/book/-/blob/master/source/2-VariableGeometry/emojis.jpeg) or those available on your mobile phone, can you describe about the shape of an emoji? what constitutes a face? What essential geometric elements do you need for a particular facial expression? What has been lost in translation?
2. Reflect upon the complexity of human emotions, and their caricatures. What is your experience using emojis? What are the cultural and political implications of emojis (you might refer to the reading and introduction above)?
3. Beyond the face, take a look at more emojis (https://www.pngfind.com/mpng/ohwmTJ_all-the-emojis-available-on-facebook-russian-revolution/). Is there anything you want to add?
Experiment with p5.js. How do you translate your thoughts into lines of code? You may want to print the coordinates of the mouse press in the web console area to get a more accurate position for your shapes.

## 14 Next week peer-tutoring

<img src="https://media.giphy.com/media/ylBc46uTBHGEg/giphy.gif">

- Guidline
- buddy group 1 & 2: https://pad.vvvvvvaria.org/AP2021

---

### teacher's reflection (just some notes for myself)
- the topic seems works
- second day with more discussion and contextualization, which is really nice to have the extra hours
- having two students presenting their work is nice, then other students can ask more on the work-in-process, how to make a sketch, how to clean up code, etc.
- may be can emphasis the attention to process rather than an end result, then can further link to programming as a way of thinking.  
