[[_TOC_]]

# Class 01: 1 & 2 Feb | [Getting Started](https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/AP2021/Readme.md#class-01-week-5-1-2-feb-getting-started)

**Messy notes:**

## 1. Introduction + formalities + questions

<img src="https://media.giphy.com/media/137EaR4vAOCn1S/source.gif">

- messy notes
- Why programming? -> See PCD: https://www.pcdaarhus.net/whycode/

Teaching team: 
- [Winnie Soon](http://www.siusoon.net)
- [Mads Marschall](https://gitlab.com/M.Marschall)
- [Nynne Lucca](https://gitlab.com/nynnelucca)

Sharing/advices from last year students:
https://pad.riseup.net/p/AP2020-keep 

## 2. Class whiteboard discussion: Design & Programming?

<img src="https://media.giphy.com/media/3o6MbsWjZwURvye0ko/giphy.gif">

[Miro whiteboard](https://miro.com/app/board/o9J_lYKfgmA=/)

> (✋) How many of you have some sort of programming/scripting experience? (even a little)

- Beyond the tools perspective
- technical vs cultural (STEM vs STEAM)
    - [Exploratory programming for arts and humanities](https://mitpress.mit.edu/books/exploratory-programming-arts-and-humanities-second-edition)  (Montfort, 2016)
- [Coding literacy](https://mitpress.mit.edu/books/coding-literacy) (Vee, 2017)
    - programming > wider social relations (Vee, 2017)
- Creativity, Exploration in a more open-ended manner
- The book [Aesthetic Programming](http://aesthetic-programming.net/) -> Design and Programming: repository, web, markdown, etc.

> (🧐 ) Based on the book (p. 29-30), what might be reasons to think about the need to code? (Type on the chat?)

## 3. Overview: Syllabus, class and learning, expectation

- [What is the course about](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021) / learning outcome
- Expectation and Tasks in general 
- Buddy Group 
- KEY learning tool: [Weeky Ex and Feedback](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex)
    - the concept of "low floors, high ceilings"
- PCD @ Aarhus - March (http://www.pcdaarhus.net)
- Plan your schedule from Mon to Sun
- Online class time (e.g Tue)?
- Questions?

## 4. Setting up the working/development environment (p5.js lib + Atom code editor + web browser)

p5.js + ATOM (code editor) + web browser

### p5.js
See section: **p5.js** (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/1-GettingStarted)

**DO IT TOGETHER - walkthrough**

1. First go to the download page of [p5.js](https://p5js.org/download/) 
    - get the p5.js complete library (in the compressed "p5.zip" format) by clicking it and saving the file, which includes all the necessary libraries to run the code.

2. Double click to unzip the file 
    - to extract all the files it contains. A new folder will be automatically created called "p5."

3. The next part is crucial to the on-going development process, because you have to somehow identify where your work folder will be located. If you have no idea, you may consider using the "Desktop" folder. 
    - ("Foldering" is a concept used for organizing files on your device, which is similar to organizing papers, folders, books on a bookshelf. Increasingly streamlined UX designs mean that many people find it alienating to navigate to or locate the path and directory of files, such as images, on a device as people are becoming increasingly accustomed to putting everything on the first few pages of a phone or simply on the desktop.)

4. Click on the folder "empty-example," and you will see a list of the files you need to start:

    - **index.html**: the default Hypertext Markup Language (HTML) which will be first to be picked up by a web browser. 
        - HTML is a fundamental technology used to define the structure of a webpage and it can be customized to include text, links, images, multimedia, forms, and other elements.
    - **sketch.js**: the key work file for writing JavaScript. 
        - The word 'sketch' is used similarly to the way it would be in the visual arts, in other words it is a less formal means of working out or capturing ideas, and experimenting with composition.
    - **p5.js**: the p5.js core library.
    - **p5.sound.js**: the p5.js sound library for web audio functionality
        - including features like playback, listening to audio input, audio analysis and synthesis.

### Atom (code editor)

**DO IT TOGETHER - walkthrough**

See section: **Code editor** (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/1-GettingStarted)

Atom is used as the code editor. It supports cross-platform editing and can be run on Mac OS, Windows and Linux.

1. **Download** the software Atom from the homepage: https://atom.io/

2. **Drag** the "p5" folder that you have just unzipped into Atom. You should be able to see the left-hand pane with your project. Try to navigate to the "index.html" file under the "empty-example" folder, double click that file and the source code should display on the right-hand pane.
<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/1-GettingStarted/ch1_4.png">

- "index.html" is usually the default page the web browser will display. 
- You can customize the page title and other styling issues
- incorporate JavaScript files and libraries by using the tags ```<script>``` and ```</script>```.
    - *path and directory*: a useful concept when we need to understand how the libraries are operated, how to locate the files and how to incorporate new libraries and files in the future. JavaScript libraries are simply files, and we have to incorporate these files into the HTML so that they can be imported and read by the program. 

3. install a package called "atom-live-server"  -> for setting up a web server so you can update your code and see the results immediately in a browser without needing to refresh it. 
    - check under "Packages" on your menu bar of Atom and see if the package is there. 
        - If not, then go to "Edit > Preferences > '+ Install'," then type "atom-live-server." 
        - Hit the blue install button and you should be able to find it again in the Packages menu.

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/1-GettingStarted/ch1_5.png">

*If you want to customize the theme like the background color of the panes, simply go to "Preferences > Themes."*

## 5. Run the first p5.js program

**DO IT TOGETHER/Live Coding**

Task: To be able to run the first program on your own computer with ATOM, and display the executed version on a web browser: 

```javascript
function setup() {
  // put setup code here
  createCanvas(640, 480);
  print("hello world");
}
function draw() {
  // put drawing code here
  background(random(50));
  ellipse(55, 55, 55, 55);
}
```

- `function setup()`
:   → Code within this function will only be "run once" by the sketch work file. This is typically used to set the canvas size to define the basic sketch setup.

- `function draw()`
:   → Taking cues from drawing practice in visual arts, code within this function will keep on looping, and that means `function draw()` is called on for each running frame. The default rate is 60 frames/times per second

- the symbol `//` (comments): referring to text that are written for humans but not computers. This means a computer will automatically ignore those code comments when it executes the code. or `/*……………*/`: indicates multiple lines of code comments.

- Sample code repository - sketch#1: https://aesthetic-programming.gitlab.io/book/ 

### Exercise in class (folder - for later git upload)

This exercise is to familiarize you with the working environment, path and local directory so you learn that running a sketch in a web browser requires loading the exact path to the JavaScript libraries. You are also free to create your own folder name and rename the file sketch.js as you please.

- **Stop the server**. Stop the atom-live-server by going to "Packages > atom-live-server > Stop."
- **Rename the folder**. Try to rename the folder "empty-example" as "miniX1" 
    - (in order to help the computer to process better, don't use any spaces). 
- **Structure the p5 libraries** - to consolidate all libraries files into one folder
    - Try to create a folder called "libraries" under "miniX1."
    - Drag the two p5 libraries (e.g p5.js) into the newly created folder: "libraries."
    - Change the relative path of the two js libraries in index.html `<script src="../libraries/p5.js"></script>`
- **HTML**. Change the title in the HTML file
    - **RUN again**. Can you run the program again ("Atom > Packages > atom-live-server > Start Server")?

<img src="folder.png" width="650">

## 6. Mini-ex [1] walk-through: My First Program | due SUN mid-night

Walkthrough together the section: [MiniX: RunMe and ReadMe](https://gitlab.com/aesthetic-programming/book/-/blob/master/source/1-GettingStarted/Readme.md)

**Important:**
- the concept of RunMe and ReadMe (writing code & writing about code)
- additional inspirations
- low floors & high ceilings
- group feedback 

**Start to think about it! Prepare your idea before the Wed/Thur instructor class.**

---
---

## 7. What is JavaScript and why p5.js?

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/1-GettingStarted/ch1_1.png" width="700">

(BREAKOUT ROOM)
> (🧐) Based on the discussion in the book (p. 30-31 > start()) & may be your experience and perception, what's Javascript and Java?
<img src="https://regmedia.co.uk/2013/01/30/java_logo.jpg" width="200">

> (🧐) What's special about p5.js and its community? How do they put focus on accessibility, inclusivity and diversity?

- Code execution 

<img src="https://2r4s9p1yi1fa2jd7j43zph8r-wpengine.netdna-ssl.com/files/2017/02/02-02-interp02.png" width="400">

## 8. Web console + reading reference guide

Refer to the [sample code #1](https://aesthetic-programming.gitlab.io/book/) 

- All the syntax check: `createCanvas()`, `background()`, `random()`, `ellipse()`,`print()` .
- reference guide (walkthrough together): https://p5js.org/reference/  > `ellipse()`

- Web console: 
    - `print()` = `console.log()`: write in the "console area" of a web browser.
        - not intended for end users, but for programmers or developers to see if there are any error messages, which are logged to the console and to check that code is executing in the expected way.
    - First concept of error like spelling or missing punctuation. 
    - Where?
        - In Firefox: "Tools > Web Developer > Web Console" (or press the keyboard shortcut: Ctrl + Shift + K for Linux/Windows, and Option + Command + K for Mac); 
        - In Chrome: "Command + Option + J" for Mac.

```javascript
print("hello world");
console.log("hello world");
```

### Hello World Program - natural language

<img src="https://media.giphy.com/media/LQzvI8zbzrwZliUj3V/giphy.gif">

[The project](http://www.anti-thesis.net/hello-world-60/) hallo welt! (hello world!) by Geoff Cox and Duncan Shingleton plays on this communicative act, looping more than 100 Hello World programs written in different programming languages, alongside a selection of human languages, combining them into a real-time, multilingual, machine-driven confusion of tongues (as in The Tower of Babel)

## 9. The idea of RunMe and ReadMe & MiniX

<img src="https://media.giphy.com/media/iGRhu8VRr8tgXBJwA8/giphy.gif">

- the the notion of literacy (this week's theme)
    - cultivate **reflection** by writing code (RunMe) and writing about code (ReadMe) 
    - students are encouraged to **question** the application of their learning in the making of artefacts
- rooted in software culture 
    - related to [runme.org](http://runme.org/) in 2003 > software art (repository)
    - [readme festival](https://monoskop.org/Readme): promoted the artistic and experimental practice of software, which took place at the Marcros-Center in Moscow (2002), Media Centre Lume in Helsinki (2003), Aarhus University and Rum46 in Aarhus (2004), and HMKV in Dortmund (2005). 
    - an annual festival for art and digital culture, transmediale had started to use 'artistic software' or 'software art' from 2001-2004.
    - More: see Florian Cramer, and Ulrike Gabriel, "Software Art," American Book Review, issue “Codeworks”(Alan Sondheim, ed.) (2001); Goriunova, Olga, and Alexei Shulgin. read_me: Software Art & Cultures (Aarhus: Aarhus Universitetsforlag, 2004); Andreas Broeckmann, "Software Art Aesthetics," Mono 1 (2007): 158-167

So what to do? 

[Weekly MiniX](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/class01#6-mini-ex-1-walk-through-my-first-program-due-sun-mid-night) **(due Sun mid-night)** 

[Group Peer-Feedback](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2021/ex) **(due next Tue before class)**

## 10. Git & Setting up the GitLab structure for miniX

**This will be likely continued on the Wed/Thur instructor class with a small class size walkthrough**

<img src="https://media.giphy.com/media/L8K62iTDkzGX6/giphy.gif">

- What's Git (2005 by Linus Torvalds) and GitLab as a platform?
    - Git as a version control and management system
    - Gitlab as open source web-based repository platform and a social platform
- Making your projects public 
- Many ways to work on Gitlab e.g command lines with [Git](https://git-scm.com/downloads), [Git Kraken](https://www.gitkraken.com/git-client) and the simplist way is GitLab Web IDE (although there are still some steps, you will get a clearer sense about the folder/directory/files concept).

**WEB IDE**

(**DO IT TOGETHER - walkthrough**)
Example: https://gitlab.com/siusoon/aesthetic-programming-minix

0. Quick interface walkthrough

**A. Steps for the account and repository creation:** 

This is the basic level to create a Readme.md file so that other classmates can read your work and give you feedback.

1. Register an account by clicking 'Register' at the navigation bar (https://gitlab.com/) 
2. To create new project: Go to the top menu and find the + sign for adding a new project
3. Give a project name and project description  (e.g `Aesthetic-Programming`) 
    - click Public if this project can be accessed by others without any authentication.
4. At this point you can also initialize a README within the repository by ticking the checkbox. (file name e.g `README.md`) -> the default file to pick up from the folder
5. A folder in your repository then should be created

**B. Steps for Web server configuration:** 
1. Under the folder `Aesthetic-Programming` > find the + sign > New file 
2. Select a template type: choose `.gitlab-ci.yml`, and then the file name do the same, type: `.gitlab-ci.yml`
    - copy and paste the file content: https://gitlab.com/siusoon/aesthetic-programming-minix/-/blob/master/.gitlab-ci.yml 
    - scroll down and click the green button 'commit changes' 
3. Wait a while to setup the server 

**C. Steps about folders: library folder** 

(simulate what you have on your local computer)
1. Under the folder `Aesthetic-Programming` > find the + sign next to the folder name > `New directory`
2. Type "libraries" in the directory name, and click the button `create directory`
3. Go to the "libraries" directory > click `Upload file` > drag the p5.js file there > Click the green button "Upload file" 

**D. Steps about folders: miniX1 folder**
(simulate what you have on your local computer)
1. Under the folder `Aesthetic-Programming` > find the + sign next to the folder name > `New directory`
2. Type "miniX1" in the directory name, and click the button `create directory`
3. Go to the "miniX1" directory > click `Upload file` > upload the files **one by one** from your first program: "index.html' and `sketch.js` > Click the green button "Upload file" 

NB! At this point you should have two folders under "Aesthetic-Programming" repository: miniX1 and libraries

**E. How to see my first program on a web browser**

The web only renders in an HTML format in this context. You need to have an HTML page in order to show something.

The logic of a URL in Gitlab: https:// + (username).gitlab.io/(directoryname)/(if you have more folders)/

e.g website: https://siusoon.gitlab.io/aesthetic-programming-minix/ (only if you have the index.html file in the folder/directory)

Try this - On-going miniX (example): https://siusoon.gitlab.io/aesthetic-programming-minix/miniX1/

NB! Path and folder concept are important here, you need to constantly check the path of the library and the sketch.js in the index.html.

**F. How to make a readme with a screenshot?**

- Go to the miniX folder, then upload file > upload the image (e.g abc.jpg)
- Go to the miniX folder, then create a new file with the name `README.md`
- Image markdown/html:
    - `![](abc.jpg)`
    - `<img src="abc.jpg" width="600">`
- Suggest to look at the source of other readme on Gitlab by clicking the icon 'Open Raw' of a readme file e.g https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx1/READMEMiniX1.md 
- [Markdown Guide by GitLab](https://about.gitlab.com/handbook/markdown-guide/)
- [Markdown: Basics](https://daringfireball.net/projects/markdown/basics) by John Bruger

**G. How to rename/delete files?**

(see [here](https://forum.gitlab.com/t/delete-rename-a-file-folder/18304))
- Got to Web IDE 
- On the left panel, click the folder / file name that you want to modify. The right corner arrow will expand with options like 'Rename/Move', 'Delete'
- Click on one of the features, and then write a commit message e.g 'update the file names"  -> click the button 'Commit'
- In the radio button, select 'Commit to master branch' -> click the button 'commit'

---

#### teacher's reflection (just some notes for myself) 
- screen sharing on zoom need to resolve before class start (a reminder to check before the class)
- can repeat next year (install atom, package, firefox and chrome browser, register gitlab account) - do the prep before the offical class starts
- file path/folders and directory are still difficult, perhaps need an illustration next time. 
- zip is a slightly difficult concept that requires file extraction.
- split into two days with gitlab at the end is good, so that people who are done can leave early.
- need to observe how things go with web IDE 
- need to check chrome disable javascript issue 
- for the breakout room discussion can use miro board 
