# Class 11:  25 & 27 Apr | Machine Unlearning

**Messy notes:**

[[_TOC_]]

## 0. Announcement

- This Fri guest lecture by Geoff Cox - [Machine Seeing and Invisual Literacy](https://arts.au.dk/en/aktuelt/arrangementer/vis/artikel/friday-lecture-geoff-cox-machine-seeing-and-invisual-literacy-2)
- This week (Fri) no shutup and code
- Next week - No physical class on 2 May (Mon)
  - WORK on your final project draft
  - see [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022#class-12-week-18-2-4-may-studio-time)

## 1. Machine Unlearning

![](https://media.giphy.com/media/8dYmJ6Buo3lYY/giphy.gif)

> “If you consider a child’s eyes as a pair of biological cameras, they take one picture about every two hundred milliseconds, the average time an eye movement is made. So by age three, a child would have hundreds of millions of pictures of the real world. That’s a lot of training examples. So instead of focusing on solely better and better algorithms, my insight was to give the algorithms the kind of training data that a child was given by experiences, in both quantity and quality.” - Fei Fei Li

Discussion: What do you know about Machine Learning? What is learning means to you?

---

ML: collection of models, statistical methods and operational algorithms that are used to analyze experimental or observational data.
- coined by Arthur Samuel (1959)
- goal: reduce or even elimiate the need for 'detailed programming effort'
- input/output

## 2. ELIZA by Joseph Weizenbaum (1964 and 1966)

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/10-MachineUnlearning/ch10_5.png" width=600>

Norbert Landsteiner in 2005:

Class together:

1. Visit the ELIZA Test (2005) by clicking the button "Next Step", https://www.masswerk.at/elizabot/eliza_test.html
2. Then visit the work ELIZA Terminal (2005) via the link https://www.masswerk.at/elizabot/eliza.html, and try to have your own conversation.

info:
1. [The Original ELIZA in MAD-SLIP](https://wg.criticalcodestudies.com/index.php?p=/discussion/108/the-original-eliza-in-mad-slip-2022-code-critique)
2. Source code of Masswerk's [Eliza bot](https://www.masswerk.at/elizabot/)

Discussion:

- Share your experience of the original conversation (by Weizenbaum) and your conversation with the chatbot:

  - How would you describe your experience of ELIZA (e.g. the use of language, style of conversation, and quality of social interaction)?
  - Can you identify some of the patterns in natural language processing?
  - How would you assess the ability of technology such as this to capture and structure feelings, and experiences? What are the limitations?

## 3. Teachable machines (understanding input and output)

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/10-MachineUnlearning/ch10_2.png" width=600>

https://teachablemachine.withgoogle.com/v1/

- A quick demo (ML: data - model - output)

## 3.1 Exercise in class

1. Train the machine using three different sets of gestures/facial expressions, then observe the predictive results shown as various outputs.
2. Test the boundaries of recognition or classification problems, such as having a different test dataset, or under different conditions such as variable lighting and distance. What can, and cannot, be recognized?
3. What happens when you only use a few images? How does this amount of training input change the machine's predictions?

* Here just to give you some ways to pay attention to computation, and see the relation between parameters and things

## 4. Walkthrough MiniX [10] - Group

- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - NO PEER GROUP FEEDBACK this week
  - **Prepare a group presentation on next Wed**

---
---

## 5. Some food for thoughts

- Work by Gabriel Pereira & Bruno Moreschi : [Recording Art](https://vimeo.com/321866181) (password: recoding)
- Anna Ridler (2021), [Automated Dreaming: Using AI in a Creative Practice](https://www.youtube.com/watch?v=LSasCBJak_k) & her artwork: [Mosaic Virus](https://vimeo.com/399861718)

## 6. Mahine Learning to Pattern Recognition

- (un)Learning?
  - Supervised Learning
    - labelling
    - training dataset (pairs)
    - classification
    - https://thephotographersgallery.org.uk/whats-on/digital-project/exhibiting-imagenet (Exhibiting ImageNet (2019) by Nicolas Malevé)
      - ImageNet by Fei-Fei Li (2009)
      - Computer Vision
      - Invisible labour (Amazon Mechanical Turk)

  - Unsupervised learning
    - <img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/10-MachineUnlearning/ch10_4.gif" width="400">
    - https://aesthetic-programming.net/pages/10-machine-unlearning.html
      - Clustering
      - find similiaries

## 7. ml5.js library

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch10_MachineUnlearning/

Repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch10_MachineUnlearning

- Download the source code [here](https://gitlab.com/aesthetic-programming/book/-/tree/master/public) (download the respository - including the libraries and models)
- index.html > include the ml5.js library
- DOM elements

- Machine generated [chapter](https://aesthetic-programming.net/pages/afterword-recurrent-imaginaries.html)

- Training model: The training process is run in a Python environment with TensorFlow installed. It was developed as a multi-layer, recurrent neural network for character-level language models, and it works well with ml5.js. See the open source code by Cristóbal Valenzuela at https://github.com/Paperspace/training-lstm.

## 8. Reflecting ML

Braindump:
Type of 10 mins on things around AI/ML in your mind.

---

Discussion:
> There are many things we can talk about when it comes to machine learning (or AI). Which specific aspect that interests you the most? and why? (try to substantiate your thoughts with the readings and the concrete experience that you have. - Not to just say something you feel, but substantiate and articulate it)

## 9. Studio time

Work on your miniX
