# Class 03:  21 & 23 Feb | Infinite Loops

**Messy notes:**

[[_TOC_]]

## 0. Announcement

- **NO** Fri shutup and code this week
- 25 Feb (FRI) GUEST SPEAKER: 1400-1600 by Alessandro Ludovico
- Next week: Catch up week
- MiniX [3] and feedback will defer a week

## 1. The concept of loop & time

<img src="http://softwarestudies.projects.cavi.au.dk/images/9/99/Animatedthrobber.gif" width=600>

(Aesthetic Programming, p. 73)

- Loops are control structures
- relate to control and automated tasks -> make decision
- repetitive procedures e.g sound material in music or even animated gifs
- allows the repeated execution of a fragment of source code
  - continues UNTIL a given condition is met
  - becomes INFINITE (or endless if a condition never become false)
  - save efforts -> operational timed

## ⏺️1.2 Discussion (8 mins)

> 🧐 Assume you have read the text book chapter 3: Have you experienced loops in everyday life? What is loop in programming? How's loop relate to time and temporality?

## ⏺️1.3 Loop ex: something simple and concrete

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_8.png)

> Prompt: Draw 20 ellipses and each with a distance of 20 pixels each.

How would you approach this question? Use a paper and try to outline the algorithm (it's ok not in a right spelling - draft the concept first).

## ⏺️1.3 Loop ex: Do it with live coding

Hints:
1. 20 ellipses (how many we need to loop)
2. a distance of 20 pixels (accumulative distance involves arithmetic calculation)
3. use of variable?
4. use of a for-loop (with a condition)
5. shape - ellipse(x, y, size, size)

**loop:**
 - A variable declaration and initialization: Usually starts with 0 (start from somewhere to count)
 - A specified condition: The criteria to meet the condition
 - Action: What you want to happen when the condition is met
 - Loop for next: For the next iteration (usually incremental/decremental).

## 2. Throbber
<img src="https://gitlab.com/siusoon/aesthetic-programming/-/raw/master/Ap2019/class03/browser1.png" width=650>
<img src="https://user-content.gitlab-static.net/e77ec1d81fa507275d6c6fa5fbdc085e86374ffb/68747470733a2f2f6d6f6e647269616e2e6d61736861626c652e636f6d2f75706c6f6164732532353246636172642532353246696d6167652532353246353334373331253235324663303434663930342d373361302d346465342d613034382d3630316536376537393230342e706e672532353246393530783533345f5f66696c7465727325323533417175616c6974792532353238393025323532392e706e673f7369676e61747572653d4d6e693859354666536c373976475651496f433367333939316f673d26736f757263653d6874747073253341253246253246626c75657072696e742d6170692d70726f64756374696f6e2e73332e616d617a6f6e6177732e636f6d" width=650>

- Throbber as a prominent visual icon
- contemporary culture (streams and real-timeness)
  - Buffering icons of death see [here](https://mashable.com/2017/07/12/buffering-icons-ranked/?europe=true#4FSuDdS21Oqr) => many other different names e.g Hourglass cursor, Spinning Wait Cursor, Spinning Beach Ball, Spinning Wheel of Death, etc.
- Opearational and micro(temporal) processes beyond the threshold of human perception
    - streaming, data transmission, networked decisions, signal processing, networked congestion, buffering, etc.
    - (Real)Time and Now  (What is real-time?)

## 2.1 Experience of time (Lammerant 2018):

![](https://media.giphy.com/media/VXxy0at5j484U/giphy.gif)
- "how the actual experience of time is shaped is strongly influenced by all sort of design decisions and implementations, both for humans and machines" (p. 89)
- "time as a common moment, the duration of a certain time, time as cyclic events, historical time, and so on." (p. 89)
- meassured & standardized time (clock > unification of time lengths > economic processes across timezones & spaces)
  - linked to natural rhythms: years, life cycles
- machine times: Real time clock -> discrete time > time rhythm
  - software: update
  - computers and software get links and networked: synchronize times
  - scheduling tasks and times
  - server request (http) > discontinuous time practice
- develop alternative time practices and experiences

Wanna download a screensaver? https://github.com/siusoon/Throb

## 3. ⏺️ Exercise in class (Decode)

<img src="https://aesthetic-programming.net/pages/ch3_2.png" width=300>

RunMe (Not looking at the souce code): https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/sketch3_1/

NB! the technique of code reading and decoding code

🧐 In-class exercise (all together):

Write it down:

**Speculation:**
- Based on what you see/experience on the screen, describe:
    - What are the elements? Come up with a **list** of features.
    - What is moving and what isn’t?
    - How many ellipses are there in the middle?
    - Try to resize the window and see what happens.
- Further questions:
    - How do you set the background color?
    - How does the ellipse rotate?
    - How can you make the ellipse fade out and rotate to the next position?
    - How can you position the static yellow lines, as well as the moving ellipses
in a single sketch?

### 3.1 Tinkering

**Experimentation**
- Tinker with the [source code](https://aesthetic-programming.gitlab.io/book/)
- Try to change some of the parameters, e.g. background(), framerate(), the
variables inside drawElements()
- There are some new functions you can check in the p5.js reference (e.g.
push(), pop(), translate(), rotate())

### 3.2 Mapping

**Mapping**
- Map some of the findings/features from the speculation you have done to the source code. Which block of code relates to your findings?
- Can you identify the part/block of code that responds to the elements that you have speculated on?

### 3.3 Sample Code

```javascript
//throbber
function setup() {
 //create a drawing canvas
 createCanvas(windowWidth, windowHeight);
 frameRate (8);  //try to change this parameter
}

function draw() {
  background(70, 80);  //check this syntax with alpha value
  drawElements();
}

function drawElements() {
  let num = 9;
  push();
  //move things to the center
  translate(width/2, height/2);
  /* 360/num >> degree of each ellipse's movement;
  frameCount%num >> get the remainder that to know which one
  among 8 possible positions.*/
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255, 255, 0);
  //the x parameter is the ellipse's distance from the center
  ellipse(35, 0, 22, 22);
  pop();
  stroke(255, 255, 0, 18);
  //static lines
  line(60, 0, 60, height);
  line(width-60, 0, width-60, height);
  line(0, 60, width, 60);
  line(0, height-60, width, height-60);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
```

- attention to:
  - seperate/custom function (e.g windowResized and drawElements)
  - local variable, and what that means?
  - translate / rotate
  - listening event
  - push/pop
  - frameCount

![](https://dj1hlxw0wr920.cloudfront.net/userfiles/wyzfiles/fc023d7d-db75-4a93-9a91-03f900de1e16.png)

Any questions?

## 4. Transformation

a spatial reconfiguration: translate (moving position), rotate, etc

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_3.png)

 the transformation is done at canvas background level, not at the individual shape/object level.

- `translate(width/2, height/2);`
- `rotate(radians(cir));`
- `push()` & `pop()`: save and restore style/settings

NB: there are also `scale()` (https://p5js.org/examples/transform-scale.html)

## 5. ⏺️Function

🧐 what is the function here? and why do we need to have function?

```javascript
let x = sum(4, 3, 2);   
print(x);
//passing values four as a, three as b, two as c to the function sum
function sum(a, b, c) {
  return a + b + c; //return statement
}
```

vocabulary: function, argument, return values, return type

🧐 Can you point out where is the function, arguments, return values?

### 5.1 ⏺️Exercise in class

You can also type/copy the above code into your own sketch, where it will return the number 9. a, b, c are called "arguments" that are passed to the function (i.e. `sum()`) to do a specific task. The function will return a numeric type and pass to the variable x as the return value.

The function "sum" can be reused if you pass on other arguments/values to it, as for instance another line of code `let y = sum(5,6,7);` and the return value of y would be 18.

pause for 3 mins:
> 🧐 Come up with your own functions and arguments.

## 6. Ending
<img src="https://media.giphy.com/media/V0iBMLuftFImIBIGzV/giphy-downsized-large.gif">

Re sample code:

1. Change the arguments/values, as well as the position/sequence of the sample code to understand the functions and syntax such as the variable `num`, the transformational functions `translate()` and `rotate()`, as well as saving and restoring current style and transformations such as `push()` and `pop()`.

2. This exercise is about structuring code. How would you restructure the sample code so that it is easier for others to understand, but still maintains the same visual outcome? There are no right or wrong answers, but some pointers below might facilitate discussion:
    - You may rename the function and/or add new functions
    - Instead of having drawElements(), you might have something like drawThrobber() and drawLines()?

---

## 7. Arrays

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlD6hgALe1ilW_NSCdqPzBGjl9SF3Y_ZnX7W90Ou28tVsqHfwWOJ3vg_SJAAVU_i5OvKo&usqp=CAU)

- a [list] of data (relate to variable and data types)

```javascript
//example
let words = [] //array -> start with 0
words[0] = "what";
words[1] = "are";
words[2] = "arrays";
console.log(words[2]); //output: arrays
console.log(words.length); //output: 3
```

Similar principle as how we use variables:

1. Declare
2. Initialize/Assign
3. Re(use)

NB!
1. array index starts with [0]
2. `array.length` -> how many
3. vocabulary: array index, array value

Others:
`array.push(value)` and `array.splice()`

## 7.5 ⏺️ TEST

1. draw 20 horizontal lines and align them vertically. Each line with a distance of 20 pixels.

```
-------------
-------------
-------------
-------------
-------------
...
```

2. how to code something like below?

```
*
**
***
****
*****
```

## 8. Asterisk Painting (advanced sketch - just for learning)

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/Asterisk_Painting.gif" width=650>

- RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/
- Source code [here]( https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch3_InfiniteLoops/sketch.js)

⏺️  Use the decoding method & try to speculate, experiment, and map your thoughts to the source code (3-4 people - 15 mins).

Write down on your paper first:

**Speculation:**
- Describe what you see/experience on screen.
    - What are the elements on screen?
    - How many asterisks are there on screen and how are they arranged?
    - What is moving and how does it move?
    - What makes each asterisk spin/rotate and when does it stop to create a new one?
    - Can you locate the time-related syntax in this sketch?

**Experimentation:**
- Change some of the code's arguments
    - Try to change some of the values, e.g. the values of the global variables
    - Which new syntax and functions didn't you know? (Check them out in the p5.js reference.)
- Mapping:
    - Map the elements from the speculation to the source code

### 8.1 Conditional statements

```javascript
if(sentences >= maxSentences){  //reach the max for each asterisk
   //move to the next one and continues;
}
```

`let maxSentences = 77;`

### 8.2 Loops (for loop)

Look at *Asterisk Painting* again:

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_5.png" width=600>

```javascript
/*calculate the x-position of each asterisk as
an array (xPos[]) that starts with an array index[0]*/
for(let i = 0; i < xPos.length; i++) {
  xPos[i] = xPos[i] * (xDim / (xPos.length+1));
}
/*calculate the y-position of each asterisk as
an array (ypos[]) that starts with an array index[0]*/
for(let i = 0; i < yPos.length; i++) {
  yPos[i] = yPos[i] * (yDim / (yPos.length+1));
}
```
🧐 Discuss with the one next to you (8 mins):

Why a for-loop is used here? Can you discuss the usage and explain how it works?

### 8.3 While loops

- another type of loop for executing iterations. The statement is executed until the condition is true and stops as soon as it is false.

🧐 Discuss with the one next to you (10 mins):

`while(millis() < wait){}`

> Explain what is the above line? and what's the role of the above line in the entire sketch?

## 9. Walkthrough MiniX[3] - Designing a throbber

- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - FEEDBACK  
