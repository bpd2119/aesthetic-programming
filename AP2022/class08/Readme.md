# Class 07:  28 & 30 Mar | Pause Week

**Messy notes:**

[[_TOC_]]

## 1. Workshop by [Jazmin Morris](https://linktr.ee/jmmorris) on the 28 Mar

Bio: Jazmin Morris is a Creative Computing Artist and Educator based in London. Her personal practise and research explore representation and inclusivity within technology. She uses virtual reality and open-source game development tools to create experiences that highlight issues surrounding gender, race and power; focusing on the complexities within simulating culture and identity.

Jazmin is the Lead Digital Tutor on the Graphic Communication Design course at Central Saint Martins as well as an Associate Lecturer on the BSc in Creative Computing at UAL’s Creative Computing Institute, she works in their Outreach Department running a club that encourages young people to engage in Creative Computing.

1. Read [this](https://societycentered.design/) (approx 5-min)
2. Read [this](https://github.com/processing/p5.js/blob/main/CODE_OF_CONDUCT.md) (approx 5-min)
3. Play [this](https://blacktransarchive.com/) (approx 10-min)

https://docs.google.com/document/d/1zdiGaC02F_keELJiIdKpe6UQLzqSOE3muZTMLSVtvV0/edit#

---
---

## 1.5 Reflection

- Inbetween Binary
- MiniX[6]: Games with objects

## 2. Forming groups

A group of 4:

Deadline: by Friday

Go to 👉 https://cryptpad.fr/sheet/#/2/sheet/edit/PuPZFgguYDg6XgK89u2ve29+/  (second sheet)

### Collaborative coding:
- p5 live: https://teddavis.org/p5live/
- teletype: https://teletype.atom.io/  (github account is needed)

### Deadline for miniX [1-7]

4 Apr 2022 - 08.00 (ReadMe and RunMe)
- cross check with your peers
- oral exam and re-exam

## 3. Oral Exam

**Prerequisite**
1. Read the required reading and instructional video before coming to the class as stated in the syllabus.
2. Active participation in class/instructor discussion and exercises, and presentation of your miniX + peer-feedback. Ready to engage in activities, and contribute to the class through discusson, respectful and attentive participation
3. Submit all the WEEKLY mini exercises (mostly individual with a few group works) in a timely manner
4. Provide WEEKLY peer feedback online in a timely manner
5. Submission of the final group project and participate in the final group presentation- in the form of a “readme” and a “runme” (software) package

!NB: 20 ECTS is equivalent to around 28 hours per week, including lecture and tutorial.

**Oral exam:**

- 4 mins presentation based on the pre-assigned question - related to the final project
- 4 mins presentation on one of the miniX (will be asked by the examiner)
- 12 mins discussion

## 4. Walkthrough MiniX[7] - Revisit the past
- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - NO FEEDBACK this week

## 5. ⏺️Presentation by students on MiniX [6]

![](https://media.giphy.com/media/l0MYS1WzKiLlEeQJq/giphy.gif)

0. Group of 4
1. Present to one another: 5 mins each
2. Discuss the ideas and questions together

## 5.5 Further discussion
> What does it mean by programming/making critical work? Where is critical inquiry in your miniX, and how would you articulate critical-aesthetic in the context of programming?

## 6. Test 1

[presenter mode](https://presenter.ahaslides.com/presentation/2827933)

```javascript
for(let x = 1; x < 5; x++) {
  console.log(x);
}
```

A. 11111

B. 12345

C. 1234

D. 5555

👉 https://audience.ahaslides.com/czm0baig5j

## 7. Test 2

```javascript
let arr = [1,2,3,4,5];
arr.splice(1,3);
arr.push(9);
console.log(arr)
```

A. Returns [4,5]

B. Returns [9,1,5]

C. Returns [1,2,3,4,5,9]

D. Returns [1,5,9]

👉 https://audience.ahaslides.com/czm0baig5j

## 8. Test 3
```javascript
let x=8;  
  if(x>9) {  
    console.log(9);  
  }else{  
    console.log(x);  
  }  
```

A. 9

B. 0

C. 8

D. Undefined

👉https://audience.ahaslides.com/czm0baig5j

## 9. Test 4

Which one of the following operator returns false if both values are equal?

A. !

B. !==

C. !=

D. All of the above

👉 https://audience.ahaslides.com/czm0baig5j
