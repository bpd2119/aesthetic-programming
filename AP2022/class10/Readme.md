# Class 10:  20 Apr | Algorithmic Procedures

**Messy notes:**

[[_TOC_]]

## 0. Announcement

- Next Fri guest lecture by Geoff Cox - [Machine Seeing and Invisual Literacy](https://arts.au.dk/en/aktuelt/arrangementer/vis/artikel/friday-lecture-geoff-cox-machine-seeing-and-invisual-literacy-2)
- Next week (Fri) no shutup and code
- MiniX[8] - how does it work for you as a group and with group feedback?

## 1. Discussion on algorithmic procedures

![](https://media.giphy.com/media/dY0A2zOKQjzEigiR58/giphy.gif)

⏺️ Discussion with the one next to you:

10 mins:

> What is algorithm? What do you know about algorithm? (if you have read the assigned text, you may also discuss some of the points there)
> Can you give an everyday example (detailing the computational logic) of an algorithm that you have used or experienced?

Based on Bucher's text:
- multiple algorithms -> technical and social
- instructions - steps - procedures - algorithms => recipe-like algorithmic procedures
- about order, sequence and sorting
- "steps then produce desired outcomes"
- "implies a simplification of the problem at hand" -> bg: engineering perspective -> depends on technical considerations, and data structures (p. 22)
- "flow of control" -> tied with conditional structure and with alternative paths

## 2. Procedure operations

- One way of thinking: an algorithm demonstrates the systematic breakdown of procedural operations to describe how an operation moves from one step to the next.

Flowcharts:
- "Flow of the chart -> chart of the flow"
- steps and sequences
- representation

## 3. Flowcharts

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/9-AlgorithmicProcedures/ch9_1.png)

RunMe: https://dobbeltdagger.net/VocableCode_Educational/

- Oval: Indicates the start or end point of a program/system. (But this requires further refection on whether all programs have an end.)

- Rectangle: Represents the steps in the process.

- Diamond: Indicates the decision points with "yes" and "no" branches.

- Arrow: Acts as a connector to show relationships and sequences, but sometimes an arrow may point back to a previous process, especially when repetition and loops are concerned.

Challenges:

1. Translating programming syntax and functions into understandable, plain language.
2. Deciding on the level of detail on important operations to allow other people to understand the logic of your program.

## 4. ⏺️Exercise 1 (per group)

```javascript
function setup() {
  let multi = ['🐵','🐭','🐮','🐱'];
  for (let species = 0; species < multi.length; species++) {
    console.log(multi[species]);
  }
}
/*output
🐵
🐭
🐮
🐱
*/
```

The task is to draw a flowchart based on this program (10 mins)
- imagine your target audience is someone who doesn't know coding.

## 5. ⏺️Exercise 2 (per group)

Sorting is a common algorithm in digital culture, and recommendation lists on Spotify, Amazon, Google and Netflix, will be familiar to you.

Watch this video together: * Marcus du Sautoy, “The Secret Rules of Modern Living: Algorithms,” / "How does Google work?" BBC Four (2015),https://www.bbc.co.uk/programmes/p030s6b3/clips.


Think about the "algorithmic procedures" required to program something to solve the sorting task set below - sorting numbers

Task: 20 mins

Generate a list of x (for example, x = 1,000) unique, random integers between two number ranges. Then implement a sorting algorithm and display them in an ascending order. You are not allowed to use the existing sort() function in p5.js or JavaScript. How would you approach this problem? You have to draw the algorithm as a flowchart with the focus on procedures/steps, but not the actual programming syntax. Present your findings to the class

Notes:
- This is the task about thinking algorithm, but also to learn to how break a task into smaller steps.

## 6. Walkthrough MiniX [9] - individual and group

- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - NO PEER GROUP FEEDBACK this week

## 7. notes:
- https://editor.p5js.org/siusoon/sketches/7g1F594D5
