# Class 06:  14 & 16 Mar | Auto-Generator

**Messy notes:**

[[_TOC_]]

## 0. Auto-generator

- Generative Art / NFT Art e.g [NudemeNFT](https://nudemenft.com/)
  - machine creativity
  - adaptive system
- AI - automating tasks
  - AI-assisted [automatic warehouse](https://www.youtube.com/watch?v=qt8ux1EYQU4)
    - labour forces, capitalism
  - automated driving cars
    - responsibility, accountability

## 1. Abstract machines

![](https://aesthetic-programming.net/pages/ch5_1.png)
- self operating machines
- universal computing machine -> process input and produce output
- Turing machine: 6 operations
  - read, write, move left, move right, change state, halt/stop
- Implication: capability to compute numeric tasks and automate various processes
- Video: Turing Machines Explained - Computerphile by Assistant Professor Mark Jago: https://www.youtube.com/watch?v=dNRDvLACg5Q&list=PLzH6n4zXuckodsatCTEuxaygCHizMS0_I&index=8

**Take away keywords**:

- performing rules / instructions
- self-operating system

## 2. Generative/rule-based art

<img src="https://whitneymedia.org/assets/image/822825/medium_WMAA_PROGRAMMED_06_PS_SM.jpg" width="750">

_Wall drawing #289_ by Sol Le Witt (1978)
Ref: https://massmoca.org/event/walldrawing289/

Instructions:
1. 24 lines from the center
2. 12 lines from the midpoint of each of the sides
3. 12 lines from each corner

- write it in the form of code: https://github.com/wholepixel/solving-sol/blob/master/289/cagrimmett/index.html
- RunMe: http://cagrimmett.com/sol-lewitt/289.html
- Ref: https://cagrimmett.com/sol-lewitt/2016/10/30/sol-lewitt-289/

> The idea becomes a machine that makes art. - LeWitt

## 2.5 ⏺️Thinking about simple rules > complex patterns

Another examples - [generative circles](https://editor.p5js.org/siusoon/sketches/4xCzybWK3)
- draw a 2 dimensional grid with circles
- start from small and increase the size of all the circles
- decrease the size when it reaches a certain size

a. Two people in a group

b. everyone think of at least 3 rules that lead to a complex pattern. Write down the 3 rules and pass to your neighbor

c. the other person needs to execute your rules (pretending to be a computer) and draw the emergent pattern.

## 3. Generative / rule-based art > Entropy and emergence

<img src="https://gitlab.com/aesthetic-programming/book/-/raw/master/source/5-AutoGenerator/ch5_8.jpg" width="750">

_Joan Truckenbrod, Entropic Tangle (1975)_

Other examples:
- flocking: https://p5js.org/examples/simulate-flocking.html
- fractal tree: https://editor.p5js.org/marynotari/sketches/BJVsL5ylz

ideas:
- machine creativity (intentionality, control) -> questioning the centrality of human
- simulation of nature
- entropy -> ordering, predictability -> evolve over time

> "Generative art refers to any art practice where [sic] artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art." - Galanter, 2003

## 4. 10 PRINT

![](http://bogost.com/wordpress/wp-content/uploads/2012/11/10PRINT_01.jpg)

BASIC programming language (Commodore 64 in the 80s): `10 PRINT CHR$(205.5+RND(1));: GOTO 10`
![](https://user-content.gitlab-static.net/ccdcf454559c501a2cea8409af212466397d45ca/687474703a2f2f746578742d6d6f64652e6f72672f77702d636f6e74656e742f75706c6f6164732f323031392f30382f31307072696e742d7472616d6d656c6c2d687564736f6e2e6a7067)

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch5_AutoGenerator/

Following 2 RULES:

1. Throw a dice and print a backslash half the time
2. Print a forward slash the other half of the time

```javascript
let x = 0;
let y = 0;
let spacing = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  stroke(255);
  if (random(1) < 0.5) {  
    //backward slash
    line(x, y, x+spacing, y+spacing);
  } else {
    //forward slash
    line(x, y+spacing, x+spacing, y);
  }
  x+=spacing;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
```

## 5. 🧐In-class ex

Try to modify the existing rules, for example:

- Can we change the size, color, and spacing of the slashes?
- Can we have outputs other than just the backward and forward slashes?

Appropriation: https://twitter.com/search?q=%2310print&src=typd

**Your task in class is to create a sketch with a clear set of rules that operates like a modified version of 10 PRINT. Post a link of your sketch next to the pad: https://pad.vvvvvvaria.org/AP2022** (You may want to use p5 web editor: https://editor.p5js.org/)

## 6. 🧐In-class discussion

ALL:
What is randomness, and what is the application of randomness in digital culture?

Other similar syntax: `random()` and `noise()`

- Perlin noise by Ken Perlin, [`noise()`](https://p5js.org/reference/#/p5/noise)
- only returns the range of 0-1
- a more organic appearance
- non uniform distribution
- "it produces a naturally ordered (“smooth”) sequence of pseudo-random numbers." -> good for simulating natural/life-like behaviors
- the generated values are related to each other -> useful for generating random patterns
- example [here](https://blog.federicopepe.com/2016/06/random-vs-perlin-noise/)

Another example:
https://podcast.pcdaarhus.net/ by Margrete Lodahl Rolighed

> ⏺️Discuss why you have used random/noise function in your previous mini_exercises?
What is the significance of randomness in your program? What is pseudorandomness?

⏺️Drawing on the text "Randomness":

- **How** is control being implemented in 10 PRINT?
- **What** might the (un)predictability of regularity be?
- **What** is randomness to a computer?
- **Discuss** the use and the role of randomness in 10 PRINT, and more generally in the arts, including literature, and games?

## 7. Walkthrough MiniX[5] - A Generative Program
- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - FEEDBACK  

## 8. Preparation for Wed

- Look over Langton's ant history + the sample source code for tomorrow
  - RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch5_AutoGenerator/sketch5_1/
  - Source code: https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch5_AutoGenerator/sketch5_1/sketch.js
  - text book ch. 5 > Langton's Ant
- If you have time: research a bit on The game of life by the British mathematician John Conway in 1970

---
---

## 9. ⏺️Short presentation by students on MiniX [4]

![](https://media.giphy.com/media/l0MYS1WzKiLlEeQJq/giphy.gif)

1. note taking and questions: https://pad.vvvvvvaria.org/AP2022
2. Need one volunteer per presentation

## 10. Queer Life forms - generative (language) art

Love & Care to people and to the world

1. love-letters (1953) by Christopher Strachey
  - remade by Nick Montfort: https://nickm.com/memslam/love_letters.html
    > "You are my — Adjective — Substantive," and "My — [Adjective] — Substantive — [Adverb] — Verb — Your — [Adjective] — Substantive."

- ![image](https://wordandimage.files.wordpress.com/2012/04/4194960757_de88781e77_o.jpg)
2. [A House of Dust](http://www.zachwhalen.net/pg/dust/) (a simulation of a 1967 poem by Alison Knowles and James Tenney) by Zach Whalen | see more background info [here](http://www.artbytranslation.org/_pdf/HOUSE_OF_DUST_JOURNAL_25_08_2016_BDEF_PREVIEW.pdf)

Rules:
```
A HOUSE OF (a material)
 (a site/situation)
  USING (a light source)
   INHABITED BY (list inhabitants)
```
*in which combinations of the variables were randomly generated. (chance operations and language)
*originally written in FORTRAN
- interpretable score (Higgins, et al, 2012)
- meaning of the work beyond language
- influenced by [Fluxus movement](https://www.widewalls.ch/what-is-fluxus/) and John Cage (chance operations)

3. Diastic algorithm + Machine Learning (NLP): [Recurrent queer imaginaries](https://digital-power.siggraph.org/piece/recurrent-queer-imaginaries/) by Helen Pritchard and Winnie Soon

4. [Digital Love Languages](https://lovelanguages.melaniehoff.com/syllabus/) by Melanie Hoff

> ⏺️ How would you understand randomness in this context? What's the significance of it? To what extent do you consider the machine to be an active agent in generative systems?

# 11. Langton's Ant (1986) by Computer Scientist Christopher Langton

Beyond the human!

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/5-AutoGenerator/ch5_6.gif)
![](https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/raw/master/source/5-AutoGenerator/ch5_5.gif)

Langton's Ant is a classical mathematical game that simulates the molecular logic of a living ant. The simulation of the cell's state is inspired by the classical Turing machine that can perform computational tasks with the manipulation of symbols on a strip of tape according to a set of rules. Based on simple rules, an ant is considered to be the sensor that processes the cell's data as input, then the cell will change it's color by having the ant moving in four possible directions. Gradually, the ant will turn the grid into a more complex system that exhibits emergent behavior over time.

**Rules -> generate complex and emergent behaviors**

RunMe: https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch5_AutoGenerator/sketch5_1/

Source code: https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch5_AutoGenerator/sketch5_1/sketch.js

Stages:
- simple symmetrical patterns
- emergent highway pattern
- constantly moving/changing endlessly

Rules:

1. If the ant is at a white cell, it turns right 90 degrees and changes to black, then moves forward one cell as a unit.
2. If the ant is at a black cell, it turns left 90 degrees and changes to white, then moves forward one cell as a unit.

To slow down the program (demo):

- `let grid_space = 5;` in Line 2: If you change the value to 10, everything will be enlarged.

- `frameRate(20);` in Line 26: Lower the frame rate value to help slow down the program.

- `draw()` in Line 28: This function contains a for-loop where n is the ant's number of steps. If so you can reduce the n < 100 to n < 1 (in Line 31), i.e `for (let n = 0; n < 100; n++) {` this instructs the program to only process n steps per frame.

Logic/background:
1. grid as cell units
2. Ant has 4 possible directions: UP, RIGHT, DOWN, LEFT
3. Initiate ant direction: UP
4. 2 states: color of the cell on/off, black/white
5. increase in complexity

Functions:
- `function setup()`
- `function drawGrid()`: 2-dimensional grid -> using nested for-loop
- `function draw()` - check and perform the specific rules
- `function nextmove()` - determine the next movement
- `function checkEdges()` - check edges and continue at the other end.

## 12. 🧐In-class discussion (4 people in a group)

> It is clear that in simulating and abstracting living systems — such as the complex behaviour of insects — there is a focus on emergent and generative process over end-result. In terms of creative process this allows us to think about how rules and instructions can produce complexity and other forms of control and agency.

- Give yourself sometime to read and tinker with the code, as well as to observe **the different stages** of Langton's Ant.
- Can you think of, and describe, other systems and processes that exhibit emergent behaviour?
- Each group try to bring one question/reflection to the class

## 13. 2d arrays

variables -> arrays -> 2d arrays

> an array of other arrays -> expressed as array[][]

e.g declare and assign value:

- `let arr = new Array(cols);`
- `arr[i] = new Array(rows);`
- arr[][]

```javascript
function drawGrid() {
 cols = width/grid_space;
 rows = height/grid_space;
 let arr = new Array(cols);
 for (let i = 0; i < cols; i++) { //no of cols
   arr[i] = new Array(rows); //2D array
   for (let j = 0; j < rows; j++){ //no of rows
     let x = i * grid_space; //actual x coordinate
     let y = j * grid_space; //actual y coordinate
     stroke(0);
     strokeWeight(1);
     noFill();
     rect(x, y, grid_space, grid_space);
     arr[i][j] = 0;  // assign each cell with the off state + color
   }
 }
 return arr; //a function with a return value of cell's status
}
```

## 14. Other Inspiration

![](https://media.giphy.com/media/tXlpbXfu7e2Pu/source.gif)
- [Game of life by John Conway](https://www.youtube.com/watch?v=ouipbDkwHWA): Each cell interacts with other, directly adjacent, cells, and the following transitions occur
    - Any live cell with fewer than two live neighbors dies, as if by underpopulation.
    - Any live cell with two or three live neighbors lives on to the next generation.
    - Any live cell with more than three live neighbors dies, as if by overpopulation.
    - Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

## 15. Mid way evaluation

https://pad.vvvvvvaria.org/AP2022_midway

- what have you learnt in Aesthetic Programming?
- what is the most remarkable example/theme/ex/reading/thing?
- vibe/environment/safe space/peers/community
- motivation/difficulties/challenges
- class/content/theme/activity
- workload/miniX/feedback/grouping
- instructor and shutup and code (thur/Fri)
- Any other things? e.g zoom/sickness
