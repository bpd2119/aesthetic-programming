# Class 01: 7 & 9 Feb | Getting Started

**Messy notes:**

AGENDA:

[[_TOC_]]

## 1. Introduction

<img src="https://media.giphy.com/media/137EaR4vAOCn1S/source.gif">

#### AP2022's Teaching team:
- [Winnie Soon](http://www.siusoon.net)
- [Jakob Stougaard Wang](https://gitlab.com/jakobwangau)
- [Jonas Hegelund Rasmussen](https://gitlab.com/jonhegras)

#### The concept of Messy Notes, Gitlab, Aesthetic Programming

- Diversifying approaches and curriculum
- Opening Question about Aesthetic Programming (Group discussion: 5 mins - in English):

  > What's your programming experience?

  > What are the programming langauages that you know already e.g html, css, javascript, processing, Java, C++, Python, R, etc?

  > Why do you think you need to know programming in Digital Design Programme?

  > Beyond the STEM approach, how could we imagine learning to program otherwise?

### 1.1. Round the table introduction

<img src="https://media.giphy.com/media/3o6MbsWjZwURvye0ko/giphy.gif">

> (✋) How many of you have some sort of programming/scripting experience? (even a little)

Within 5 lines:

1. Your name
2. What's your programming experience?
  - any programming/scripting experience e.g HTML, CSS, JS, C#, Python, etc
  - what do you feel about programming?
3. What's your feeling right now?

### 1.2 Aesthetic Programming

- How is AP different from code academy, or most programming books in the market e.g Programming x for dummies
- My RQ:
> Beyond the focus on obtaining problem solving and technical skills, how to develop a more humanistic (cultural, creative and critical) approach to programming, especially addressing gender disparity, societal issues, social justice and alternative imagination in relation to computational culture?

- Focus: Exploratory/Experimentation - Cultural/Critical approaches - Open up access (different doors for you e.g designers, artists, programmers, creative coders, etc)
- Software: not as an end product but a discovery **process**
- My source of inspiration:
<img src="https://imgcdn.saxo.com/_9780262062749" width=150>
<img src="https://mitpress.mit.edu/sites/default/files/styles/large_book_cover/http/mitp-content-server.mit.edu%3A18180/books/covers/cover/%3Fcollid%3Dbooks_covers_0%26isbn%3D9780262015424%26type%3D.jpg?itok=E7XnDQ4h" width=150>
<img src="https://unipress.dk/media/11830/9788798844044_cover.jpg?width=360" width=150>

### 1.3 Coding vs Writing (literacy)

<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.thenile.io%2Fr1000%2F9780262036245.jpg" height=400>

[Coding literacy](https://mitpress.mit.edu/books/coding-literacy) (Vee, 2017)

> Literacy no longer implies just reading for comprehension, but also reading for technical thought as well as writing with complex structures and ideas (p. 48).

> With reference to writing/reading literacy, Why coding is an important (useful) skill for everyday? What are the implications that coding is framed under the umbrella of literacy and for whom? (and what if coding is just framed under Computer Science?)

> Coder vs Writer? (learning to code does not mean that you need to be a programmer. Similarly learning to write doesnt mean that you will become a writer or novelist.)

> programming > wider social relations (Vee, 2017)

## 2. Syllabus, curriculum and expectation

![](https://aesthetic-programming.net/theme/images/coverGraph.svg)
- TextBook: [Aesthetic Programming: A Handbook of Software Studies](http://www.aesthetic-programming.net) by Winnie Soon & Geoff Cox (2020), published by Open Humanities Press
- Reading - take notes (and in relation to Software Studies)
- Description & learning outcomes (+ Participation & Prerequisites)
- Groups, Underground buddy, feedback, individual
- Class protocol
- KEY learning tool: [Week ex and feedback](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - the concept of "Low Floors, High Ceilings" (Seymour Papert, 1980)
- Plan your schedule from Mon to Sun
  - Mon (prepare the readings before class)
  - Wed (deadline of miniX - feedback)
  - Thur (instructor class)
  - Fri shut up and code camps (first 6 sessions)
  - Sun (deadline of miniX)
- Corona situation - teaching - illness

## 3. MiniX [1] - Readme and Runme
- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)

What to do before Wed:
1. Watch the 5 videos on Brightspace (Web browsers, ATOM, p5.js, Gitlab, Markdown/Readme)
2. Register a Gitlab account and post your link to [here](https://cryptpad.fr/sheet/#/2/sheet/edit/PuPZFgguYDg6XgK89u2ve29+/) under your name in column A.

---

## 4. First sketch (on ATOM)

- Check Web browers
- Check Atom
- Check p5.js library  - What is JavaScript? and why [p5.js](https://wg.criticalcodestudies.com/index.php?p=/discussion/120/week-3-p5-js-and-access-discussion-starter#latest)?
- Check GitLab
- Check Markdown

### 4.1 All together

Check RUNME sketch & repository: https://aesthetic-programming.gitlab.io/book/

```js
function setup() {
  // put setup code here
  createCanvas(640,480);
  print("hello world");
}
function draw() {
  // put drawing code here
  background(random(50));
  ellipse(55,55,55,55);
}
```
- Code explain: setup(), draw(), createCanvas(), background(), ellipse()
- `function setup()` → Code within this function will only be "run once" by the sketch work file. This is typically used to set the canvas size to define the basic sketch setup.

- `function draw()` → Taking cues from drawing practice in visual arts, code within this function will keep on looping, and that means `function draw()` is called on for each running frame. The default rate is 60 frames/times per second

- the symbol `//` (comments): referring to text that are written for humans but not computers. This means a computer will automatically ignore those code comments when it executes the code. or `/*……………*/`: indicates multiple lines of code comments.

#### 4.1.1 Web console

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/1-GettingStarted/ch1_8.png)

Ctrl + Shift + K for Linux/Windows,
Option + Command + K for Mac

```javascript
print("hello world");
console.log("hello world");
```

> What is this web console thing?

#### 4.1.2 File/Directory

![](path.png)

- (html, script, gitlab)

Where do we use this stuff?

1. In HTML
```
<a href="../in/here.html">…</a>
```
2. Image source
```
<img src="assets/abc.jpg" alt="">
```
3. Linking CSS/Javascript libraries
```
<script src="../libraries/p5.js"></script>
<script src="../libraries/p5.sound.js"></script>
<script src="sketch01.js"></script>
```

### 4.2 Reading the reference guide

p5.js > Reference

[ellipse()](https://p5js.org/reference/#/p5/ellipse)

🗒Ex: Add another 'thing' in your sketch?

## 5. Ending ...

<img src="https://media.giphy.com/media/V0iBMLuftFImIBIGzV/giphy-downsized-large.gif">

- [MiniX [1] - RunMe and ReadMe](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - the concept of RunMe and ReadMe (writing code & writing about code) > cultivate **reflection** by writing code (RunMe) and writing about code (ReadMe)
  - related to [runme.org](http://runme.org/) in 2003 > software art (repository)
      - [readme festival](https://monoskop.org/Readme): promoted the artistic and experimental practice of software, which took place at the Marcros-Center in Moscow (2002), Media Centre Lume in Helsinki (2003), Aarhus University and Rum46 in Aarhus (2004), and HMKV in Dortmund (2005).
      - an annual festival for art and digital culture, transmediale had started to use 'artistic software' or 'software art' from 2001-2004.
  - additional inspirations
  - low floors & high ceilings
  - group **peer-feedback** and how
  - think about your underground buddy
  - **Start to think about it! Prepare your idea before Thur instructor class.**
- Sharing/advices from last year students:
https://pad.vvvvvvaria.org/AP2021

if there is time - collaborative live coding: https://teddavis.org/p5live/
