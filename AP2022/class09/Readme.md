# Class 09:  4 & 6 Apr | Vocable Code

**Messy notes:**

[[_TOC_]]

## 1. Forming groups

Go to 👉 https://cryptpad.fr/sheet/#/2/sheet/edit/PuPZFgguYDg6XgK89u2ve29+/  (second sheet)

### Collaborative coding:
- p5 live: https://teddavis.org/p5live/
- teletype: https://teletype.atom.io/  (github account is needed)

## 2. Language and Code

### 2.1 Electronic Literature

[Electronic Literature: What is it?](https://eliterature.org/pad/elp.html) (Hayles 2007)

> In the contemporary era, both print and electronic texts are deeply interpenetrated by code. Digital technologies are now so thoroughly integrated with commercial printing processes that print is more properly considered a particular output form of electronic text than an entirely separate medium. Nevertheless, electronic text remains distinct from print in that it literally cannot be accessed until it is performed by properly executed code. The immediacy of code to the text's performance is fundamental to understanding electronic literature, especially to appreciating its specificity as a literary and technical production. Major genres in the canon of electronic literature emerge not only from different ways in which the user experiences them but also from the structure and specificity of the underlying code. Not surprisingly, then, some genres have come to be known by the software used to create and perform them

- associated with print literature
- hypertext link & the web
  - hypertext & interactive fictions -> hybrid forms  e.g [My boyfriend came back from the war](https://anthology.rhizome.org/my-boyfriend-came-back-from-the-war) (1996) by Olia Lialina   
- generative text/literature e.g love letters
- experimental book e.g Aesthetic Programming
- narrative games
- code poetry & codework

### 2.2 code and text

![](ccs_01.png)

- code with symbols (language structure) and as poetry: read aloud (poetry reading)
- code is both script and performance -> ready to do something
- analogy to speech (do things with words + code)
- Voice: instability - human subject  

## 3. Sample Code - Vocable Code

![](https://aesthetic-programming.net/pages/ch7_1.jpg)

- RunMe: https://dobbeltdagger.net/VocableCode_Educational/
- Coding source code + critical writing operate together = Queer Code
  - requires a different kind of reading & writing
  - expose the material and linguistic tensions between writing and reading
- hierarchy between the source and its results

## 4. Decoding Vocable Code

1) Take a look at the RunMe for 1-2 mins, describe the characteristics of text, how the text is displayed and performed?

  - There is always text on the black screen/canvas.
  - The text moves upwards and mostly downwards, but also sometimes slowly oscillates between the two.
  - The text fades over time.
  - The text varies in size.
  - Some of the text’s content overlaps, but there are at least ten different or unique texts.
  - For each new batch of text shown on screen, you can hear a voice speaking one of the texts.
  - There is a maximum limit of the text appearing simultaneously on screen. (Similar to the previous object-oriented approach, the text is continuously generated on screen if certain conditions are met.)
  - Can you continue the list?

2) If I tell you the code is using an object-oriented approach, can you describe the properties and methods of the class?
  - Decode when and how (new) text objets are created/removed?

3) Thinking

- How's this codework (code poetry) different from what you used to write?
- Can you read the code aloud?
- What are these expressive and performative qualities of the piece?

## 5. JSON

- an open-standard, independent file format
- widely used for data storage and communication on the internet, and in software applications.
- can be read and processed by many programming languages such as JavaScript.
- data manipulation: such as retrieving and displaying data on a screen in any color, size, and at any tempo.
- separation of data and computational logic

```JSON
{  "description": "This file contains the meta data of queer text",  
   "condition": "yourStatement cannot be null",  
   "copyLeft": "Creative Common Licence BY 4.0",  
   "lastUpdate": "Apr, 2019",
   "queers":  
  [    
    {"iam": "WinnieSoon",
    "yourStatement": "not fixed not null",
    "myStatement": "not null not closed"},
    {"iam": "GeoffCox",
    "yourStatement": "queer and that means queer",
    "myStatement": "null"  },
    {"iam": "GoogleAlgorithm",
    "yourStatement": "not a manifesto",
    "myStatement": "here"}
  ]
}
```

full json voice file: https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch7_VocableCode/voices.json

many other json files:
- Corpora - A repository of JSON files by Darius Kazemi (n.d.), https://github.com/dariusk/corpora/tree/master/data.

## 6. JSON in practice (hands-on)

In p5.js, we use the function `loadJSON()`

![](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/7-VocableCode/ch7_4.png)

https://editor.p5js.org/siusoon/sketches/MY85q7miY

**Step 1: Prepare your json file**

1. make a json file -> save it as voices.json

```json
{  
  "description": "This file contains the meta data of queer text",  
  "condition": "yourStatement cannot be null",  
  "copyLeft": "Creative Common Licence BY 4.0",  
  "lastUpdate": "Apr, 2019",
  "queers":  
  [    
    {
      "iam": "WinnieSoon",
      "yourStatement": "not fixed not null",
      "myStatement": "not null not closed"
    },{
      "iam": "GeoffCox",
      "yourStatement": "queer and that means queer",
      "myStatement": "null"  
    }, {
      "iam": "GoogleAlgorithm",
      "yourStatement": "not a manifesto",
      "myStatement": "here"
    }
  ]
}

```

**Step 2: loadJSON (to load the specific file and path)**

```javascript
let whatisQueer;

function preload() {
  whatisQueer = loadJSON('voices.json');
}
```

**Step 3: Process the JSON file (selected lines)**

```javascript

function setup(){
  makeVisible();
}

function makeVisible() {
  //get the json txt
   let queers;
   queers = whatisQueer.queers;
  //which statement to speak - ref the json file
   let WhoIsQueer = int(random(queers.length));
   console.log(WhoIsQueer);
   //let makingStatements = int(random(2.34387, 3.34387));
  // SpeakingCode(queers[WhoIsQueer].iam, makingStatements);
   SpeakingCode(queers[WhoIsQueer].iam, queers[WhoIsQueer].yourStatement);
}
```

**Step 4. Use it**

```javascript
function SpeakingCode(iam, makingStatements) {
	let getVoice = "voices/" + iam + "-" + makingStatements + ".wav";
	console.log(getVoice);
}
```
I simply **use** the data to simply display them on the web console. You can use it to print on the web page, or play sound files (like in the artwork Vocable Code).

### 6.1 rules

- Data is stored in name/value pairs, e.g. "copyLeft": "Creative Common Licence BY 4.0" and the pair are separated by a colon.
- All property name/value pairs have to be surrounded by double quotes.
- Each data item is separated by commas.
- Square brackets "[]" hold arrays.
- Curly braces "{}" hold objects as there are many object instances that share the same structure.
- Comments are not allowed.
- No other computational logics like conditional structures or for-loop can be used.

## 7. Other functions

- `loadFont()`
- `textFont()`
- `textAlign()`
- `noStroke()`
- `text()`

## 8. Walkthrough MiniX[8] - E-Lit
- See [here](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex)
  - BRIEF
  - GROUP FEEDBACK this week

---
---

## 8.5 MiniX

check: https://cryptpad.fr/sheet/#/3/sheet/edit/daefa731b968da792c92d08c63ce43a3/

- the concept of portfolio
- demonstrate your progress, thinking and outcome

Just a short walkthrough on Final Project: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/ex

## 8.9 Language and Code

In Katherine Hayles, My Mother Was a Computer: Digital Subjects and Literary Texts :

Language & Code:

> Code that runs on a machine is performative in a much stronger sense than that attributed to language. When language is said to be performative, the kinds of actions it “performs” happen in the minds of humans, as when someone says, “I declare this legislative session open” or “I pronounce you husband and wife.” Granted, these changes in the mind can and do result in behavioral effects, but the performative force of language is nonetheless tied to the external changes through complex chains of mediation. By contrast, code running in a digital computer causes changes in machine behavior and, through networked ports and other interfaces, may initiate other changes, all implemented in the transmission and execution of code. (2005, 49–50)

## 9. Aesthetic Programming: performing critique (in this specific artwork - Vocable Code)

RunMe: https://dobbeltdagger.net/VocableCode_Educational/

- Think about the materials: what's code and what's execution?
    - language: natural language and execution of code
- what's happening behind? "there is a discrepancy as what you see is not literally how it operates"
    - the concept of 'sourcery' (Chun 2008 - "On 'Sourcery,' or Code as Fetish")
      - code and its execution
      - knowledge-power negotiation
- questioning the assumptions: normativity of code and coding
    - What's being prioritized in software design and apps? (front/back end)
    - what's an interface?
    - queerng the demarcation
- bodily practice
    - voice (both humans and nonhumans) & voice as political statements/expressions
    - binary status of 1/0 and discrete structure
        - master and slave, parent and child, human and machine...

## 10. Forking the Vocable Code

- Work as a group.
- Download the whole Vocable Code program with libraries here (https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode) > Click Download the whole directory, and run it on your own computer.
  - Briefly discuss the various computational structures and syntax to understand how things generally work, then specifically examine the relationship between voice file naming and the JSON file structure.
  - Follow the instructions and record your own voice with your computer or mobile phone. (The program only accepts the .wav file format)
    - Find a blank sheet of paper and prepare to write a sentence.
    - Complete the sentence with the starting given words: “Queer is.”
    - Each sentence contains no more than 5 words (the first words “queer is” don’t count). It is ok to add just one word.
    - Produce a maximum of two sentences/voices.
    - Download/locate a voice recording app on your smartphone (e.g. Voice Recorder on Android or Voice Memos on iOS).
    - Try to find a quiet environment, record your voice, and see if the app works (controlling the start and end the recording).
    - Prepare to record yourself reading your written sentence(s).
    - You may decide the temporality and rhythm.
    - You may either speak the full word or full sentence with different intonation.
    - Record your voice, then convert the recording into a .wav file. Audacity is an example of free software that can do so.
- Add your voice/s and update the JSON file and put your voice files in the voices folder. Refresh the program and see if you can hear your own voice among the voices.
- Advanced: Try to change the text presentation, e.g. its color or its animated behavior.
  - Discuss the different critical and aesthetic aspects of queer code.

## 11. Ideation - for miniX [8] - group work

![](https://miro.medium.com/max/1200/1*YETpi927sVRseQxePA6zeQ.png)

- creative process -> generate ideas
- Part of Design Thinking process
- Gather with open minds and produce as many ideas as they can
- No bad ideas
- Being bold and curious
- generate range of possibilities
- Ask silly/stupid questions

**Some food for thoughts to kick start:**

- What interests you in electronic literature? (and why?)
- What might be the concept that you want to address?
  - materiality of text/voice/literature
  - address gender disparity / societal issues / social injustice
  - expression of (love) feelings and care
  - explore possibilities e.g stretch the boundary of what it means as a piece of literature?
  - Any question that you want to explore?
  - provide alternative imaginery of things?
  - Provide provocation?
  - What's interesting/inspiring from the assigned readings that you want to explore further via coding?
  - what do you want to problematize?

**Tools:**

- sketching
- mind mapping
- flow chart?
- storyboarding

**Understand the dynamic of the team:**

- What's the strength of the team members?
- How to better divide the tasks within a tight timeline?
- How to make sure everyone is one the same page?
- How to make sure everyone is included in the design process?
- What would be the next step? How would you use the Thur and Fri instructor sessions?

**Deliverable:**

- What's the problem you want to pose/understand (not solve) via this miniX?

## 12. ⏺️Presentation by students on MiniX [7]

![](https://media.giphy.com/media/l0MYS1WzKiLlEeQJq/giphy.gif)

0. Group of 4
1. Present to one another: 5 mins each
2. Discuss the ideas and questions together
