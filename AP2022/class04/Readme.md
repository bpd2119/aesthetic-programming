# Class 04:  29 Feb & 2 Mar | Catch up week

**Messy notes:**

[[_TOC_]]

## 0. TEST - on arrays and for-loop

```
let words = [];
words[0] = "What";
words[1] = "is";
words[2] = "Aesthetic";
words[3] = "Programming";
words.push("?");
console.log(words.length);
```
1. What is the output of `console.log(words.length)`?
Answer [here](https://www.mentimeter.com/s/3c75b9ce9f122407537ad630e102a903/ab88cd0bcc60)

2. Write a for-loop to loop through all the words and display them on the web console?

3. live [together](https://teddavis.org/p5live/?cc=9g7a8)

## 1. Why catch up week?

- [Syllabus](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022)
- revisit some basic principles & some concepts framing of the course

> Beyond the focus on obtaining problem solving and technical skills, how to develop a more humanistic (cultural, creative and critical) approach to programming, especially addressing gender disparities, societal issues, social injustices and alternative imagination in relation to computational culture?

## 2. Why p5.js

- focus on accessibility and inclusivity - open access (how? and what that means?)
- put efforts in communities building, see [statement](https://p5js.org/community/)
- Watch the PCD video together (McCarthy, 2019)
  - What does it mean by taking uncertainties?
- free and open source **culture** -> community, contributors, people, social relations
- sensitive to tools that we are using, what it is shaping and promoting?
- can we design and make our own tools?

⏺️ Discussion
> What is access means to you and what's your privileges?
> Imaginary qn: If you have to think of projects to improve "access" using p5.js, what will you make?

## 3. Aesthetic Programming

Programming as a method, Aesthetic Programming as a methodology. A methodology to understand and articulate computational culture from a critical-aesthetic perspective.

🧐 What does it mean by programming/making critical work? What is critical in your miniX, and how would you articulate critical-aesthetic in programming?

**critical - site - reflection - politics and power dynamics - critical theory - imaginary - alternative - intervention**

**Critical Making and Interdisciplinary Learning by Matt ratto & Garnet Hertz**

> "material engagements with technologies to open up and extend critical social reflection" - p. 18

> "technological artifacts embody cultural values, and that technological development can be combined with cultural reflectivity to build provocative objects that encourage a re-evaluation of the role of technology in culture" - p. 19

> "emphasize the value of material production itself as a site for critical reflection" - p. 19

> "critical making is about turning the relationship between technology and society from a 'matter of fact' into a matter of concern'" - p. 20

> "the process of being critical starts by denaturalizing standard assumptions, values, and norms in order to reflect on the position and role of specific technologies within society" - p. 22

**Aesthetic Programming (Preface) by Winnie Soon and Geoff Cox (2020):**

> "address the cultural and aesthetic dimensions of programming as a means to think and act critically" - p. 13

> "to reflect deeply on the pervasiveness of computational culture and its social and political effects - from the language of human-machine languages to abstraction of objects, datafication and recent developments in automated machine intelligence, for example." - p. 13

> "take a particular interest in power relations that are under-acknowledged, such as inequalities and injustices related to class, gender and sexuality, as well as race and the enduring legacies of colonialism." - p. 14

> "how power differentials are implicit in code in terms of binary logic, hierarchies, naming of attributes, accessibility, and how wider societal inequalities are further reinforced and perpetuated through non-representational computation" - p. 14

> we do not refer to ideas of beauty as it is commonly misunderstood (aka bourgeois aesthetics), but to political aesthetics: to what presents itself to sense-making experience and bodily perception (to paraphrase Jacques Rancière’s The Politics of Aesthetics, as one key reference). How we perceive the world in this political sense is not fixed, but is in constant flux, much like software itself and bound to wider conditions, and ecologies. - p. 14-15

> "Aesthetic programming in this sense is considered as a practice to build things, and makeworlds, but also produce immanent critique drawing upon computer science, art, and cultural theory." - p. 15

> "Such practical “knowing” also points to the practice of “doing thinking,” embracing a plurality of ways of working with programming to explore the set of relations between writing, coding and thinking to imagine,create and propose “alternatives.” - p. 15

Look at the required learning outcome again for this course:

1. Read and write computer code in a given programming language
2. Use programming to explore digital culture
3. Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

🧐 Discussion:

How do you see if your weekly miniX / miniX feedback has fullfilled the required outcomes? If not, what can be done further to extend your work (both ReadMe and RunMe)beyond the focus on just practical/technical function of the code? Or how would you link the syntax, fundamental of programming to wider cultural phenomena?

## ⏺️4. One software artwork/critical design work

Prep: Please bring one software artwork/critical design work (requires programming in particular) that interests you.

Can you discuss the work within the context of Aesthetic Programming? Why you like this and how does it operate?

## 5. Re-visit concepts

- important fundamental concepts from last 3 weeks (variables, conditional structures, loop, arrays, function, etc);

## 6. Bugs/Debug | Errors

![](https://external-preview.redd.it/H--cUJKsbzN5L5f6JPE2CC4c1o7WnMmLAmtdfKJKYys.jpg?auto=webp&s=e7c8efae77c4d5d42160d46931d6096efce312de)
In 1945, a dead moth was taped into Grace Murray Hopper’s computer log book to document a problem with Harvard University’s Mark II Aiken Relay Calculator.

  - comment out
  - print/console.log()
  - https://p5js.org/learn/debugging.html
  - post a sketch and work together
  - the idea and estimation of planning -> break into smaller steps -> rescoping
  - gain insights into program operations and its dependency

When solving errors, it is important to identify exactly where they occur, i.e. which block or line of code contains the mistake by using console.log() (or print() in p5.js). Test and run the various parts of the program step by step, then try to identify the error types, and fix them accordingly.

3 usual types of errors:

1. **Syntax errors** are problems with the syntax, also known as parsing errors. These errors — such as spelling errors or missing a closed bracket — tend to be easier to catch, and can be detected by a parser (in this case the browser).

`SyntaxError: missing ) after argument list`

2. **Runtime errors** happen during the execution of a program while the syntax is correct.

```
p5.js says: loadImage() was expecting String for parameter #0 (zero-based index), received an empty variable instead. If not intentional, this is often a problem with scope: https://p5js.org/examples/data-variable-scope.html at about:srcdoc:94:6. https://github.com/processing/p5.js/wiki/Local-server
Wrong API key sent to the server. It is a more critical error because the program cannot extract the image and display it on the screen:

p5.js says: It looks like there was a problem loading your json. Try checking if the file path is correct, or running a local server.
```

3. **Logical errors** are arguably the hardest errors to locate as they deal with logic not syntax. The code may still run perfectly, but the result is not what was expected.
  - a discrepancy between what we think we asked the computer to do and how it actually processes the instructions.

## 7. Short presentation by students on MiniX [2]

![](https://media.giphy.com/media/l0MYS1WzKiLlEeQJq/giphy.gif)

1. note taking and questions: https://pad.vvvvvvaria.org/AP2022
2. Need one volunteer per presentation

## 8. (Extra) Ada Lovelace and the concept of loop

- Repetition, task driven, & computer operation: Ada Lovelace
- [diagram](https://gitlab.com/aesthetic-programming/book/-/raw/master/source/3-InfiniteLoops/ch3_1.jpg)
- loop / cycle
- a set of instructions to calculate Bernoulli’s Numbers
- written for Charles Babbage’s unbuilt Analytical Engine (in 1843)
- See the video here by Zoe Philpott: https://www.youtube.com/watch?v=1QQ3gWmd20s
  - Zoe Philpott is an award-winning experiential storyteller, writer, designer and tech entrepreneur.
  - Specialising in making complex ideas accessible and relevant to wider audiences.
  - She creates brand experiences for businesses in collaboration with technologists and designers.
  - She is the creator of Ada.Ada.Ada and speaks on story, creative innovation, and gender equality in STEM.
